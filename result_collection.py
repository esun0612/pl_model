#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 10 14:59:00 2019

@author: esun

Collect result
"""
from os.path import join as pjoin
import sys
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/erika_utils')
from model_analysis import collect_result, collect_result_2, merge_var_importance
    
model_output_dir = '/home/ec2-user/SageMaker/round_2/experiments'
exps = ['7c_rate_15_term']

merge_var_importance('../round_2/experiments/7c_2019-11-22_19-48-31/variable_importance_rate_7c.csv',
                     '../round_2/experiments/12_2020-01-14_14-29-17/variable_importance_rate_12.csv',
                     pjoin(model_output_dir, 'var_impt_7c_12_rate.csv'))


collect_result_2(model_output_dir, exps, 'oot', how='ratio')
collect_result(model_output_dir, exps, 'oos', how='ratio')

collect_result_2(model_output_dir, exps, 'oot', how='absolute')
collect_result(model_output_dir, exps, 'oos', how='absolute')