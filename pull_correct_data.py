#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 18 09:25:08 2019

@author: esun
Query basic data from sofidw
"""
import numpy as np
import pandas as pd
import sys
sys.path.insert(0, '/Users/esun/Documents/erika_git/erika_utils')
from util_data import query_sofidw, query_snowflake

start_date = '2018-01-01'
end_date = '2019-09-30'
product = f"'PL'"
date_range = f"af.date_start>='{start_date}' and af.date_start<='{end_date}'"

PL_qry = f"""
SELECT
af.id,
af.date_signed,
af.date_doc_upload,
af.date_start,
af.initial_term,
af.interest_rate,
af.requested_amount,
af.loan_amt,
af.challenger_name,
af.gross_income,
af.adjusted_gross_income,
af.app_created_via_mobile,
af.g_program,
af.g_grad_year,
af.credit_score,
CASE WHEN af.coborrower_applicant_id IS NOT NULL THEN 1 ELSE 0 END AS coborrower_ind,
af.employer_name,
af.years_of_experience,
af.pl_funds_use,
af.tax_burden_amount,
af.monthly_housing_cost,
af.consolidated_channel,
af.attr_affiliate_referrer,
af.campaign_welcome_bonus,
af.housing_status,
af.max_loan_amount,
acaf.pil0438,
af.registration_date,
af.revolving_credit_amount,
af.tier,
af.tier_type,
af.ug_ctgry,
af.ug_grad_year,
af.ug_program,
af.member_indicator,
acaf.iln5020,
acaf.alx8220,
acaf.all8020,
acaf.iln5820,
acaf.all0416,
acaf.ILN0416,
acaf.REV0416,
acaf.mtf5020,
acaf.mta5020,
acaf.mta5830,
acaf.mtj5030,
acaf.mtj5820,
acaf.all5820,
acaf.all6200,
acaf.all7516,
acaf.all8220,
acaf.all5020,
acaf.bcc7110,
acaf.bcc8322,
acaf.bcx3421,
acaf.bcx3422,
acaf.bcx5020,
acaf.bcx5320,
acaf.bcx7110,
acaf.iqf9416,
acaf.iqt9415,
acaf.rev2800,
acaf.rev5020,
acaf.rev6200,
acaf.rev8150,
acaf.rev8320,
acaf.rta7110,
acaf.rta7300,
acaf.rtr5030,
acaf.rtr6280,
acaf.all9110,
acaf.all6160,
acaf.all7517,
acaf.all8154,
acaf.all8163,
acaf.all9951,
acaf.iln0416,
acaf.mta0416,
acaf.mta1370,
acaf.mta2800,
acaf.mta8150,
acaf.mta8153,
acaf.mta8157,
acaf.mta8160,
acaf.mtf0416,
acaf.mtf4260,
acaf.mtf8166,
acaf.mts5020,
af.free_cash_flow_pre,
CASE WHEN af.date_doc_upload IS NOT NULL THEN 1 ELSE 0 END AS docs_ind,
CASE WHEN af.date_signed IS NOT NULL THEN 1 ELSE 0 END AS signed_ind
FROM dwmart.applications_file af
JOIN dwmart.application_credit_attributes_file acaf ON af.dw_application_id = acaf.dw_application_id
WHERE af.application_type = {product} AND {date_range}
AND (af.interest_rate_type = 'FIXED' OR af.interest_rate_type IS NULL) \
AND af.current_decision = 'ACCEPT'
"""

df = query_snowflake(PL_qry)
df.to_csv('/home/ec2-user/SageMaker/input/1801_1909_correct_raw.csv', index=None)
"""
Clean data for initial analysis
1. app_created_via_mobile to  0 / 1 variable
2. g_grad_year and ug_grad_year: year(date_start) - var
3. employer_name: clean and categorize into at least 'self employed', 'RETIRED', missing and 'employerName'
4. registration_date: date_start - var
"""

#app_created_via_mobile to  0 / 1 variable
df['app_created_via_mobile'] = df['app_created_via_mobile'].map({False: 0, True: 1})

#g_grad_year and ug_grad_year: year(date_start) - var
df['date_start'] = pd.to_datetime(df['date_start'])
df['g_grad_year'] = df['date_start'].dt.year - df['g_grad_year']
df['ug_grad_year'] = df['date_start'].dt.year - df['ug_grad_year']

#registration_date: date_start - var
df['registration_date'] = pd.to_datetime(df['registration_date'])
df['registration_date'] = (df['date_start'] - df['registration_date']).dt.days

#employer_name: clean and categorize into at least 'self employed', 'RETIRED', missing (<-'employerName')
df['employer_name'] = df['employer_name'].str.lower()
df['employer_name_new'] = 'other'
df['employer_name_new'] = np.where((df['employer_name'].str.contains('self', na=False)) & (df['employer_name'].str.contains('employ', na=False)), 'self_employed', df['employer_name_new'])
df['employer_name_new'] = np.where(df['employer_name'].str.contains('retire', na=False), 'retire', df['employer_name_new'])
df['employer_name_new'] = np.where((df['employer_name'].str.contains('employername', na=False)) | (df['employer_name'].isnull()) | (df['employer_name'] == ' '), 'missing', df['employer_name_new'])
df.drop('employer_name', axis=1, inplace=True)
df.rename(columns={'employer_name_new': 'employer_name'}, inplace=True)

df.to_csv('/home/ec2-user/SageMaker/input/1801_1909_correct_clean.csv', index=None)
