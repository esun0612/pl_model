#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 12 09:12:32 2019

@author: esun

Check and compare with OOT result
"""

from os.path import join as pjoin
import numpy as np
import pandas as pd
import sys
sys.path.insert(0, '/Users/esun/Documents/erika_git/erika_utils')
sys.path.insert(0, '/Users/esun/Documents/erika_git/erika_model')
from util_data import query_sofidw, query_snowflake, impute_rate
from util_model import score_model, load_json, score_term_model
from util_data import add_signed_ind, clean_str
from header_to_model import merge_all_pred, report_performance_metrics
from util_analysis import 

start_date = '2019-08-01'
end_date = '2019-09-30'
product = f"'PL'"
date_range = f"af.date_start>='{start_date}' and af.date_start<='{end_date}'"

df = query_snowflake(PL_qry)
df.to_csv('/home/ec2-user/SageMaker/score_new/oot/raw_oot.csv', index=None)

df_rate = query_sofidw(rate_qry)
df_rate.to_csv('/home/ec2-user/SageMaker/score_new/oot/rate_oot_2.csv', index=None)

#Dedup rate
df = pd.read_csv('/home/ec2-user/SageMaker/score_new/oot/raw_oot.csv')
df_rate = pd.read_csv('/home/ec2-user/SageMaker/score_new/oot/rate_oot_2.csv')

df_rate.sort_values(by=['id', 'product_term', 'max_amount'], inplace=True)
df2 = df_rate.drop_duplicates(subset=['id', 'product_term', 'min_rate'], keep='last') #For each unique id, product_term and min_rate, keep the largest max_amount
df_max = df2.drop_duplicates(subset=['id', 'product_term'], keep='last')
df_max['max_ind'] = 1
df4 = pd.merge(df2, df_max[['id', 'product_term', 'max_amount', 'max_ind']], how='left', on=['id', 'product_term', 'max_amount'])
df4 = df4[(df4['max_amount'] >= df4['requested_amount'])]
df4 = df4.sort_values(by=['id', 'product_term', 'max_amount'])
df4 = df4.drop_duplicates(subset=['id', 'product_term'], keep='first')
df4_new = flat_term_rate_dat(df4, flat_cols=['min_rate', 'tier'], consolidate_cols=['tier'])
df4_new.rename(columns={'tier': 'tier_pre'}, inplace=True)

#Merge data
df = pd.merge(df, df4_new, on='id')
#Preprocess data
PL_terms = [2, 3, 4, 5, 6, 7]
df = add_signed_ind(df, terms=PL_terms)
df['member_indicator'] = df['member_indicator'].map({False: 0, True: 1})
df['attr_affiliate_referrer'] = clean_str(df['attr_affiliate_referrer'])
df['date_start'] = pd.to_datetime(df['date_start'])
min_start_date = df['date_start'].min()
df['date_start_year'] =  df['date_start'].dt.year
df['date_start_month'] = df['date_start'].dt.month
df['date_start_day'] = df['date_start'].dt.day
df['date_start_weekday'] = df['date_start'].dt.weekday
df['ref_month'] = df['date_start_year'] * 12 + df['date_start_month'] - (min_start_date.year * 12 + min_start_date.month)
df['ref_day'] = df['date_start'].dt.dayofyear + (df['date_start_year'] - min_start_date.year) * 365
df['attr_affiliate_referrer_new'] = clean_str(df['attr_affiliate_referrer'])
df['attr_affiliate_referrer_new'] = np.where(df['attr_affiliate_referrer_new'].isin(['www.sofi.com','www.google.com','credit_karma','lending_tree']),
                                                 df['attr_affiliate_referrer_new'], 'other')
df['grad'] = np.where(df['g_program'].notnull(), 1, 0)

for term in PL_terms:
    df[f'initial_term_{term}'] = np.where((df['initial_term'] == term), 1, 0)
    
impute_rate_dict = {'min_rate_2': 0.16677,
                 'min_rate_3': 0.16677,
                 'min_rate_4': 0.17436,
                 'min_rate_5': 0.1792,
                 'min_rate_6': 0.15313,
                 'min_rate_7': 0.16115}
df, impute_rate_dict = impute_rate(df, rate_cols=[f'min_rate_{term}' for term in PL_terms], impute_rate_dict=impute_rate_dict)

df = flat_rate(df, PL_terms)

def flat_rate(df, terms, term_col='initial_term', rate_col='interest_rate'):
    rate_map_cols = [f'rate_map_{term}' for term in terms]
    for term, col in zip(terms, rate_map_cols):
        df[col] = np.where(df[term_col] == term, df[rate_col], 0)
    return df

df.to_csv('/home/ec2-user/SageMaker/score_new/oot/data_oot.csv', index=None)

#Change the signed_ind distribtution to similar as 10/04 to 11/04 as of 11/12
df = pd.read_csv('/home/ec2-user/SageMaker/score_new/oot/data_oot.csv')
df['date_lag_bound'] = np.random.choice(a=date_lag_distribution.index.tolist(), size=df.shape[0], p=date_lag_distribution.values)
df['date_start'] = pd.to_datetime(df['date_start'])
df['date_signed'] = pd.to_datetime(df['date_signed'])
df['sign_date_lag'] = (df['date_signed'] - df['date_start']).dt.days
df['signed_ind_early'] = np.where(df['sign_date_lag'] <= df['date_lag_bound'], df['signed_ind'], 0)
df.to_csv('/home/ec2-user/SageMaker/score_new/oot/data_oot.csv', index=None)

#Score model
model_dir = '/home/ec2-user/SageMaker/experiment/3_2019-11-08_17-04-22'
out_dir = '/home/ec2-user/SageMaker/score_new/oot'
header_dir = '/home/ec2-user/SageMaker/headers'

params_rate = load_json(pjoin(model_dir, 'params_rate_3.json')) 
terms = params_rate['terms']
rate_cols = ['{0}_{1}'.format(params_rate['term_col_prefix'], term) for term in terms]
pred_term_col = 'term_wgt_prob'
pred_term_prob_cols = [col + '_prob_adj' for col in rate_cols]
rate_orig_cols = [col + '_orig' for col in rate_cols]
target_term_col = 'initial_term'
target_term_prob_cols = [f'{target_term_col}_{term}' for term in terms]

score_model(pjoin(model_dir, 'lightgbm_rate.txt'), pjoin(out_dir, 'rate_pred_raw.csv'), 
            data_file=pjoin(out_dir, 'data_oot.csv'), header_file=pjoin(header_dir, 'exp_3_rate_header.csv'))

pred_rate = merge_rate_model(pjoin(out_dir, 'data_oot.csv'), pjoin(out_dir, 'rate_pred_raw.csv'), pjoin(out_dir, 'rate_pred_early.csv'), target_col='signed_ind_early')

score_model(pjoin(model_dir, 'lightgbm_term.txt'), pjoin(out_dir, 'term_pred.csv'), 
            data_file=pjoin(out_dir, 'data_oot.csv'), header_file=pjoin(header_dir, 'exp_3_term_header.csv'))

score_term_model(data_file=pjoin(out_dir, 'data_oot.csv'), raw_model_output_file=pjoin(out_dir, 'term_pred.csv'), 
                 output_file=pjoin(out_dir, 'pred_term_final.feather'), 
                 impute_rate_cols=rate_cols, orig_rate_cols=rate_orig_cols, terms=terms, target_term_prob_cols=target_term_prob_cols)

#Evaluation
act_term_prob_cols = ['signed_ind_{}'.format(term) for term in terms]  
rate_map_cols = [f'rate_map_{term}' for term in terms]
df_final = merge_all_pred(pjoin(out_dir, 'rate_pred_early.csv'), pjoin(out_dir, 'pred_term_final.feather'), 
                          pjoin(out_dir, 'data_oot.csv'), pjoin(out_dir, 'pred_final_early.feather'), act_term_prob_cols, rate_map_cols)
for term in terms:
    df_final[f'signed_ind_{term}'] = np.where((df['signed_ind_early'] == 1) & (df['initial_term'] == term), 1, 0)
        
final_result = report_performance_metrics([df_final], ['oot'], terms, act_term_prob_cols,
                                          pred_term_prob_cols, rate_map_cols, rate_cols, 'tier', 'tier_pre', act_sign_col='signed_ind_early')
final_result.to_csv(pjoin(out_dir, "final_metric_early.csv"))

#Check rate score
out_dir_2 = '/home/ec2-user/SageMaker/experiment/3_2019-11-08_17-04-22'
rate_pred_1 = pd.read_csv(pjoin(out_dir, 'rate_pred.csv'))
rate_pred_2 = pd.read_csv(pjoin(out_dir_2, 'pred_oot_rate.csv'))
c1 = compare_dat(rate_pred_1, rate_pred_2, idvar='id')
c1[c1['diff_rate_pred'] > 0.01].shape
#1988
#Rate prediction, a little off

#Check term score
term_pred_1 = eread(pjoin(out_dir, 'pred_term_final.feather'))
term_pred_2 = pd.read_csv(pjoin(out_dir_2, 'pred_oot_term_final.csv'))
c2 = compare_dat(term_pred_1, term_pred_2, idvar='id')
for col in c2.columns:
    if 'diff' in col:
        print(col, ",", c2[(c2[col] != 0) & (c2[col].notnull())].shape[0] / c2.shape[0])
#Check original data
header_rate = Header(pjoin(header_dir, 'exp_3_rate_header.csv'))
cols = header_rate.cat_cols + header_rate.num_cols
df1 = pd.read_csv(pjoin(out_dir, 'data_oot.csv'))
df2 = pd.read_csv('/home/ec2-user/SageMaker/input/PL_rate_oot.csv')
c = compare_dat(df, df2, idvar='id', common_cols=cols)
#Data not match!
Data type matches!
all0416 -7.0 3.0
all5020 -352029.0 719807.0
all5820 -7796.0 4470.0
all6200 0.0 0.0
all7516 -25.0 33.0
all8020 -699.0 1132.0
all8220 -44.0 3.0
all9110 0.0 0.0
alx8220 -44.0 63.0
attr_affiliate_referrer_new is not numerical type
Match on two columns: False
bcc7110 -37.0 36.0
bcc8322 -48.0 12.0
bcx3421 -2.0 1.0
bcx3422 -2.0 1.0
bcx5020 -27443.0 26365.0
bcx5320 -24000.0 25000.0
bcx7110 -37.0 26.0
coborrower_ind 0 0
consolidated_channel is not numerical type
Match on two columns: False
credit_score -42 49
date_start_day 0 0
date_start_month 0 0
date_start_year 0 0
free_cash_flow_pre -13246.21 4876.320000000001
grad 0 1
gross_income -297768.92000000004 81630.0
iln0416 is not numerical type
Match on two columns: False
iln5020 -51126.0 57086.0
iln5820 -1136.0 834.0
iqf9416 0.0 0.0
iqt9415 -1.0 2.0
min_rate_2 -0.10437 0.10287
min_rate_3 -0.07667 0.09672
min_rate_4 -0.07435999999999998 0.09911999999999999
min_rate_5 -0.041630000000000014 0.09394999999999999
min_rate_6 -0.03322999999999998 0.0503
min_rate_7 -0.03084999999999999 0.055099999999999996
monthly_housing_cost -968.62 77.62
mta5020 -999697950.0 999651055.0
mta5830 -999997826.0 999996980.0
mtf5020 -999697950.0 999651055.0
pl_funds_use is not numerical type
Match on two columns: True
requested_amount -73000.0 40000.0
rev0416 -1.0 3.0
rev2800 0.0 0.0
rev5020 -55658.0 30755.0
rev6200 0.0 0.0
rev8150 0.0 3.0
rev8320 -17.0 10.0
rta7110 -991.0 962.0
rta7300 -11.0 9.0
rtr5030 -999999940.0 999997522.0
rtr6280 -996.0 996.0
tax_burden_amount -138918.84999999998 36144.0

for col in c.columns:
    if 'diff' in col:
        print(col, ",", c[(c[col] != 0) & (c[col].notnull())].shape[0] / c.shape[0])

for col in [f'diff_min_rate_{term}' for term in range(2, 8)]:
    print(col, ",", c[(c[col].abs() > 0.01) & (c[col].notnull())].shape[0] / c.shape[0])

def merge_rate_model(data_file, raw_model_output_file, output_file, id_col='id', target_col='signed_ind'):
    y_pred = eread(raw_model_output_file)
    df = pd.read_csv(data_file, usecols=[id_col, target_col])
    df['rate_pred'] = y_pred
    esave(df, output_file)
    return df

def flat_term_rate_dat(df_orig, flat_cols, term_col='product_term', 
                       id_col='id', term_values=[2, 3, 4, 5, 6, 7],
                       consolidate_cols=['tier'], rename_postfix=None):
    """Flatten the data based on term_col
    """
    df_flat = df_orig.copy()
    df_flat = df_flat[df_flat[term_col].isin(term_values)]
    df_flat = df_flat.pivot(index=id_col, columns=term_col, values=flat_cols)
    df_flat.columns = ['_'.join([col_level_0, str(col_level_1)]) for col_level_0, col_level_1 in zip(list(df_flat.columns.get_level_values(0)), list(df_flat.columns.get_level_values(1)))]
    if consolidate_cols is not None:
        for consolidate_col in consolidate_cols:
            orig_cols = [consolidate_col + '_{}'.format(term) for term in [2, 3, 4, 5, 6, 7]]
            df_flat[consolidate_col] = df_flat[orig_cols].min(axis=1, skipna=True)
            df_flat.drop(columns=orig_cols, inplace=True)
    if rename_postfix is not None:
        df_rename_dict = {col: col + '_' + rename_postfix for col in df_flat.columns}
        df_flat.rename(columns=df_rename_dict, inplace=True)
    return df_flat

rate_qry = f"""SELECT
af.id,
af.requested_amount,
p.product_term,
of.min_rate,
SUBSTRING(o.min_tier_code,'[1-9]')::INT AS tier,
of.max_amount
FROM dwmart.applications_file af
left JOIN product_application_facts paf on af.dw_application_id = paf.application_id
JOIN underwriting_info ui ON ui.underwriting_info_id = coalesce(NULLIF(paf.final_uw_id, 0), NULLIF(paf.selected_uw_id, 0), NULLIF(paf. initial_uw_id, 0))
JOIN offer_facts of ON of.underwriting_info_id = ui.underwriting_info_id
JOIN offer_details o on of.offer_details_id = o.offer_details_id
JOIN products p ON p.product_id = of.product_id
WHERE o.rate_type_code = 'FIXED' AND o.eligible = TRUE
and af.application_type = {product} AND {date_range}
AND (af.interest_rate_type = 'FIXED' OR af.interest_rate_type IS NULL)
AND af.current_decision = 'ACCEPT'
"""

PL_qry = f"""
SELECT
af.id,
af.date_signed,
af.date_doc_upload,
af.date_start,
af.initial_term,
af.interest_rate,
af.requested_amount,
af.loan_amt,
af.challenger_name,
af.gross_income,
af.adjusted_gross_income,
af.app_created_via_mobile,
af.g_program,
af.g_grad_year,
af.credit_score,
CASE WHEN af.coborrower_applicant_id IS NOT NULL THEN 1 ELSE 0 END AS coborrower_ind,
af.employer_name,
af.years_of_experience,
af.pl_funds_use,
af.tax_burden_amount,
af.monthly_housing_cost,
af.consolidated_channel,
af.attr_affiliate_referrer,
af.campaign_welcome_bonus,
af.housing_status,
af.max_loan_amount,
acaf.pil0438,
af.registration_date,
af.revolving_credit_amount,
af.tier,
af.tier_type,
af.ug_ctgry,
af.ug_grad_year,
af.ug_program,
af.member_indicator,
acaf.iln5020,
acaf.alx8220,
acaf.all8020,
acaf.iln5820,
acaf.all0416,
acaf.ILN0416,
acaf.REV0416,
acaf.mtf5020,
acaf.mta5020,
acaf.mta5830,
acaf.mtj5030,
acaf.mtj5820,
acaf.all5820,
acaf.all6200,
acaf.all7516,
acaf.all8220,
acaf.all5020,
acaf.bcc7110,
acaf.bcc8322,
acaf.bcx3421,
acaf.bcx3422,
acaf.bcx5020,
acaf.bcx5320,
acaf.bcx7110,
acaf.iqf9416,
acaf.iqt9415,
acaf.rev2800,
acaf.rev5020,
acaf.rev6200,
acaf.rev8150,
acaf.rev8320,
acaf.rta7110,
acaf.rta7300,
acaf.rtr5030,
acaf.rtr6280,
acaf.all9110,
acaf.all6160,
acaf.all7517,
acaf.all8154,
acaf.all8163,
acaf.all9951,
acaf.iln0416,
acaf.mta0416,
acaf.mta1370,
acaf.mta2800,
acaf.mta8150,
acaf.mta8153,
acaf.mta8157,
acaf.mta8160,
acaf.mtf0416,
acaf.mtf4260,
acaf.mtf8166,
acaf.mts5020,
af.free_cash_flow_pre,
CASE WHEN af.date_doc_upload IS NOT NULL THEN 1 ELSE 0 END AS docs_ind,
CASE WHEN af.date_signed IS NOT NULL THEN 1 ELSE 0 END AS signed_ind
FROM dwmart.applications_file af
JOIN dwmart.application_credit_attributes_file acaf ON af.dw_application_id = acaf.dw_application_id
WHERE af.application_type = {product} AND {date_range}
AND (af.interest_rate_type = 'FIXED' OR af.interest_rate_type IS NULL)
AND af.current_decision = 'ACCEPT'
"""

