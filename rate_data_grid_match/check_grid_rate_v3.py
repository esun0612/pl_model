#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 25 14:09:52 2019

@author: esun
"""
from util_data import read_grid

rate_qry = f"""
SELECT
af.id,
af.applicant_id,
af.requested_amount,
af.credit_score,
af.date_start,
af.interest_rate,
af.interest_rate_type,
CASE WHEN af.date_signed IS NOT NULL OR af.date_fund is not null THEN 1 ELSE 0 END AS signed_ind,
p.product_term,
o.champion_challenger_name,
ofr.min_rate,
d.CALENDAR_DATE as offer_date,
REGEXP_SUBSTR(o.min_tier_code,'[1-9]') AS tier,
ofr.max_amount
FROM dwmart.applications_file af
left JOIN product_application_facts paf on af.dw_application_id = paf.application_id
JOIN underwriting_info ui ON ui.underwriting_info_id = coalesce(NULLIF(paf.final_uw_id, 0), NULLIF(paf.selected_uw_id, 0), NULLIF(paf. initial_uw_id, 0))
JOIN offer_facts ofr ON ofr.underwriting_info_id = ui.underwriting_info_id
JOIN offer_details o on ofr.offer_details_id = o.offer_details_id
JOIN products p ON p.product_id = ofr.product_id
JOIN sofidw.dates d ON d.DATE_ID = ofr.ASOF_DATE_ID
WHERE o.rate_type_code = 'FIXED' AND o.eligible = TRUE
and
af.current_decision = 'ACCEPT'
AND af.date_start >= '2018-04-01'
AND af.application_type in ('PL')
and offer_date >= '2019-10-04' and offer_date <= '2019-11-04'
and champion_challenger_name = 'O.G. Challenger'
"""

df_rate = query_snowflake(rate_qry)
df_rate['tier'] = df_rate['tier'].astype(int)
df_rate['id'].nunique()
#7779 (7770 on tableau)
df_rate.to_csv('/home/ec2-user/SageMaker/score_new/check_rate_match/raw_rates_1125.csv', index=None)

df = df_rate.drop_duplicates(['id'])
df = df[df['interest_rate_type'] != 'VARIABLE']
wac = (df['interest_rate'] * df['requested_amount'] * df['signed_ind']).sum() / (df['requested_amount'] * df['signed_ind']).sum()
#0.11782731945811531
#-0.00225 ACH: 0.11557731945811531 -> match

df_rate.sort_values(by=['id', 'product_term', 'max_amount'], inplace=True)
df2 = df_rate.drop_duplicates(subset=['id', 'product_term', 'min_rate'], keep='last') #For each unique id, product_term and min_rate, keep the largest max_amount
df4 = df2[df2['max_amount'] >= df2['requested_amount']]
df4 = df4.sort_values(by=['id', 'product_term', 'max_amount'])
df4 = df4.drop_duplicates(subset=['id', 'product_term'], keep='first')
df4_new = flat_term_rate_dat(df4, flat_cols=['min_rate', 'tier'], consolidate_cols=[])
df4_new.shape
#7172
df4_new = pd.merge(df4_new, df[['id', 'applicant_id', 'date_start', 'offer_date', 'requested_amount', 'credit_score']], left_index=True, right_on='id')
df4_new.shape
#(6936, 15)

df = df4_new
credit_score_bins= [680, 700, 720, 740, 760, 780, 800, np.inf]
df['fico_bin'] = pd.cut(df['credit_score'], bins=credit_score_bins, include_lowest=True, right=False).astype(str)  
df['loan_amt_bin'] = np.where(df['requested_amount'] <= 20000, '[5000, 20000]',
                      np.where(df['requested_amount'] <= 50000, '(20000, 50000]', '(50000, 100000]'))

grid_dir = '~/SageMaker/price_grid'
grid = read_grid(pjoin(grid_dir, 'am_grid.xlsx'))

#Check conversion on the population with abs diff > 5 bps
result = []
for term in range(2, 8):
    df_term_check = pd.merge(df[df[f'tier_{term}'].notnull()], grid, left_on=[f'tier_{term}', 'fico_bin', 'loan_amt_bin'], right_on=['tier','fico_bin', 'loan_amt_bin'])
    notnull_shape = df[df[f'tier_{term}'].notnull()].shape[0]
    df_diff = df_term_check[(df_term_check[f'min_rate_{term}'] - df_term_check[f'grid_rate_{term}']).abs() > 0.0001]
    diff_pct = df_diff.shape[0] / df_term_check.shape[0]
    result.append([notnull_shape, df_diff.shape[0], diff_pct])
    
pd.DataFrame(result)

#Nearly 100% match
      0   1         2
0  5691   0  0.000000
1  6332   0  0.000000
2  6430  38  0.005910
3  6621  38  0.005739
4  2211   1  0.000452
5  2177   0  0.000000

for term in range(2, 8):
    df_term_check = pd.merge(df[df[f'tier_{term}'].notnull()], grid, left_on=[f'tier_{term}', 'fico_bin', 'loan_amt_bin'], right_on=['tier','fico_bin', 'loan_amt_bin'])
    df = pd.merge(df, df_term_check[['id', f'grid_rate_{term}']], on='id', how='left')
    df_diff = df_term_check[(df_term_check[f'min_rate_{term}'] - df_term_check[f'grid_rate_{term}']).abs() > 0.0005]
    df_diff[f'term_diff_ind_{term}'] = 1
    df = pd.merge(df, df_diff[['id', f'term_diff_ind_{term}']], on='id', how='left')
    
df_out = df[(df['term_diff_ind_2']==1) | (df['term_diff_ind_3']==1) | (df['term_diff_ind_4']==1)| (df['term_diff_ind_5']==1)| (df['term_diff_ind_6']==1)| (df['term_diff_ind_7']==1)]
df_out.shape
Out[267]: (42, 29)
for term in range(2, 8):
    df_out[f'diff_{term}'] = df[f'grid_rate_{term}'] - df[f'min_rate_{term}']
df_out.to_csv('/home/ec2-user/SageMaker/score_new/check_rate_match/check_1125.csv', index=None)
