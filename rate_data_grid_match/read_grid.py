#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 12 15:54:09 2019

@author: esun
"""
from os.path import join as pjoin
import numpy as np
import pandas as pd
import sys
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/erika_utils')
from util_analysis import compare_dat_3
from util_data import flat_term_rate_dat, read_grid
from util_data import query_sofidw, query_snowflake

#Clean rate data for comparison
df_rate = pd.read_csv('/home/ec2-user/SageMaker/score_new/rate_am.csv')
df_rate.sort_values(by=['id', 'product_term', 'max_amount'], inplace=True)
df2 = df_rate.drop_duplicates(subset=['id', 'product_term', 'min_rate'], keep='last') #For each unique id, product_term and min_rate, keep the largest max_amount
df4 = df2[df2['max_amount'] >= df2['requested_amount']]
df4 = df4.sort_values(by=['id', 'product_term', 'max_amount'])
df4 = df4.drop_duplicates(subset=['id', 'product_term'], keep='first')
df4_new = flat_term_rate_dat(df4, flat_cols=['min_rate', 'tier'], consolidate_cols=[])
df4_new.shape
#Out[15]: (8072, 12)
df4_new.to_csv('/home/ec2-user/SageMaker/score_new/rate_am_invest.csv')

#Create FICO and loan amount bin in data
take_cols = ['id', 'date_signed', 'date_doc_upload', 'date_start', 'date_fund',
           'interest_rate_type', 'initial_term', 'interest_rate', 'requested_amount', 'challenger_name', 
           'gross_income', 'credit_score', 'free_cash_flow_pre', 'docs_ind', 'signed_ind']
df = pd.read_csv('/home/ec2-user/SageMaker/score_new/raw_am.csv', usecols=take_cols)
df.shape
#Out[135]: (8849, 15)
df = df[df['interest_rate_type'] != 'VARIABLE']
df.shape
#Out[137]: (8611, 15)
df4_new = pd.read_csv('/home/ec2-user/SageMaker/score_new/rate_am_invest.csv')

credit_score_bins= [680, 700, 720, 740, 760, 780, 800, np.inf]
df['fico_bin'] = pd.cut(df['credit_score'], bins=credit_score_bins, include_lowest=True, right=False).astype(str)  
df['loan_amt_bin'] = np.where(df['requested_amount'] <= 20000, '[5000, 20000]',
                      np.where(df['requested_amount'] <= 50000, '(20000, 50000]', '(50000, 100000]'))
df = pd.merge(df, df4_new, on='id')
df.shape
#(7841, 29)

#Read grid
grid_dir = '~/SageMaker/price_grid'
grid = read_grid(pjoin(grid_dir, 'am_grid.xlsx'))

for term in range(2, 8):
    df_term_check = pd.merge(df[df[f'tier_{term}'].notnull()], grid, left_on=[f'tier_{term}', 'fico_bin', 'loan_amt_bin'], right_on=['tier','fico_bin', 'loan_amt_bin'])
    print(df[df[f'tier_{term}'].notnull()].shape[0] == df_term_check.shape[0])
    compare_dat_3(df_term_check, f'min_rate_{term}', f'grid_rate_{term}', output_dir='/home/ec2-user/SageMaker/score_new/redevelopment', out_nm=f'term_{term}')

result = []
for term in range(2, 8):
    df_term_check = pd.merge(df[df[f'tier_{term}'].notnull()], grid, left_on=[f'tier_{term}', 'fico_bin', 'loan_amt_bin'], right_on=['tier','fico_bin', 'loan_amt_bin'])
    notnull_shape = df[df[f'tier_{term}'].notnull()].shape[0]
    df_diff = df_term_check[(df_term_check[f'min_rate_{term}'] - df_term_check[f'grid_rate_{term}']).abs() > 0.0005]
    diff_pct = df_diff.shape[0] / df_term_check.shape[0]
    result.append([notnull_shape, df_diff.shape[0], diff_pct])
    
pd.DataFrame(result)



