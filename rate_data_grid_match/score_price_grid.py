#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 18 09:35:10 2019

@author: esun

Score on price from grid
"""

from os.path import join as pjoin
import numpy as np
import pandas as pd
import sys
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/erika_utils')
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/pl_model')
from util_data import query_sofidw, query_snowflake, impute_rate
from util_model import score_model, load_json, score_term_model, merge_rate_model
from util_data import add_signed_ind, clean_str, eread, esave, divide_train_oos, read_grid, flat_term_rate_dat
from header_to_model import merge_all_pred, report_performance_metrics

#Change rate from price from grid
dat_dir = '/home/ec2-user/SageMaker/score_new/redevelopment'
df_oot = pd.read_csv(pjoin(dat_dir, 'PL_rate_oot.csv'))
df_oot.shape
Out[16]: (8840, 130)

#Create tier by term columns
df_rate = eread('/home/ec2-user/SageMaker/score_new/rate_am.csv')
df_rate.sort_values(by=['id', 'product_term', 'max_amount'], inplace=True)
df2 = df_rate.drop_duplicates(subset=['id', 'product_term', 'min_rate'], keep='last') #For each unique id, product_term and min_rate, keep the largest max_amount
df_max = df2.drop_duplicates(subset=['id', 'product_term'], keep='last')
df_max['max_ind'] = 1
df4 = pd.merge(df2, df_max[['id', 'product_term', 'max_amount', 'max_ind']], how='left', on=['id', 'product_term', 'max_amount'])
df4 = df4[(df4['max_amount'] >= df4['requested_amount']) | (df4['max_ind'] == 1)]
df4 = df4.sort_values(by=['id', 'product_term', 'max_amount'])
df4 = df4.drop_duplicates(subset=['id', 'product_term'], keep='first')
df4_new = flat_term_rate_dat(df4, flat_cols=['min_rate', 'tier'], consolidate_cols=[])

#Join with df_oot
df_oot = pd.merge(df_oot, df4_new[[f'tier_{term}' for term in range(2, 8)]], left_on='id', right_index=True)
df_oot.shape

#Change min_rate_{term}, min_rate_{term}_orig
#Read grid
grid_dir = '~/SageMaker/price_grid'
grid = read_grid(pjoin(grid_dir, 'am_grid.xlsx'))

def create_fico_loan_bin(df):
    """Create fico and loan amount bin for a given dataframe. The bin is according to price grid fico loan amount overlay
    """
    credit_score_bins= [680, 700, 720, 740, 760, 780, 800, np.inf]
    df['fico_bin'] = pd.cut(df['credit_score'], bins=credit_score_bins, include_lowest=True, right=False).astype(str)  
    df['loan_amt_bin'] = np.where(df['requested_amount'] <= 20000, '[5000, 20000]',
                          np.where(df['requested_amount'] <= 50000, '(20000, 50000]', '(50000, 100000]'))
    return df
df_oot = create_fico_loan_bin(df_oot)

def merge_grid_price(df, grid, terms):
    """Merge price from grid
    """
    for term in terms:
        df_term = pd.merge(df.loc[df_oot[f'tier_{term}'].notnull(), ['id', f'tier_{term}', 'fico_bin', 'loan_amt_bin']], 
                           grid[['tier','fico_bin', 'loan_amt_bin', f'grid_rate_{term}']], 
                         left_on=[f'tier_{term}', 'fico_bin', 'loan_amt_bin'], right_on=['tier','fico_bin', 'loan_amt_bin'])
        df = pd.merge(df, df_term[['id', f'grid_rate_{term}']], on='id', how='left')
    return df
df_oot = merge_grid_price(df_oot, grid, terms=range(2,8))

#replace rate column by rate from grid
df_oot.drop([f'min_rate_{term}' for term in range(2,8)], axis=1, inplace=True)
df_oot.rename(columns={f'grid_rate_{term}': f'min_rate_{term}' for term in range(2,8)}, inplace=True)
PL_terms = list(range(2,8))
impute_rate_dict = {'min_rate_2': 0.16677,
 'min_rate_3': 0.16677,
 'min_rate_4': 0.17436,
 'min_rate_5': 0.1792,
 'min_rate_6': 0.15313,
 'min_rate_7': 0.16115}
df_oot, a = impute_rate(df_oot, rate_cols=[f'min_rate_{term}' for term in PL_terms], impute_rate_dict=impute_rate_dict)

df_oot.shape
Out[54]: (8840, 138)
df_oot.to_csv(pjoin(dat_dir, 'PL_rate_oot_grid.csv'), index=None)

#Score model
model_dir = dat_dir
out_dir = model_dir
header_dir = '/home/ec2-user/SageMaker/headers'

params_rate = load_json(pjoin(model_dir, 'params_rate_redevelop.json')) 
terms = params_rate['terms']
rate_cols = ['{0}_{1}'.format(params_rate['term_col_prefix'], term) for term in terms]
pred_term_col = 'term_wgt_prob'
pred_term_prob_cols = [col + '_prob_adj' for col in rate_cols]
rate_orig_cols = [col + '_orig' for col in rate_cols]
target_term_col = 'initial_term'
target_term_prob_cols = [f'{target_term_col}_{term}' for term in terms]

score_model(pjoin(model_dir, 'model_rate.txt'), pjoin(out_dir, 'rate_pred_raw_grid.csv'), 
            data_file=pjoin(out_dir, 'PL_rate_oot_grid.csv'), header_file=pjoin(header_dir, 'exp_3_rate_header.csv'))
pred_rate = merge_rate_model(pjoin(out_dir, 'PL_rate_oot_grid.csv'), pjoin(out_dir, 'rate_pred_raw_grid.csv'), pjoin(out_dir, 'rate_pred_grid.csv'))
score_model(pjoin(model_dir, 'model_term.txt'), pjoin(out_dir, 'term_pred_grid.csv'), 
            data_file=pjoin(out_dir, 'PL_rate_oot_grid.csv'), header_file=pjoin(header_dir, 'exp_3_term_header.csv'))
score_term_model(data_file=pjoin(out_dir, 'PL_rate_oot_grid.csv'), raw_model_output_file=pjoin(out_dir, 'term_pred_grid.csv'), 
                 output_file=pjoin(out_dir, 'pred_term_final_grid.feather'), 
                 impute_rate_cols=rate_cols, orig_rate_cols=rate_orig_cols, terms=terms, target_term_prob_cols=target_term_prob_cols)

#Evaluation
act_term_prob_cols = ['signed_ind_{}'.format(term) for term in terms]  
rate_map_cols = [f'rate_map_{term}' for term in terms]
df_final = merge_all_pred(pjoin(out_dir, 'rate_pred_grid.csv'), pjoin(out_dir, 'pred_term_final_grid.feather'), 
                          pjoin(out_dir, 'PL_rate_oot_grid.csv'), pjoin(out_dir, 'pred_final_grid.feather'), act_term_prob_cols, rate_map_cols)
final_result = report_performance_metrics([df_final], ['oot'], terms, act_term_prob_cols,
                                          pred_term_prob_cols, rate_map_cols, rate_cols, 'tier', 'tier_pre')
final_result.to_csv(pjoin(out_dir, "final_metrics_grid.csv"))

#Check the result with filter
df_filter = pd.read_csv('/home/ec2-user/SageMaker/score_new/rate_am_asof_date_invest.csv', usecols=['id'])
df = pd.read_csv('/home/ec2-user/SageMaker/score_new/raw_am.csv', usecols=['id', 'interest_rate_type'])
df = df[df['interest_rate_type'] != 'VARIABLE']
df_filter = pd.merge(df_filter, df[['id']], on='id')
df_final_1 = eread(pjoin(out_dir, 'pred_oot_final.feather'))
df_final_1 = pd.merge(df_final_1, df_filter, on='id')
df_final_2 = eread(pjoin(out_dir, 'pred_final_grid.feather'))
df_final_2 = pd.merge(df_final_2, df_filter, on='id')

final_result = report_performance_metrics([df_final_1, df_final_2], ['data', 'grid'], terms, act_term_prob_cols,
                                          pred_term_prob_cols, rate_map_cols, rate_cols, 'tier', 'tier_pre')
final_result.to_csv(pjoin(out_dir, "final_metrics_filter_grid.csv"))

