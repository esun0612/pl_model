#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 13 15:07:08 2019

@author: esun

#Check ChaCha Distribution
"""
from os.path import join as pjoin
import numpy as np
import pandas as pd
import sys
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/erika_utils')
from util_analysis import compare_dat_3
from util_data import flat_term_rate_dat, read_grid
from util_data import query_sofidw, query_snowflake

#Requery rate data -> not helping
dat_dir = '/home/ec2-user/SageMaker/score_new/check_chacha'
start_date = '2019-10-04'
end_date = '2019-11-04'
product = f"'PL'"
date_range = f"af.date_start>='{start_date}' and af.date_start<='{end_date}'"
challenger_grid = "'O.G. Challenger'"

rate_qry = f"""SELECT
af.id,
af.requested_amount,
p.product_term,
of.created_date,
of.updated_date,
of.CHALLENGER_NAME,
of.min_rate,
SUBSTRING(o.min_tier_code,'[1-9]')::INT AS tier,
of.max_amount,
of.min_amount
FROM dwmart.applications_file af
left JOIN product_application_facts paf on af.dw_application_id = paf.application_id
JOIN underwriting_info ui ON ui.underwriting_info_id = coalesce(NULLIF(paf.final_uw_id, 0), NULLIF(paf.selected_uw_id, 0), NULLIF(paf. initial_uw_id, 0))
JOIN offer_facts of ON of.underwriting_info_id = ui.underwriting_info_id
JOIN offer_details o on of.offer_details_id = o.offer_details_id
JOIN products p ON p.product_id = of.product_id
WHERE o.rate_type_code = 'FIXED' AND o.eligible = TRUE
and af.application_type = {product} AND {date_range}
AND af.current_decision = 'ACCEPT'
AND af.challenger_name = {challenger_grid}
"""

df_rate = query_sofidw(rate_qry)
df_rate.to_csv(pjoin(dat_dir, 'rate_am.csv', index=None))
df_rate.sort_values(by=['id', 'product_term', 'max_amount'], inplace=True)

#Read data
take_cols = ['id', 'date_signed', 'date_doc_upload', 'date_start', 'date_fund',
           'interest_rate_type', 'initial_term', 'interest_rate', 'requested_amount', 'challenger_name', 
           'gross_income', 'credit_score', 'free_cash_flow_pre', 'docs_ind', 'signed_ind']
df = pd.read_csv('/home/ec2-user/SageMaker/score_new/raw_am.csv', usecols=take_cols)
df4_new = pd.read_csv('/home/ec2-user/SageMaker/score_new/rate_am_invest.csv')
df4_new.shape
#(8072, 13)
df = df[df['interest_rate_type'] != 'VARIABLE']
df.shape
#Out[137]: (8611, 15)
credit_score_bins= [680, 700, 720, 740, 760, 780, 800, np.inf]
df['fico_bin'] = pd.cut(df['credit_score'], bins=credit_score_bins, include_lowest=True, right=False).astype(str)  
df['loan_amt_bin'] = np.where(df['requested_amount'] <= 20000, '[5000, 20000]',
                      np.where(df['requested_amount'] <= 50000, '(20000, 50000]', '(50000, 100000]'))
df = pd.merge(df, df4_new, on='id')
df.shape
#(7841, 29)

grid_dir = '~/SageMaker/price_grid'
grid_am = read_grid(pjoin(grid_dir, 'am_grid.xlsx'))
grid_al = read_grid(pjoin(grid_dir, 'al_grid.xlsx'))
grid_ap = read_grid(pjoin(grid_dir, 'ap_grid.xlsx'))
grid_aq = read_grid(pjoin(grid_dir, 'aq_grid.xlsx'))

term = 5
id_cols = ['tier','fico_bin', 'loan_amt_bin']
df_term_check = pd.merge(df[df[f'tier_{term}'].notnull()], grid_am[id_cols + [f'grid_rate_{term}']], left_on=[f'tier_{term}', 'fico_bin', 'loan_amt_bin'], right_on=id_cols)
df_term_check.rename(columns={f'grid_rate_{term}': f'grid_rate_{term}_am'}, inplace=True)
for grid_data, grid_nm in zip([grid_al, grid_ap, grid_aq], ['al', 'ap', 'aq']):
    df_term_check = pd.merge(df_term_check, grid_data[id_cols + [f'grid_rate_{term}']], on=id_cols)
    df_term_check.rename(columns={f'grid_rate_{term}': f'grid_rate_{term}_{grid_nm}'}, inplace=True)

for grid_nm in ['am', 'al', 'ap', 'aq']:
    df_term_check[grid_nm] = 0
    df_term_check[grid_nm] = np.where(df_term_check[f'min_rate_{term}'] == df_term_check[f'grid_rate_{term}_{grid_nm}'], 1, 0)
df_term_check[['am', 'al', 'ap', 'aq']].sum()


df_term_check[(df_term_check['am'] == 0) & (df_term_check['al'] == 0) & (df_term_check['ap'] == 0) & (df_term_check['aq'] == 0)].shape



for term in range(2, 8):
    df_term_check = pd.merge(df[df[f'tier_{term}'].notnull()], grid, left_on=[f'tier_{term}', 'fico_bin', 'loan_amt_bin'], right_on=['tier','fico_bin', 'loan_amt_bin'])
    print(df[df[f'tier_{term}'].notnull()].shape[0] == df_term_check.shape[0])
    compare_dat_3(df_term_check, f'min_rate_{term}', f'grid_rate_{term}', output_dir='/home/ec2-user/SageMaker/score_new/redevelopment', out_nm=f'term_{term}')
