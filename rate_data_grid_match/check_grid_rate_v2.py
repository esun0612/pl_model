#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 19 09:46:37 2019

@author: esun

Fix challlenger name
"""
from os.path import join as pjoin
import numpy as np
import pandas as pd
import sys
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/erika_utils')
from util_analysis import compare_dat_3
from util_data import flat_term_rate_dat, read_grid, add_signed_ind, map_term_to_rate
from util_data import query_sofidw, query_snowflake

#Query the asof_dt
start_date = '2019-10-04'
end_date = '2019-11-04'
product = f"'PL'"
date_range = f"af.date_start>='{start_date}' and af.date_start<='{end_date}'"
challenger_grid = "'O.G. Challenger'"

rate_qry = f"""
select 
os.app_id,
os.asof_dt,
os.challenger_name,
os.term,
os.min_rate,
SUBSTRING(os.min_tier_code,'[1-9]')::INT AS tier,
os.max_amount,
of.min_rate as of_min_rate,
of.max_amount as of_max_amount
from offer_staging os
join offer_facts of on of.offer_package_id = os.offer_package_id
where os.product_name = 'PL' 
and os.asof_dt <= '2019-11-04' and os.asof_dt >= '2019-10-04'
and os.rate_type = 'FIXED' 
and os.decision = 'ACCEPT'
and os.challenger_name == 'O.G. Challenger'
"""

rate_qry = f"""SELECT
af.id,
af.date_start,
af.requested_amount,
p.product_term,
of.asof_date_id,
of.min_rate,
SUBSTRING(o.min_tier_code,'[1-9]')::INT AS tier,
of.max_amount
FROM dwmart.applications_file af
left JOIN product_application_facts paf on af.dw_application_id = paf.application_id
JOIN underwriting_info ui ON ui.underwriting_info_id = coalesce(NULLIF(paf.final_uw_id, 0), NULLIF(paf.selected_uw_id, 0), NULLIF(paf. initial_uw_id, 0))
JOIN offer_facts of ON of.underwriting_info_id = ui.underwriting_info_id
JOIN offer_details o on of.offer_details_id = o.offer_details_id
JOIN products p ON p.product_id = of.product_id
WHERE o.rate_type_code = 'FIXED' AND o.eligible = TRUE
and af.application_type = {product} AND {date_range}
AND af.current_decision = 'ACCEPT'
AND af.challenger_name = {challenger_grid}
and of.asof_date_id >= 20191004 and of.asof_date_id <= 20191104
"""

rate_qry = f"""
select 
distinct(os.app_id),
os.asof_dt,
os.challenger_name
from offer_staging os
join dwmart.applications_file af on af.id = os.app_id
join offer_facts of on af.dw_application_id = of.application_id
where os.product_name = 'PL' 
and os.asof_dt <= '2019-11-04' and os.asof_dt >= '2019-10-04'
and os.rate_type = 'FIXED' 
and os.decision = 'ACCEPT'
"""

SELECT
af.id,
os.asof_dt,
af.requested_amount,
os.term,
os.min_rate,
SUBSTRING(os.min_tier_code,'[1-9]')::INT AS tier,
os.max_amount
FROM dwmart.applications_file af
join offer_staging os on af.id = os.app_id
join offer_facts of ON af.dw_application_id = of.application_id
WHERE o.rate_type_code = 'FIXED' AND o.eligible = TRUE
and af.application_type = {product} AND {date_range}
AND af.current_decision = 'ACCEPT'
AND af.challenger_name = {challenger_grid}
and of.asof_date_id >= 20191004 and of.asof_date_id <= 20191104






df_rate = query_sofidw(rate_qry)

rate_qry = f"""SELECT
af.id,
af.date_start,
af.requested_amount,
p.product_term,
of.asof_date_id,
of.min_rate,
SUBSTRING(o.min_tier_code,'[1-9]')::INT AS tier,
of.max_amount,
a.CHALLENGER_NAME
FROM dwmart.applications_file af
left JOIN product_application_facts paf on af.dw_application_id = paf.application_id
JOIN underwriting_info ui ON ui.underwriting_info_id = coalesce(NULLIF(paf.final_uw_id, 0), NULLIF(paf.selected_uw_id, 0), NULLIF(paf. initial_uw_id, 0))
JOIN offer_facts of ON of.underwriting_info_id = ui.underwriting_info_id
join applications a on of.application_id = a.application_id
JOIN offer_details o on of.offer_details_id = o.offer_details_id
JOIN products p ON p.product_id = of.product_id
WHERE o.rate_type_code = 'FIXED' AND o.eligible = TRUE
and af.application_type = {product} AND {date_range}
AND af.current_decision = 'ACCEPT'
and of.asof_date_id >= 20191004 and of.asof_date_id <= 20191104
"""

t = df_rate.drop_duplicates(subset=['id'])
t.shape
Out[42]: (24993, 9)
t2 = pd.merge(t, df, on='id')
#(24333, 101)

t['challenger_name'].value_counts()

df_rate['challenger_name'].value_counts()
"""
Out[22]: 
O.G. Champion      95757
O.G. Challenger    91970
Morgan             59558
Rothschild         43872
Chase              42421
DEFAULT              249
"""
df_rate = df_rate[df_rate['challenger_name'] == 'O.G. Challenger']
print(df_rate.shape, df_rate['id'].nunique())
#(91970, 9) 6907

df_rate.to_csv('/home/ec2-user/SageMaker/score_new/raw_am_asof_date_3.csv', index=None)

#Dedup rate
df_rate.sort_values(by=['id', 'product_term', 'max_amount'], inplace=True)
df2 = df_rate.drop_duplicates(subset=['id', 'product_term', 'min_rate'], keep='last') #For each unique id, product_term and min_rate, keep the largest max_amount
df4 = df2[df2['max_amount'] >= df2['requested_amount']]
df4 = df4.sort_values(by=['id', 'product_term', 'max_amount'])
df4 = df4.drop_duplicates(subset=['id', 'product_term'], keep='first')
df4_new = flat_term_rate_dat(df4, flat_cols=['min_rate', 'tier'], consolidate_cols=[])
df4_new.shape
df_rate_id = df_rate.drop_duplicates(subset=['id', 'asof_date_id'])
df4_new = pd.merge(df4_new, df_rate_id[['id', 'asof_date_id']], on='id')
df4_new.shape
# (6339, 14)
df4_new.to_csv('/home/ec2-user/SageMaker/score_new/rate_am_asof_date_invest_2.csv')

df = query_sofidw(PL_qry)
df.shape
Out[30]: (31931, 91)
df['challenger_name'].value_counts()
"""
Out[31]: 
O.G. Champion      10747
O.G. Challenger     8586
Morgan              6149
Rothschild          3224
Chase               3195
DEFAULT               30
"""
df = pd.merge(df, df4_new, on='id')
df.shape
#(6138, 104)
df.to_csv('/home/ec2-user/SageMaker/score_new/raw_am_2.csv', index=None)

take_cols = ['id', 'applicant_id', 'date_signed', 'date_doc_upload', 'date_start', 'date_fund',
           'interest_rate_type', 'initial_term', 'interest_rate', 'requested_amount', 'challenger_name', 
           'gross_income', 'credit_score', 'free_cash_flow_pre', 'docs_ind', 'signed_ind', 'initial_term', 'consolidated_channel']
df = df[take_cols]
credit_score_bins= [680, 700, 720, 740, 760, 780, 800, np.inf]
df['fico_bin'] = pd.cut(df['credit_score'], bins=credit_score_bins, include_lowest=True, right=False).astype(str)  
df['loan_amt_bin'] = np.where(df['requested_amount'] <= 20000, '[5000, 20000]',
                      np.where(df['requested_amount'] <= 50000, '(20000, 50000]', '(50000, 100000]'))

PL_qry = f"""
SELECT
af.id,
af.applicant_id,
af.date_signed,
af.date_doc_upload,
af.date_start,
af.date_fund,
af.initial_term,
af.interest_rate,
af.requested_amount,
af.loan_amt,
af.challenger_name,
af.gross_income,
af.adjusted_gross_income,
af.app_created_via_mobile,
af.g_program,
af.g_grad_year,
af.credit_score,
CASE WHEN af.coborrower_applicant_id IS NOT NULL THEN 1 ELSE 0 END AS coborrower_ind,
af.employer_name,
af.years_of_experience,
af.pl_funds_use,
af.tax_burden_amount,
af.monthly_housing_cost,
af.consolidated_channel,
af.attr_affiliate_referrer,
af.campaign_welcome_bonus,
af.housing_status,
af.max_loan_amount,
acaf.pil0438,
af.registration_date,
af.revolving_credit_amount,
af.tier,
af.tier_type,
af.ug_ctgry,
af.ug_grad_year,
af.ug_program,
af.member_indicator,
acaf.iln5020,
acaf.alx8220,
acaf.all8020,
acaf.iln5820,
acaf.all0416,
acaf.ILN0416,
acaf.REV0416,
acaf.mtf5020,
acaf.mta5020,
acaf.mta5830,
acaf.mtj5030,
acaf.mtj5820,
acaf.all5820,
acaf.all6200,
acaf.all7516,
acaf.all8220,
acaf.all5020,
acaf.bcc7110,
acaf.bcc8322,
acaf.bcx3421,
acaf.bcx3422,
acaf.bcx5020,
acaf.bcx5320,
acaf.bcx7110,
acaf.iqf9416,
acaf.iqt9415,
acaf.rev2800,
acaf.rev5020,
acaf.rev6200,
acaf.rev8150,
acaf.rev8320,
acaf.rta7110,
acaf.rta7300,
acaf.rtr5030,
acaf.rtr6280,
acaf.all9110,
acaf.all6160,
acaf.all7517,
acaf.all8154,
acaf.all8163,
acaf.all9951,
acaf.iln0416,
acaf.mta0416,
acaf.mta1370,
acaf.mta2800,
acaf.mta8150,
acaf.mta8153,
acaf.mta8157,
acaf.mta8160,
acaf.mtf0416,
acaf.mtf4260,
acaf.mtf8166,
acaf.mts5020,
af.free_cash_flow_pre,
CASE WHEN af.date_doc_upload IS NOT NULL THEN 1 ELSE 0 END AS docs_ind,
CASE WHEN af.date_signed IS NOT NULL THEN 1 ELSE 0 END AS signed_ind
FROM dwmart.applications_file af
JOIN dwmart.application_credit_attributes_file acaf ON af.dw_application_id = acaf.dw_application_id
WHERE af.application_type = {product} AND {date_range}
AND (af.interest_rate_type = 'FIXED' OR af.interest_rate_type IS NULL) \
AND af.current_decision = 'ACCEPT'
"""