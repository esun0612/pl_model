#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 14 09:13:52 2019

@author: esun
"""

from os.path import join as pjoin
import numpy as np
import pandas as pd
import sys
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/erika_utils')
from util_analysis import compare_dat_3
from util_data import flat_term_rate_dat, read_grid, add_signed_ind, map_term_to_rate
from util_data import query_sofidw, query_snowflake

#Query the asof_dt
start_date = '2019-10-04'
end_date = '2019-11-04'
product = f"'PL'"
date_range = f"af.date_start>='{start_date}' and af.date_start<='{end_date}'"
challenger_grid = "'O.G. Challenger'"

df_rate = query_sofidw(rate_qry)

rate_qry = f"""SELECT
af.id,
af.date_start,
af.requested_amount,
p.product_term,
of.asof_date_id,
of.min_rate,
SUBSTRING(o.min_tier_code,'[1-9]')::INT AS tier,
of.max_amount
FROM dwmart.applications_file af
left JOIN product_application_facts paf on af.dw_application_id = paf.application_id
JOIN underwriting_info ui ON ui.underwriting_info_id = coalesce(NULLIF(paf.final_uw_id, 0), NULLIF(paf.selected_uw_id, 0), NULLIF(paf. initial_uw_id, 0))
JOIN offer_facts of ON of.underwriting_info_id = ui.underwriting_info_id
JOIN offer_details o on of.offer_details_id = o.offer_details_id
JOIN products p ON p.product_id = of.product_id
WHERE o.rate_type_code = 'FIXED' AND o.eligible = TRUE
and af.application_type = {product} AND {date_range}
AND af.current_decision = 'ACCEPT'
AND af.challenger_name = {challenger_grid}
and of.asof_date_id >= 20191004 and of.asof_date_id <= 20191104
"""
df_rate.to_csv('/home/ec2-user/SageMaker/score_new/raw_am_asof_date_2.csv', index=None)
#af.date_start >= '2019-10-04'
df_rate['id'].nunique()
#Out[15]: 6952
df_rate.shape
#Out[14]: (92623, 8)

#af.date_start >= '2019-08-04'
df_rate['id'].nunique()
#Out[41]: 7289
df_rate.shape
#Out[40]: (97205, 8)


#Check population with diff > 0.05% what is the signed percentage
take_cols = ['id', 'applicant_id', 'date_signed', 'date_doc_upload', 'date_start', 'date_fund',
           'interest_rate_type', 'initial_term', 'interest_rate', 'requested_amount', 'challenger_name', 
           'gross_income', 'credit_score', 'free_cash_flow_pre', 'docs_ind', 'signed_ind', 'initial_term', 'consolidated_channel']
df = pd.read_csv('/home/ec2-user/SageMaker/score_new/raw_am.csv', usecols=take_cols)
df.shape
#Out[125]: (8852, 17)
#Out[135]: (8849, 15)
df = df[df['interest_rate_type'] != 'VARIABLE']
df.shape
#Out[126]: (8615, 17)
#Out[137]: (8611, 15)

df_rate = pd.read_csv('/home/ec2-user/SageMaker/score_new/raw_am_asof_date.csv')
df_rate.sort_values(by=['id', 'product_term', 'max_amount'], inplace=True)
df2 = df_rate.drop_duplicates(subset=['id', 'product_term', 'min_rate'], keep='last') #For each unique id, product_term and min_rate, keep the largest max_amount
df4 = df2[df2['max_amount'] >= df2['requested_amount']]
df4 = df4.sort_values(by=['id', 'product_term', 'max_amount'])
df4 = df4.drop_duplicates(subset=['id', 'product_term'], keep='first')
df4_new = flat_term_rate_dat(df4, flat_cols=['min_rate', 'tier'], consolidate_cols=[])
df4_new.shape
df_rate_id = df_rate.drop_duplicates(subset=['id', 'asof_date_id'])
df4_new = pd.merge(df4_new, df_rate_id[['id', 'asof_date_id']], on='id')
df4_new.shape
#Out[15]: (6379, 12)
#2, (6716, 12)
df4_new.to_csv('/home/ec2-user/SageMaker/score_new/rate_am_asof_date_invest.csv')

df4_new = pd.read_csv('/home/ec2-user/SageMaker/score_new/rate_am_asof_date_invest.csv')
credit_score_bins= [680, 700, 720, 740, 760, 780, 800, np.inf]
df['fico_bin'] = pd.cut(df['credit_score'], bins=credit_score_bins, include_lowest=True, right=False).astype(str)  
df['loan_amt_bin'] = np.where(df['requested_amount'] <= 20000, '[5000, 20000]',
                      np.where(df['requested_amount'] <= 50000, '(20000, 50000]', '(50000, 100000]'))
df = pd.merge(df, df4_new, on='id')
df.shape
#(6178, 32)
#(6150, 29)
#(6495, 30)

#Read grid
grid_dir = '~/SageMaker/price_grid'
grid = read_grid(pjoin(grid_dir, 'am_grid.xlsx'))

#Check conversion on the population with abs diff > 5 bps
result = []
for term in range(2, 8):
    df_term_check = pd.merge(df[df[f'tier_{term}'].notnull()], grid, left_on=[f'tier_{term}', 'fico_bin', 'loan_amt_bin'], right_on=['tier','fico_bin', 'loan_amt_bin'])
    notnull_shape = df[df[f'tier_{term}'].notnull()].shape[0]
    df_diff = df_term_check[(df_term_check[f'min_rate_{term}'] - df_term_check[f'grid_rate_{term}']).abs() > 0.0001]
    diff_pct = df_diff.shape[0] / df_term_check.shape[0]
    result.append([notnull_shape, df_diff.shape[0], diff_pct])
    
pd.DataFrame(result)

for term in range(2, 8):
    df_term_check = pd.merge(df[df[f'tier_{term}'].notnull()], grid, left_on=[f'tier_{term}', 'fico_bin', 'loan_amt_bin'], right_on=['tier','fico_bin', 'loan_amt_bin'])
    compare_dat_3(df_term_check, f'min_rate_{term}', f'grid_rate_{term}', output_dir='/home/ec2-user/SageMaker/score_new/redevelopment', out_nm=f'term_{term}_v2')
    
for term in range(2, 8):
    df_term_check = pd.merge(df[df[f'tier_{term}'].notnull()], grid, left_on=[f'tier_{term}', 'fico_bin', 'loan_amt_bin'], right_on=['tier','fico_bin', 'loan_amt_bin'])
    df = pd.merge(df, df_term_check[['id', f'grid_rate_{term}']], on='id', how='left')
    df_diff = df_term_check[(df_term_check[f'min_rate_{term}'] - df_term_check[f'grid_rate_{term}']).abs() > 0.0005]
    df_diff[f'term_diff_ind_{term}'] = 1
    df = pd.merge(df, df_diff[['id', f'term_diff_ind_{term}']], on='id', how='left')

df = df[['id', 'applicant_id',
'date_start',
'asof_date_id',
'date_doc_upload',
'date_signed',
'date_fund',
'interest_rate_type',
'initial_term',
'interest_rate',
'requested_amount',
'challenger_name',
'gross_income',
'credit_score',
'consolidated_channel',
'free_cash_flow_pre',
'docs_ind',
'signed_ind',
'fico_bin',
'loan_amt_bin',
'min_rate_2',
'min_rate_3',
'min_rate_4',
'min_rate_5',
'min_rate_6',
'min_rate_7',
'grid_rate_2',
'grid_rate_3',
'grid_rate_4',
'grid_rate_5',
'grid_rate_6',
'grid_rate_7',
'tier_2',
'tier_3',
'tier_4',
'tier_5',
'tier_6',
'tier_7'] + [f'term_diff_ind_{term}' for term in range(2,8)]]
df.shape
#Out[185]: (6178, 44)
df_out = df[(df['term_diff_ind_2']==1) | (df['term_diff_ind_3']==1) | (df['term_diff_ind_4']==1)| (df['term_diff_ind_5']==1)| (df['term_diff_ind_6']==1)| (df['term_diff_ind_7']==1)]
df_out.shape
#Out[186]: (443, 44)
df_out.to_csv('/home/ec2-user/SageMaker/score_new/check.csv', index=None)


#Check if the map_interest_rate from min_rate match with interest_rate from application_file table
terms = [2, 3, 4, 5, 6, 7]
rate_cols = ['min_rate_{}'.format(term) for term in terms]
df['map_min_rate'] = df.apply(lambda x: map_term_to_rate(x, rate_cols=rate_cols, terms=terms), axis=1)
stat, fig, df_diff = compare_dat_3(df, 'interest_rate', 'map_min_rate', return_diff_df=True)
df_significant = df_diff[df_diff['diff'].abs() >= 0.0001]

#Example of signed people
term = 7
df_term_check = pd.merge(df[df[f'tier_{term}'].notnull()], grid, left_on=[f'tier_{term}', 'fico_bin', 'loan_amt_bin'], right_on=['tier','fico_bin', 'loan_amt_bin'])
df_diff = df_term_check[(df_term_check[f'min_rate_{term}'] - df_term_check[f'grid_rate_{term}']).abs() > 0.0005]
df_diff_signed = df_diff[(df_diff['signed_ind'] == 1) & (df_diff['initial_term'] == term)]
df_diff_signed.shape
df_diff_signed['diff'] = (df_diff_signed[f'min_rate_{term}'] - df_diff_signed[f'grid_rate_{term}']).abs()
df_diff_signed[['id', 'date_start', 'date_signed', 'requested_amount', 'credit_score',
                'initial_term',  f'min_rate_{term}', f'grid_rate_{term}', f'tier_{term}',
                'map_min_rate', 'interest_rate', 'diff']].sort_values(by='diff', ascending=False).head()
id = 9674853

df_rate = pd.read_csv('/home/ec2-user/SageMaker/score_new/rate_am.csv')
df_rate.sort_values(by=['id', 'product_term', 'max_amount'], inplace=True)
df_rate[df_rate['id'] == 9674853]