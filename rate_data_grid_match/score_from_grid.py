#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 13 17:02:09 2019

@author: esun
"""

df = pd.read_csv("/home/ec2-user/SageMaker/score_new/redevelopment/PL_rate_oot.csv")

df_rate = pd.read_csv('/home/ec2-user/SageMaker/score_new/rate_am.csv')
df_rate.sort_values(by=['id', 'product_term', 'max_amount'], inplace=True)
df2 = df_rate.drop_duplicates(subset=['id', 'product_term', 'min_rate'], keep='last') #For each unique id, product_term and min_rate, keep the largest max_amount
df_max = df2.drop_duplicates(subset=['id', 'product_term'], keep='last')
df_max['max_ind'] = 1
df4 = pd.merge(df2, df_max[['id', 'product_term', 'max_amount', 'max_ind']], how='left', on=['id', 'product_term', 'max_amount'])
df4 = df4[(df4['max_amount'] >= df4['requested_amount']) | (df4['max_ind'] == 1)]
df4 = df4.sort_values(by=['id', 'product_term', 'max_amount'])
df4 = df4.drop_duplicates(subset=['id', 'product_term'], keep='first')
df4_new = flat_term_rate_dat(df4, flat_cols=['tier'], consolidate_cols=[])
df = pd.merge(df, df4_new, on='id')

#Read rates from grid and replace in a new file
grid_dir = '~/SageMaker/price_grid'
grid = read_grid(pjoin(grid_dir, 'am_grid.xlsx'))

credit_score_bins= [680, 700, 720, 740, 760, 780, 800, np.inf]
df['fico_bin'] = pd.cut(df['credit_score'], bins=credit_score_bins, include_lowest=True, right=False).astype(str)  
df['loan_amt_bin'] = np.where(df['requested_amount'] <= 20000, '[5000, 20000]',
                      np.where(df['requested_amount'] <= 50000, '(20000, 50000]', '(50000, 100000]'))

for idx, term in enumerate(range(2, 8)):
    if idx == 0:
        grid.rename(columns={'tier': f'tier_{term}'}, inplace=True)
    else:
        grid.rename(columns={'tier_{term-1}': f'tier_{term}'}, inplace=True)
    print(grid.columns[0])
    df = pd.merge(df, grid[[f'tier_{term}', 'fico_bin', 'loan_amt_bin'] + [f'grid_rate_{term}']], on=[f'tier_{term}', 'fico_bin', 'loan_amt_bin'])