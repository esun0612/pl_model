#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov  1 10:42:26 2019

@author: esun
"""
import numpy as np
import pandas as pd
import sys
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/erika_utils')
from util_data import add_signed_ind, clean_str

#Filter out observations for modeling data
df_rate =pd.read_csv('/home/ec2-user/SageMaker/input/180101_190930_process_rates.csv')
df = pd.read_csv('/home/ec2-user/SageMaker/input/1801_1909_correct_clean.csv')
print("Original shapes: ", df.shape, df_rate.shape)
#(714759, 91), (714650, 76)

df = pd.merge(df, df_rate, on='id')
print("Merge shapes: ", df.shape, df_rate.shape)
#Merge shapes:  (714650, 148) (714650, 58)

df = df.loc[df['requested_amount'] != 0]
print("Filter out requested_amount = 0: ", df.shape)

PL_terms = [2, 3, 4, 5, 6, 7]
df = df[(df['initial_term'].isin(PL_terms)) | df['initial_term'].isnull()]
print("Filter out initial terms not in PL_terms: ", df.shape)
#Filter out requested_amount = 0:  (620673, 148)
#Filter out initial terms not in PL_terms:  (620673, 148)

#Add signed_ind by term
df = add_signed_ind(df, terms=PL_terms)

#Change member_indicator to 0 / 1 variable
df['member_indicator'] = df['member_indicator'].map({False: 0, True: 1})

df['attr_affiliate_referrer'] = clean_str(df['attr_affiliate_referrer'])

df.shape
#(620673, 172)

df.to_csv('/home/ec2-user/SageMaker/input/1801_1909_correct_clean_filter.csv', index=None)