#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov  8 20:42:24 2019

@author: esun

For each experiment re evaluate the metrics
"""
from os.path import join as pjoin
import glob
import pandas as pd
from util_model import load_json
import sys
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/pl_model')
from header_to_model import report_performance_metrics, merge_all_pred


model_root_dir = '/home/ec2-user/SageMaker/experiment'
model_dirs = sorted(glob.glob(pjoin(model_root_dir, '[0-9]_*')))

for model_dir in model_dirs:
    json_file = glob.glob(pjoin(model_dir, 'params_rate*.json'))[0]
    params_rate = load_json(json_file) 
    exp_name = params_rate['exp_name']
    terms = params_rate['terms']
    act_term_prob_cols = ['signed_ind_{}'.format(term) for term in terms]  
    rate_map_cols = [f'rate_map_{term}' for term in terms]
    if 'term_col_postfix' in params_rate:
        rate_cols = ['{0}_{1}_{2}'.format(params_rate['term_col_prefix'], term, params_rate['term_col_postfix']) for term in terms]
    else:
        rate_cols = ['{0}_{1}'.format(params_rate['term_col_prefix'], term) for term in terms]
    pred_term_prob_cols = [col + '_prob_adj' for col in rate_cols]
    df_final_train = merge_all_pred(pjoin(model_dir, 'pred_train_rate.csv'), pjoin(model_dir, 'pred_train_term_final.csv'), 
                                        params_rate['dev_dat'], pjoin(model_dir, 'pred_train_final.csv'), act_term_prob_cols, rate_map_cols)
    df_final_oos = merge_all_pred(pjoin(model_dir, 'pred_oos_rate.csv'), pjoin(model_dir, 'pred_oos_term_final.csv'), 
                                  params_rate['oos_dat'], pjoin(model_dir, 'pred_oos_final.csv'), act_term_prob_cols, rate_map_cols)
    df_final_oot = merge_all_pred(pjoin(model_dir, 'pred_oot_rate.csv'), pjoin(model_dir, 'pred_oot_term_final.csv'), 
                                  params_rate['oot_dat'], pjoin(model_dir, 'pred_oot_final.csv'), act_term_prob_cols, rate_map_cols)
    final_result = report_performance_metrics([df_final_train, df_final_oos, df_final_oot], ['train', 'oos', 'oot'], terms, 
                                              act_term_prob_cols, pred_term_prob_cols, rate_map_cols, rate_cols, 'tier', 'tier_pre')
    final_result.to_csv(pjoin(model_dir, f"final_metrics_{params_rate['exp_name']}.csv"))
