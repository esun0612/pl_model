#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 28 17:26:28 2019

@author: esun
"""
from textwrap import wrap
from math import fabs as fabs
import numpy as np
import pandas as pd

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf
from metrics import calculate_gini
from util_analysis import gen_analysis_plot
from util_data import calc_payments


df = pd.read_csv('/home/ec2-user/SageMaker/input/1801_1909_correct_clean_filter.csv')
data_dict = pd.read_excel('/home/ec2-user/SageMaker/input/data_dict_3.xlsx')

rate_label = 'signed_ind'
term_labels = [f'signed_ind_{term}' for term in range(2,8)]
plot_vars = data_dict.loc[data_dict['var_type'].isin(['N', 'C']), 'var']
total_var_no = len(plot_vars)
   
pdf = matplotlib.backends.backend_pdf.PdfPages('/home/ec2-user/SageMaker/output/data_analysis_raw_4.pdf')
gini_list = []
for i, plot_var in enumerate(plot_vars):
    print(f"plotting variable: {plot_var},  {i} / {total_var_no}")
    plot_var_gini = calculate_gini(df, rate_label, plot_var)
    gini_list.append(plot_var_gini)
    plot_var_descript = data_dict.loc[data_dict['var'] == plot_var, 'Description'].values[0]
    plot_var_type = data_dict.loc[data_dict['var'] == plot_var, 'Category'].values[0]
    title = '{0}: {1}; Type: {2}; Gini: {3:.1%}'.format(plot_var, plot_var_descript, plot_var_type, plot_var_gini)
    title = "\n".join(wrap(title, 90))
    fig = gen_analysis_plot(df, rate_label, term_labels, plot_var, title)
    pdf.savefig(fig)
    plt.close()
pdf.close() 
gini_result = pd.Series(gini_list)
gini_result.index = plot_vars
gini_result.to_csv('/home/ec2-user/SageMaker/output/gini.csv')
