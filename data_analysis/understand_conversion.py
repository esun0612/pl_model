#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 11 09:15:47 2019

@author: esun

Try to understand why conversion is off when gini and accuracy both are high
"""

dir_4 = '/home/ec2-user/SageMaker/experiment/4_2019-11-10_20-30-16'
dir_6 = '/home/ec2-user/SageMaker/experiment/6_2_2019-11-10_21-51-49'

rate_4 = eread(pjoin(dir_4, 'pred_oot_rate.feather'))
rate_6 = eread(pjoin(dir_6, 'pred_oot_rate.feather'))

print(gini(rate_4['signed_ind'], rate_4['rate_pred']), accuracy_binary(rate_4['signed_ind'], rate_4['rate_pred']))
print(gini(rate_6['signed_ind'], rate_6['rate_pred']), accuracy_binary(rate_6['signed_ind'], rate_6['rate_pred']))
print(rate_4['signed_ind'].mean(), rate_4['rate_pred'].mean(), rate_4['rate_pred'].mean() - rate_4['signed_ind'].mean())
print(rate_6['signed_ind'].mean(), rate_6['rate_pred'].mean(), rate_6['rate_pred'].mean() - rate_6['signed_ind'].mean())
compare_dat_3(rate_4, 'signed_ind', 'rate_pred')
compare_dat_3(rate_6, 'signed_ind', 'rate_pred')