#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  5 10:49:39 2019

@author: esun
"""

import pandas as pd
import numpy as np
import sys
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/erika_utils')
from util_data import query_snowflake, query_sofidw

start_date = '2018-01-01'
end_date = '2019-09-30'
product = f"'PL'"
date_range = f"af.date_start>='{start_date}' and af.date_start<='{end_date}'"

#Check source data for funded loan amount
qry = f"""SELECT
af.id,
ica.certified_amount,
af.requested_amount,
paf.final_loan_amount
FROM dwmart.applications_file af
left JOIN product_application_facts paf on af.dw_application_id = paf.application_id
left JOIN 
(select dw_application_id, sum(disbursement_amount) as certified_amount
from inschool_loan_disbursement_schedule
group by dw_application_id) ica 
on ica.dw_application_id = af.dw_application_id
WHERE af.application_type = {product} AND {date_range}
AND (af.interest_rate_type = 'FIXED' OR af.interest_rate_type IS NULL) 
AND af.current_decision = 'ACCEPT'
"""
df_query = query_snowflake(qry)
df_query.head()

df_unmatch = df_query[(df_query['requested_amount'] != df_query['final_loan_amount']) & (df_query['final_loan_amount'].notnull())]
df_unmatch.shape