#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  6 13:23:51 2019

@author: esun

Prepare data for modeling
"""
import os
from os.path import join as pjoin
import pickle
import pandas as pd
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf
from textwrap import wrap
import sys
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/erika_utils')
from util_data import calc_payments, add_signed_ind, clean_str, divide_train_oos, map_term_to_rate, impute_rate
from util_analysis import compare_dat_2, gen_analysis_plot
from metrics import calculate_gini

#Add louis features for comparison
df = pd.read_csv('/home/ec2-user/SageMaker/input/1801_1909_correct_clean_filter_1106.csv')
model_lou = pickle.load(open('/home/ec2-user/SageMaker/input/louis_model/lgbm_new_pickle.pkl','rb'))
lou_vars = model_lou.feature_name() 
miss_vars = set(lou_vars) - set(df.columns)
have_vars = set(df.columns).intersection(set(lou_vars))
model_lou_term = pickle.load(open('/home/ec2-user/SageMaker/input/louis_model/lgbm_term2_new_pickle.pkl','rb'))
lou_vars_term = model_lou_term.feature_name() 
miss_vars_term = set(lou_vars_term) - set(lou_vars)
#Day effect and month indicator
df['date_start'] = pd.to_datetime(df['date_start'])
min_start_date = df['date_start'].min()
df['date_start_year'] =  df['date_start'].dt.year
df['date_start_month'] = df['date_start'].dt.month
df['date_start_day'] = df['date_start'].dt.day
df['date_start_weekday'] = df['date_start'].dt.weekday
df['ref_month'] = df['date_start_year'] * 12 + df['date_start_month'] - (min_start_date.year * 12 + min_start_date.month)
df['ref_day'] = df['date_start'].dt.dayofyear + (df['date_start_year'] - min_start_date.year) * 365

#Merge with min logic for rates
df_rate = pd.read_csv('/home/ec2-user/SageMaker/input/180101_190930_process_rates.csv', 
                      usecols=['id'] + [f'min_rate_{term}_min' for term in PL_terms])
df = pd.merge(df, df_rate, on='id')

#Day and weekday are not useful, ref_month impact month. This cannot be used
PL_terms = [2, 3, 4, 5, 6, 7]
rate_label = 'signed_ind'
term_labels = [f'signed_ind_{term}' for term in PL_terms]
pdf = matplotlib.backends.backend_pdf.PdfPages('/home/ec2-user/SageMaker/output/data_analysis_date_2.pdf')
gini_list = []
for i, plot_var in enumerate(['ref_month', 'ref_day']):
    print(f"plotting variable: {plot_var},  {i}")
    plot_var_gini = calculate_gini(df, rate_label, plot_var)
    gini_list.append(plot_var_gini)
    title = '{0}, Gini: {1:.1%}'.format(plot_var, plot_var_gini)
    title = "\n".join(wrap(title, 90))
    fig = gen_analysis_plot(df, rate_label, term_labels, plot_var, title)
    pdf.savefig(fig)
    plt.close()
pdf.close() 
gini_result = pd.Series(gini_list)
gini_result.index = ['ref_month', 'ref_day']
gini_result.to_csv('/home/ec2-user/SageMaker/output/gini_date_2.csv')

#Take ref_month and ref_day into experiments later

#Baseline model without date, competitor, loan_amt
#with granular attr, grad

#Term model has the rate difference, not useful
def add_rates_diff(df, terms, rate_col_postfix=None, rate_col_nm='min_rate'): 
    for start in terms[:-1]:
        for end in terms[1:]:
            if rate_col_postfix is not None:
                start_col_nm = f'{rate_col_nm}_{start}_{rate_col_postfix}'
                end_col_nm = f'{rate_col_nm}_{end}_{rate_col_postfix}'
                df[f'{rate_col_nm}_{end}_{start}_diff_{rate_col_postfix}'] = df[end_col_nm] - df[start_col_nm]
                df[f'{rate_col_nm}_{end}_{start}_pct_diff_{rate_col_postfix}'] = df[f'{rate_col_nm}_{end}_{start}_diff_{rate_col_postfix}'] / df[start_col_nm]
            else:
                start_col_nm = f'{rate_col_nm}_{start}'
                end_col_nm = f'{rate_col_nm}_{end}'
                df[f'{rate_col_nm}_{end}_{start}_diff'] = df[end_col_nm] - df[start_col_nm]
                df[f'{rate_col_nm}_{end}_{start}_pct_diff'] = df[f'{rate_col_nm}_{end}_{start}_diff'] / df[start_col_nm]
    return df
df = add_rates_diff(df, PL_terms, rate_col_postfix='min')
df = add_rates_diff(df, PL_terms)

plot_vars = list(df.columns)[153:163]
pdf = matplotlib.backends.backend_pdf.PdfPages('/home/ec2-user/SageMaker/output/data_analysis_date_rate_diff.pdf')
gini_list = []
for i, plot_var in enumerate(plot_vars):
    print(f"plotting variable: {plot_var},  {i}")
    plot_var_gini = calculate_gini(df, rate_label, plot_var)
    gini_list.append(plot_var_gini)
    title = '{0}, Gini: {1:.1%}'.format(plot_var, plot_var_gini)
    title = "\n".join(wrap(title, 90))
    fig = gen_analysis_plot(df, rate_label, term_labels, plot_var, title)
    pdf.savefig(fig)
    plt.close()
pdf.close() 
gini_result = pd.Series(gini_list)
gini_result.index = plot_vars
gini_result.to_csv('/home/ec2-user/SageMaker/output/gini_date_rate_diff.csv')

df = df.iloc[:, :153]
df.to_csv('/home/ec2-user/SageMaker/input/1801_1909_modeling.csv', index=None)

#Additional fix
df = pd.read_csv('/home/ec2-user/SageMaker/input/1801_1909_modeling.csv')
df['attr_affiliate_referrer_new'] = clean_str(df['attr_affiliate_referrer'])
df['attr_affiliate_referrer_new'] = np.where(df['attr_affiliate_referrer_new'].isin(['www.sofi.com','www.google.com','credit_karma','lending_tree']),
                                                 df['attr_affiliate_referrer_new'], 'other')
df['grad'] = np.where(df['g_program'].notnull(), 1, 0)
PL_terms = [2, 3, 4, 5, 6, 7]
PL_rate_cols_min = ['min_rate_{}_min'.format(term) for term in PL_terms]
PL_rate_cols_maxnear = ['min_rate_{}'.format(term) for term in PL_terms]
PL_rate_maps = {term: idx for idx, term in enumerate(PL_terms)}

df, rate_impute_vals_min = impute_rate(df, rate_cols=PL_rate_cols_min)
df, rate_impute_vals_maxnear = impute_rate(df, rate_cols=PL_rate_cols_maxnear)
df['map_interest_rate_min'] = df.apply(lambda x: map_term_to_rate(x, rate_cols=PL_rate_cols_min, terms=PL_terms), axis=1)
df['map_interest_rate'] = df.apply(lambda x: map_term_to_rate(x, rate_cols=PL_rate_cols_maxnear, terms=PL_terms), axis=1)
df.to_csv('/home/ec2-user/SageMaker/input/1801_1909_modeling.csv', index=None)
df.shape
#Out[147]: (573812, 169)
#Create term indicator
df = pd.read_csv('/home/ec2-user/SageMaker/input/1801_1909_modeling.csv')
for term in PL_terms:
    df[f'initial_term_{term}'] = np.where((df['initial_term'] == term), 1, 0)
df.to_csv('/home/ec2-user/SageMaker/input/1801_1909_modeling.csv', index=None)

#Flatten interest_rate to seperate column for metrics calculation
df = pd.read_csv('/home/ec2-user/SageMaker/input/1801_1909_modeling.csv')
"""flat_rate function
rate_map_cols = [f'rate_map_{term}' for term in PL_terms]
for term, col in zip(PL_terms, rate_map_cols):
    df[col] = np.where(df['initial_term'] == term, df['interest_rate'], 0)
"""
df.to_csv('/home/ec2-user/SageMaker/input/1801_1909_modeling.csv', index=None)

def flat_rate(df, terms, term_col='initial_term', rate_col='interest_rate'):
    rate_map_cols = [f'rate_map_{term}' for term in terms]
    for term, col in zip(terms, rate_map_cols):
        df[col] = np.where(df[term_col] == term, df[rate_col], 0)
    return df

#Add tier_min to the data
    
#Devide to train, OOS and OOT
df_oot = df.loc[df['ref_month'] >= 19]
df_train, df_oos = divide_train_oos(df.loc[df['ref_month'] < 19], random_state=612)
print(df_train.shape, df_oos.shape, df_oot.shape)
#(355365, 169) (152299, 169) (66148, 169)
print(df_train.shape[0] / df.shape[0], df_oos.shape[0] / df.shape[0], df_oot.shape[0] / df.shape[0])

dat_dir = '/home/ec2-user/SageMaker/input'
df_train.to_csv(pjoin(dat_dir, 'PL_rate_train.csv'), index=None)
df_oos.to_csv(pjoin(dat_dir, 'PL_rate_oos.csv'), index=None)
df_oot.to_csv(pjoin(dat_dir, 'PL_rate_oot.csv'), index=None)

for file_nm in ['train', 'oos', 'oot']:
    for model in ['rate', 'term']:
        df = pd.read_csv(pjoin(dat_dir, f'PL_{model}_{file_nm}.csv'))
        df = flat_rate(df, PL_terms)
        df.to_csv(pjoin(dat_dir, f'PL_{model}_{file_nm}.csv'), index=None)

#Select observations with non-missing "initial_term" variable to be used in term prediction model
df_train_term = df_train[df_train['initial_term'].notnull()]
df_oos_term = df_oos[df_oos['initial_term'].notnull()]
df_oot_term = df_oot[df_oot['initial_term'].notnull()]
print(df_train_term.shape, df_oos_term.shape, df_oot_term.shape)
#(282816, 169) (121210, 169) (43733, 169)
print(df_train_term.shape[0] / df_train.shape[0], df_oos_term.shape[0] / df_oos.shape[0], df_oot_term.shape[0] / df_oot.shape[0])
#0.7958465239964544 0.7958686531100008 0.6611386587651932
df_train_term.to_csv(pjoin(dat_dir, 'PL_term_train.csv'), index=None)
df_oos_term.to_csv(pjoin(dat_dir, 'PL_term_oos.csv'), index=None)
df_oot_term.to_csv(pjoin(dat_dir, 'PL_term_oot.csv'), index=None)

