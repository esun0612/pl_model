
select 
af.id,
af.dw_application_id,
af.date_start,
af.challenger_name,
af.credit_score,
af.requested_amount
from 
(os.asof_dt,
min(CASE WHEN term = 2 THEN os.min_rate END) AS rate_2,
min(CASE WHEN term = 3 THEN os.min_rate END) AS rate_3,
min(CASE WHEN term = 4 THEN os.min_rate END) AS rate_4,
min(CASE WHEN term = 5 THEN os.min_rate END) AS rate_5,
min(CASE WHEN term = 6 THEN os.min_rate END) AS rate_6,
min(CASE WHEN term = 7 THEN os.min_rate END) AS rate_7,
min(CASE WHEN term = 10 THEN os.min_rate END) AS rate_10,
min(CASE WHEN term = 15 THEN os.min_rate END) AS rate_15,
min(CASE WHEN term = 20 THEN os.min_rate END) AS rate_20,
min(SUBSTRING(os.min_tier_code,'[1-9]')::INT) AS tier,
os.term,
os.min_rate,
SUBSTRING(os.min_tier_code,'[1-9]')::INT AS tier,
os.max_amount,)os