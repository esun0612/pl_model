#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 20 09:38:14 2019

@author: esun
"""
from os.path import join as pjoin
import numpy as np
import pandas as pd
import sys
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/erika_utils')
from util_data import query_sofidw, query_snowflake, flat_term_rate_dat, read_grid

#1. select from offer_staging table for a specific data range and chacha
qry_os = """
SELECT
af.id,
af.dw_application_id,
os.asof_dt,
af.requested_amount,
os.term,
os.min_rate,
SUBSTRING(os.min_tier_code,'[1-9]')::INT AS tier,
os.max_amount
FROM offer_staging os 
join dwmart.applications_file af on af.id = os.app_id
where os.product_name = 'PL' 
and os.asof_dt <= '2019-11-04' and os.asof_dt >= '2019-10-04'
and os.rate_type = 'FIXED' 
and os.decision = 'ACCEPT' 
and os.challenger_name = 'O.G. Challenger'
"""

df_os = query_sofidw(qry_os)
#sofidw in 92.1907308101654[s]
print(df_os.shape, df_os['id'].nunique())
#(236282, 8) 8960
df_os.to_csv('/home//ec2-user/SageMaker/score_new/check_rate_match/qry_os.csv', index=None)

#2. merge with offer_facts on dw_application_id for asof_dt for selected offer
qry_os_of = """
SELECT
af.id,
af.dw_application_id,
af.date_start,
af.challenger_name,
af.credit_score,
os.asof_dt,
af.requested_amount,
os.term,
os.min_rate,
SUBSTRING(os.min_tier_code,'[1-9]')::INT AS tier,
os.max_amount,
ofg.asof_date_id
FROM offer_staging os 
join dwmart.applications_file af on af.id = os.app_id
join(
select of.application_id,
min(of.asof_date_id) as asof_date_id
from offer_facts of
where of.asof_date_id >= 20191004 and of.asof_date_id <= 20191104
group by of.application_id
) ofg on af.dw_application_id = ofg.application_id
where os.product_name = 'PL' 
and os.asof_dt <= '2019-11-04' and os.asof_dt >= '2019-10-04'
and os.rate_type = 'FIXED' 
and os.decision = 'ACCEPT' 
and os.challenger_name = 'O.G. Challenger'
and (af.interest_rate_type = 'FIXED' OR af.interest_rate_type IS NULL) 
"""

df_os_of = query_sofidw(qry_os_of)
#sofidw in 58.278879165649414[s]
#Without (af.interest_rate_type = 'FIXED' OR af.interest_rate_type IS NULL, time reduces to 23.962101221084595[s]
print(df_os_of.shape, df_os_of['id'].nunique())
#(228136, 12) 8684
df_os_of.to_csv('/home/ec2-user/SageMaker/score_new/check_rate_match/qry_os_of.csv', index=None)

df = df_os_of
df['asof_date_os'] = (df['asof_dt'].dt.year * 1e4 + df['asof_dt'].dt.month * 1e2 + df['asof_dt'].dt.day).astype(int)
df = df[df['asof_date_os'] == df['asof_date_id']]
print(df.shape, df['id'].nunique())
#(223564, 13) 8561
df.sort_values(by=['id', 'term', 'max_amount'], inplace=True)
df2 = df.drop_duplicates(subset=['id', 'term', 'min_rate'], keep='last') #For each unique id, product_term and min_rate, keep the largest max_amount
df4 = df2[df2['max_amount'] >= df2['requested_amount']]
df4 = df4.sort_values(by=['id', 'term', 'max_amount'])
df4 = df4.drop_duplicates(subset=['id', 'term'], keep='first')
df4_new = flat_term_rate_dat(df4, flat_cols=['min_rate', 'tier'], term_col='term', consolidate_cols=[])
df4_new.shape
#(7794, 12)
df_dedup = df.drop_duplicates(subset=['id'])
df4_new.to_csv('/home/ec2-user/SageMaker/score_new/check_rate_match/rate_am_1120.csv')

df_final = pd.merge(df_dedup[['id', 'date_start', 'challenger_name', 'credit_score', 'requested_amount']], df4_new, left_on='id', right_index=True)
df_final.shape
#Out[52]: (7794, 17)
df_final['date_start'] = pd.to_datetime(df_final['date_start'])
print(df_final['date_start'].min(), df_final['date_start'].max())
#2019-05-06 00:00:00 2019-11-19 00:00:00
df_final['challenger_name'].value_counts()
"""
O.G. Challenger    7310
O.G. Champion       179
Chase               139
Rothschild          103
Morgan               58
DEFAULT               5
"""

grid_dir = '~/SageMaker/price_grid'
grid = read_grid(pjoin(grid_dir, 'am_grid.xlsx'))

df = df_final
credit_score_bins= [680, 700, 720, 740, 760, 780, 800, np.inf]
df['fico_bin'] = pd.cut(df['credit_score'], bins=credit_score_bins, include_lowest=True, right=False).astype(str)  
df['loan_amt_bin'] = np.where(df['requested_amount'] <= 20000, '[5000, 20000]',
                      np.where(df['requested_amount'] <= 50000, '(20000, 50000]', '(50000, 100000]'))

result = []
for term in range(2, 8):
    df_term_check = pd.merge(df[df[f'tier_{term}'].notnull()], grid, left_on=[f'tier_{term}', 'fico_bin', 'loan_amt_bin'], right_on=['tier','fico_bin', 'loan_amt_bin'])
    df = pd.merge(df, df_term_check[['id', f'grid_rate_{term}']], on='id', how='left')
    df_diff = df_term_check[(df_term_check[f'min_rate_{term}'] - df_term_check[f'grid_rate_{term}']).abs() > 0.0001]
    df_diff[f'term_diff_ind_{term}'] = 1
    df = pd.merge(df, df_diff[['id', f'term_diff_ind_{term}']], on='id', how='left')
    diff_pct = df_diff.shape[0] / df_term_check.shape[0]
    result.append([df_term_check.shape[0], df_diff.shape[0], diff_pct])
    
pd.DataFrame(result)
df_out = df[(df['term_diff_ind_2']==1) | (df['term_diff_ind_3']==1) | (df['term_diff_ind_4']==1)| (df['term_diff_ind_5']==1)| (df['term_diff_ind_6']==1)| (df['term_diff_ind_7']==1)]
df_out.shape
#Out[72]: (170, 31)
df_out.to_csv('/home/ec2-user/SageMaker/score_new/check_rate_match/diff_1120.csv', index=None)





