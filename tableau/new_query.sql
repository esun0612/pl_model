


SELECT
af.id,
af.dw_application_id,
af.date_start,
af.challenger_name,
af.credit_score,
os.asof_dt,
af.requested_amount,
os.term,
os.min_rate,
SUBSTRING(os.min_tier_code,'[1-9]')::INT AS tier,
os.max_amount,
ofg.asof_date_id
FROM offer_staging os 
join dwmart.applications_file af on af.id = os.app_id
join(
select of.application_id,
min(of.asof_date_id) as asof_date_id
from offer_facts of
where of.asof_date_id >= 20191004 and of.asof_date_id <= 20191104
group by of.application_id
) ofg on af.dw_application_id = ofg.application_id
where os.product_name = 'PL' 
and os.asof_dt <= '2019-11-04' and os.asof_dt >= '2019-10-04'
and os.rate_type = 'FIXED' 
and os.decision = 'ACCEPT' 
and os.challenger_name = 'O.G. Challenger'
and (af.interest_rate_type = 'FIXED' OR af.interest_rate_type IS NULL) 

