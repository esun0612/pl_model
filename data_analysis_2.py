#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  4 13:38:16 2019

@author: esun
"""
import pandas as pd
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf
from textwrap import wrap
import sys
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/erika_utils')
from util_data import calc_payments, add_signed_ind, clean_str
from util_analysis import compare_dat_2, gen_analysis_plot
from metrics import calculate_gini

#11/04: Data analysis continue on additional features
#Feature engineering on rates
df_raw_rate = pd.read_csv('/home/ec2-user/SageMaker/input/180101_190930_raw_rates.csv')
#No of trials for each user?
df_raw_rate.sort_values(by=['id', 'product_term', 'max_amount'], inplace=True)
df2 = df_raw_rate.drop_duplicates(subset=['id', 'product_term', 'min_rate'], keep='last') #For each unique id, product_term and min_rate, keep the largest max_amount
#df4: max_amount just above requested amount or the maximum of max_amount
df4 = df2[df2['max_amount'] >= df2['requested_amount']]
df4 = df4.sort_values(by=['id', 'product_term', 'max_amount'])
df4 = df4.drop_duplicates(subset=['id', 'product_term'], keep='first')
df4 = def_monthly_payment(df4)
df4['total_interest'] = df4['total_payment'] / df4['requested_amount'] - 1
df4_new = flat_term_rate_dat(df4, flat_cols=['min_rate', 'tier', 'monthly_payment', 'total_payment', 'total_interest'])
df4_new.shape
#Out[42]: (667789, 25), ~6.5% population whose requested_amount > max(max_amount) for all terms are excluded. These 6.5% population has 0 conversion.
df4_new.to_csv('/home/ec2-user/SageMaker/input/180101_190930_process_rates_maxnear.csv')

def def_monthly_payment(df, request_amt_col='requested_amount', term_col='product_term', rate_col='min_rate'):
    """Calculate monthly payments
    """
    df['monthly_payment'], df['total_payment'] = calc_payments(df[request_amt_col], df[term_col], df[rate_col])
    return df

def flat_term_rate_dat(df_orig, flat_cols, term_col='product_term', 
                       id_col='id', term_values=[2, 3, 4, 5, 6, 7],
                       consolidate_cols=['tier'], rename_postfix=None):
    """Flatten the data based on term_col
    """
    df_flat = df_orig.copy()
    df_flat = df_flat[df_flat[term_col].isin(term_values)]
    df_flat = df_flat.pivot(index=id_col, columns=term_col, values=flat_cols)
    df_flat.columns = ['_'.join([col_level_0, str(col_level_1)]) for col_level_0, col_level_1 in zip(list(df_flat.columns.get_level_values(0)), list(df_flat.columns.get_level_values(1)))]
    for consolidate_col in consolidate_cols:
        orig_cols = [consolidate_col + '_{}'.format(term) for term in [2, 3, 4, 5, 6, 7]]
        df_flat[consolidate_col] = df_flat[orig_cols].min(axis=1, skipna=True)
        df_flat.drop(columns=orig_cols, inplace=True)
    if rename_postfix is not None:
        df_rename_dict = {col: col + '_' + rename_postfix for col in df_flat.columns}
        df_flat.rename(columns=df_rename_dict, inplace=True)
    return df_flat

#Merge with original data (data_filter.py)
#Filter out observations for modeling data
df_rate =pd.read_csv('/home/ec2-user/SageMaker/input/180101_190930_process_rates_maxnear.csv')
df = pd.read_csv('/home/ec2-user/SageMaker/input/1801_1909_correct_clean.csv')
print("Original shapes: ", df.shape, df_rate.shape)
#Original shapes:  (714759, 91) (667789, 26)

df = pd.merge(df, df_rate, on='id')
print("Merge shapes: ", df.shape)
#Merge shapes:  (667789, 116)

df = df.loc[df['requested_amount'] != 0]
print("Filter out requested_amount = 0: ", df.shape)
#Filter out requested_amount = 0:  (573812, 116)

PL_terms = [2, 3, 4, 5, 6, 7]
df = df[(df['initial_term'].isin(PL_terms)) | df['initial_term'].isnull()]
print("Filter out initial terms not in PL_terms: ", df.shape)
#Filter out initial terms not in PL_terms:  (573812, 116)

#Add signed_ind by term
df = add_signed_ind(df, terms=PL_terms)
#Change member_indicator to 0 / 1 variable
df['member_indicator'] = df['member_indicator'].map({False: 0, True: 1})
df['attr_affiliate_referrer'] = clean_str(df['attr_affiliate_referrer'])
df.shape
#(573812, 122)

#No of terms available
PL_terms = [2, 3, 4, 5, 6, 7]
rate_cols = [f'min_rate_{term}' for term in PL_terms]
monthly_payment_cols = [f'monthly_payment_{term}' for term in PL_terms]
total_interest_cols = [f'total_interest_{term}' for term in PL_terms]
df['no_terms'] = df[rate_cols].notnull().sum(axis=1)

#Minimum of all rates
df['min_all_rates'] = df[rate_cols].min(axis=1)
df['min_monthly_payment'] = df[monthly_payment_cols].min(axis=1)
df['min_total_interest'] = df[total_interest_cols].min(axis=1)
df['free_cash_flow_post_max'] = df['free_cash_flow_pre'] - df['min_monthly_payment']
df['payment_ratio_min'] = df['min_monthly_payment'] / df['free_cash_flow_pre']
for term in PL_terms:
    df[f'free_cash_flow_post_{term}'] = df['free_cash_flow_pre'] - df[f'monthly_payment_{term}']
    df[f'payment_ratio_{term}'] = df[f'monthly_payment_{term}'] / df['free_cash_flow_pre']

#One id could have different tiers under different terms after dedup
"""
id  requested_amount  product_term  min_rate  tier  max_amount  \
5563609    18000.0             3    0.1149     7    24480.72   
5563609    18000.0             4    0.1199     7    30656.78   
5563609    18000.0             7    0.0999     2    18508.05  
"""

#a. Comparison of tier_x and tier_y
stat, fig = compare_dat_3(df, 'tier_x', 'tier_y') #tier_y always bigger than tier_x
"""
Updated tier_y to be min
13843 / 573812 = 2.4% observations are different
126052 / 573812 = 22.0% observations are null for tier_x
0 / 573812 = 0.0% observations are null for tier_y
"""
"""
83608 / 573812 = 14.6% observations are different
126052 / 573812 = 22.0% observations are null for tier_x
0 / 573812 = 0.0% observations are null for tier_y
"""
df[df['tier_x'] > df['tier_y']].shape
Out[45]: (28, 141)
df[df['tier_x'] < df['tier_y']].shape
Out[46]: (83580, 141)
#14.6% population tier_y not equal not tier_x when tier_x is not missing

#b. Comparison of tier_max and tier_min
def flat_term_rate_dat(df_orig, flat_cols, term_col='product_term', 
                       id_col='id', term_values=[2, 3, 4, 5, 6, 7],
                       consolidate_cols=['tier'], rename_postfix=None):
    """Flatten the data based on term_col
    """
    df_flat = df_orig.copy()
    df_flat = df_flat[df_flat[term_col].isin(term_values)]
    df_flat = df_flat.pivot(index=id_col, columns=term_col, values=flat_cols)
    df_flat.columns = ['_'.join([col_level_0, str(col_level_1)]) for col_level_0, col_level_1 in zip(list(df_flat.columns.get_level_values(0)), list(df_flat.columns.get_level_values(1)))]
    for consolidate_col in consolidate_cols:
        orig_cols = [consolidate_col + '_{}'.format(term) for term in [2, 3, 4, 5, 6, 7]]
        #df_flat[consolidate_col] = df_flat[orig_cols].max(axis=1, skipna=True)
        df_flat[consolidate_col + '_max'] = df_flat[orig_cols].max(axis=1, skipna=True)
        df_flat[consolidate_col + '_min'] = df_flat[orig_cols].min(axis=1, skipna=True)
        df_flat.drop(columns=orig_cols, inplace=True)
    if rename_postfix is not None:
        df_rename_dict = {col: col + '_' + rename_postfix for col in df_flat.columns}
        df_flat.rename(columns=df_rename_dict, inplace=True)
    return df_flat
df4_new = flat_term_rate_dat(df4, flat_cols=['min_rate', 'tier', 'monthly_payment', 'total_payment', 'total_interest'])
stat_2, fig_2 = compare_dat_3(df4_new, 'tier_min', 'tier_max') 
"""
131109 / 667789 = 19.6% observations are different
0 / 667789 = 0.0% observations are null for tier_min
0 / 667789 = 0.0% observations are null for tier_max
"""
#Try tier_min and tier_x
df_try = pd.merge(df, df4_new[['tier_min', 'tier_max']], left_on='id', right_index=True)
stat_3, fig_3 = compare_dat_3(df_try, 'tier_y', 'tier_max') #Sanity check => same
stat_4, fig_4 = compare_dat_3(df_try, 'tier_x', 'tier_min') 
#13843 / 573812 = 2.4% observations are different, of course customer would tend to choose the rate corresponding to minimum tier
#Change the dedup tier to take the minimum

df.rename(columns={'tier_x': 'tier', 'tier_y': 'tier_pre'}, inplace=True)
df.to_csv('/home/ec2-user/SageMaker/input/1801_1909_correct_clean_filter_1106.csv', index=None)
df.shape
#Out[11]: (573812, 140)

#Data analysis on newly created features
PL_terms = [2, 3, 4, 5, 6, 7]
plot_vars = [f'min_rate_{term}' for term in PL_terms] \
                + [f'monthly_payment_{term}' for term in PL_terms] \
                + [f'total_payment_{term}' for term in PL_terms] \
                + [f'total_interest_{term}' for term in PL_terms] \
                + ['tier_pre', 'no_terms', 'min_all_rates', 'min_monthly_payment', 'min_total_interest',
                   'free_cash_flow_post_max', 'payment_ratio_min'] \
                + [f'free_cash_flow_post_{term}' for term in PL_terms] \
                + [f'payment_ratio_{term}' for term in PL_terms] 

rate_label = 'signed_ind'
term_labels = [f'signed_ind_{term}' for term in PL_terms]
total_var_no = len(plot_vars)
   
pdf = matplotlib.backends.backend_pdf.PdfPages('/home/ec2-user/SageMaker/output/data_analysis_add.pdf')
gini_list = []
for i, plot_var in enumerate(plot_vars):
    print(f"plotting variable: {plot_var},  {i} / {total_var_no}")
    plot_var_gini = calculate_gini(df, rate_label, plot_var)
    gini_list.append(plot_var_gini)
    title = '{0}, Gini: {1:.1%}'.format(plot_var, plot_var_gini)
    title = "\n".join(wrap(title, 90))
    fig = gen_analysis_plot(df, rate_label, term_labels, plot_var, title)
    pdf.savefig(fig)
    plt.close()
pdf.close() 
gini_result = pd.Series(gini_list)
gini_result.index = plot_vars
gini_result.to_csv('/home/ec2-user/SageMaker/output/gini_add.csv')

