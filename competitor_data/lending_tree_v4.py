#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 30 14:58:17 2019

@author: esun

Lending tree data

from v1: try OfferedInterestRate with Fee
"""
import re
import glob
from os.path import join as pjoin, basename
import numpy as np
import pandas as pd
pd.set_option('display.max_columns', 20)
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf
#%matplotlib inline
import sys
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/erika_utils')
from util_data import query_reporting, esave, eread, flat_term_rate_dat, query_snowflake, clean_str, gen_stat
from metrics import calculate_gini
from util_analysis import gen_analysis_plot, gen_analysis_plot_2

dat_dir = "../competitor_data/lending_tree"
#Lending tree data
#a. Exclude SoFi records
df = eread(pjoin(dat_dir, 'lt_20180101_20191014.feather'))
df = df[(df['LenderName'] == 'Confidential') & (df['SecuredFlag'] == 'Unsecured')]
#b. Take only term in range(2, 8)
df['term'] = df['LoanTermMonths'] / 12
PL_terms = list(range(2, 8))
df = df[df['term'].isin(PL_terms)]
df['term'] = df['term'].astype(int)
#c. calculate rate for fee
df['mp_fee'] = np.pmt(df['OfferedInterestRate']/(100*12), df['LoanTermMonths'], df['OfferedLoanAmount']+df['OriginationFee'])
df['rate_fee'] = np.rate(df['LoanTermMonths'], df['mp_fee'], df['OfferedLoanAmount'], 0) * 12
esave(df, pjoin(dat_dir, 'lt_20180101_20191014_cr_rate.feather'))
#d. dedup: Use APRRate for now
df = df[['QFormName', 'QFCompleteDate', 'RequestedLoanAmount',  'OfferedLoanAmount',
       'OfferedInterestRate', 'APRRate', 'OriginationFee', 'LoanTermMonths', 'term', 'mp_fee', 'rate_fee']]
df.sort_values(by=['QFormName', 'term', 'OfferedLoanAmount', 'rate_fee'], inplace=True)
df2 = df.drop_duplicates(subset=['QFormName', 'term', 'OfferedLoanAmount'], keep='first')
df3 = flat_term_rate_dat(df2, flat_cols=['rate_fee'], consolidate_cols=[], term_col='term', id_col=['QFormName', 'OfferedLoanAmount'])
df3.reset_index(inplace=True)
df3.columns = ['QFormName', 'OfferedLoanAmount'] + [f'lt_rate_{term}' for term in range(2, 8)]
df2_date = df2[['QFormName', 'QFCompleteDate']].drop_duplicates(subset=['QFormName'])
df4 = pd.merge(df3, df2_date, on='QFormName')
esave(df4.reset_index(drop=True), pjoin(dat_dir, 'lt_data_clean_20180101_20191014_v4.feather'))

#Merge lending_tree_data with email on QFormName
lt = eread(pjoin(dat_dir, 'lt_data_clean_20180101_20191014_v4.feather'))
qforum_email = eread(pjoin(dat_dir, 'qform_email_dedup.feather'))
print(lt.shape, lt['QFormName'].nunique(), qforum_email.shape, qforum_email['QFormName'].nunique())
#(3697064, 9) 439102 (540454, 3) 540454
df = pd.merge(lt, qforum_email, on='QFormName', how='left')
df = df[df['email'].notnull()]
print(df.shape, df['QFormName'].nunique())
#(3641291, 11) 432928
esave(df.reset_index(drop=True), pjoin(dat_dir, 'lt_email_v4.feather'))

#Merge lending_tree data to applications_file data on email to get id
df = eread(pjoin(dat_dir, 'id_email.feather'))
df['date_start'] = pd.to_datetime(df['date_start'])
df['date_submit'] = pd.to_datetime(df['date_submit'])
lt = eread(pjoin(dat_dir, 'lt_email_v4.feather'))
df2 = pd.merge(df, lt, on='email')
print(df2.shape, df2['id'].nunique(), df2['email'].nunique())
#(1097617, 15) 79155 64718
#Select records with condition on date
df2['QFCompleteDate'] = pd.to_datetime(df2['QFCompleteDate'])
df2['day_diff_qs_qf'] = (df2['date_start'] - df2['QFCompleteDate']).dt.days
df2['day_diff_qs_submit'] = (df2['date_submit'] - df2['date_start']).dt.days
df2 = df2[((df2['day_diff_qs_qf'] >= 0) & (df2['day_diff_qs_qf'] <= 30)) | 
          ((df2['day_diff_qs_qf'] < 0) & (-df2['day_diff_qs_qf'] <= df2['day_diff_qs_submit']) & (df2['day_diff_qs_qf'] >= -30))]
print(df2.shape, df2['id'].nunique(), df2['email'].nunique())
#(496097, 17) 45513 43778
#45513 / 730483 = 0.06230535139079212

#Merge with original data on id
df3 = df2[['id', 'email', 'QFormName', 'OfferedLoanAmount', 'lt_rate_2', 'lt_rate_3', 'lt_rate_4', 'lt_rate_5',
           'lt_rate_6', 'lt_rate_7', 'QFCompleteDate', 'day_diff_qs_qf']]
df = eread(pjoin(dat_dir, 'orig_dat.feather'))
df = pd.merge(df, df3, on='id')
print(df.shape, df['id'].nunique())
#(425087, 30) 38761
esave(df, pjoin(dat_dir, 'orig_dat_lt_rate.feather'))
#Dedup based on loan_amt difference and day_diff
df['loan_amt_diff_abs'] = (df['requested_amount'] - df['OfferedLoanAmount']).abs()
df['day_diff_qs_qf_abs'] = df['day_diff_qs_qf'].abs()
df = df.sort_values(by=['id', 'loan_amt_diff_abs', 'day_diff_qs_qf_abs'])
df = df.drop_duplicates(subset=['id'], keep='first')
print(df.shape, df['id'].nunique())
#(38761, 32) 38761
#Only select observations with loan diff ratio within 20%
df['loan_amt_diff'] = df['OfferedLoanAmount'] - df['requested_amount']
df['loan_amt_diff_ratio'] = df['loan_amt_diff'] / df['requested_amount']
gen_stat(df, 'loan_amt_diff_ratio')
df = df[(df['loan_amt_diff_ratio'] >= -0.2) & (df['loan_amt_diff_ratio'] <= 0.2)]
df.shape
#30839 / 38761 = 0.7956193080673873
esave(df, pjoin(dat_dir, 'final_lt_v4.feather'))

df = eread(pjoin(dat_dir, 'final_lt_v4.feather'))
for term in range(2, 8):
    df[f'rate_diff_{term}'] = df[f'min_rate_{term}_orig'] - df[f'lt_rate_{term}'] / 100

result = []
rate_diff_cols = [f'rate_diff_{term}' for term in range(2,8)]
for i, plot_var in enumerate(rate_diff_cols):
    result.append([calculate_gini(df, 'signed_ind', plot_var, dropna=True), df[plot_var].notnull().sum() / df.shape[0]])
"""
-0.22563325609907026 0.16741787995719706
-0.13593174017291776 0.8707156522585039
-0.18492076198803264 0.4692759168585233
-0.1628133229688482 0.8863127857582931
-0.17091530449161718 0.1348292746197996
-0.22880342709914225 0.046726547553422614
"""

rate_cols = [f'min_rate_{term}_orig' for term in range(2, 8)]
for i, plot_var in enumerate(rate_cols):
    print(calculate_gini(df, 'signed_ind', plot_var, dropna=True), df[plot_var].notnull().sum() / df.shape[0])
"""
-0.198059855094325 0.41849606018353386
-0.129498369455872 0.900256169136483
-0.1400810011220528 0.9096922727714907
-0.15107450184823001 0.9469502902169331
-0.05242833155778859 0.4207659132916113
-0.05794039209700608 0.41561010408897825
"""