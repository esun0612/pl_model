#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 20 09:43:00 2019

@author: esun

Second version 12/31 merge by exact date
"""
from os.path import join as pjoin
import numpy as np
import pandas as pd
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf
#%matplotlib inline
import sys
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/erika_utils')
from util_data import eread, esave
from gen_utils import save_fig_to_pdf
from metrics import calculate_gini
from util_analysis import gen_analysis_plot

#Clean informa data
dat_dir = '../competitor_data'
df = pd.read_csv('../competitor_data/informa_data.csv')
df['Month'] = pd.to_datetime(df['Month'])
condition = (df['Month'] >=  '2018-01-01') & (df['Product'] == 'PL') & (~df['Lender'].isin(['SoFi', 'CreditKarma - SoFi'])) \
            & (df['Product Type'] != 'Variable')
df = df.loc[condition]
select_cols = ['Date', 'Month', 'Credit Score Tier', 'Interest Rate', 'Loan Amount', 'Term']
df = df[select_cols]
df = df.dropna(subset=['Interest Rate'])
#date variable has more than 50% observations with value ############
df.columns = ['date', 'month', 'credit_score_bin', 'min_rate', 'informa_loan_amount', 'term_month']
df['term'] = df['term_month'] / 12
df['term'] = np.where(np.mod(df['term'], 1) ==0.5, df['term'] + 1e-5, df['term']) #To break the rounding to even (banker's rounding) https://stackoverflow.com/questions/977796/why-does-math-round2-5-return-2-instead-of-3
df['term'] = np.round(df['term'])
df['term'] = df['term'].astype(int)
df = df[df['term'] >= 2]
df_informa = pd.pivot_table(df, index=['month', 'credit_score_bin', 'informa_loan_amount'], columns='term', values='min_rate')
df_informa.reset_index(inplace=True)
months = np.sort(df_informa['month'].unique())
for month in months:
    print(month, df_informa[df_informa['month']==month]['credit_score_bin'].unique())
"""
18/01 - 18/09 ['660-699' '700-749' '750+']
18/10 - 19/06 ['680-699' '700-749' '750+']
19/07 - now ['680-699' '700-719' '720-739' '740-759' '760-779' '780-799' '800+']
"""  
df_informa.rename(columns={term: f'informa_rate_{term}' for term in range(2,8)}, inplace=True)
df_informa.shape
#Out[674]: (110, 9)
df_informa['credit_score_bin'] = np.where(df_informa['credit_score_bin'] == '660-699', '680-699', df_informa['credit_score_bin'])
#Delete some uncleaned data point
df_informa.loc[(df_informa['month'] == '2018-09-01') & (df_informa['informa_loan_amount'] == 7000), 'informa_rate_3'] = \
df_informa.loc[(df_informa['month'] == '2018-09-01') & (df_informa['informa_loan_amount'] == 1000), 'informa_rate_3'].values[0]
df_informa = df_informa[~((df_informa['month'] == '2018-09-01') & (df_informa['informa_loan_amount'] == 1000))]
df_informa = df_informa[~((df_informa['month'] == '2018-11-01') & (df_informa['informa_loan_amount'] == 22500))]
df_informa = df_informa[~((df_informa['month'] == '2019-01-01') & (df_informa['informa_loan_amount'] == 22000))]
df_informa.to_csv(pjoin(dat_dir, 'informa_data_clean.csv'), index=None)

#Merge with modeling data
df_informa = pd.read_csv(pjoin(dat_dir, 'informa_data_clean.csv'))
df = eread('../round_2/data_lr/PL_all.feather')
df['month'] = df['date_start'].values.astype('datetime64[M]')
cond_list_1 = [(df['month'] <= '2019-06-01') & (df['credit_score'] >= 680) & (df['credit_score'] <= 699), 
               (df['month'] <= '2019-06-01') & (df['credit_score'] >= 700) & (df['credit_score'] <= 749),
               (df['month'] <= '2019-06-01') & (df['credit_score'] >= 750),
               (df['month'] >='2019-07-01') & (df['credit_score'] >= 680) & (df['credit_score'] <= 699),
               (df['month'] >='2019-07-01') & (df['credit_score'] >= 700) & (df['credit_score'] <= 719),
               (df['month'] >='2019-07-01') & (df['credit_score'] >= 720) & (df['credit_score'] <= 739),
               (df['month'] >='2019-07-01') & (df['credit_score'] >= 740) & (df['credit_score'] <= 759),
               (df['month'] >='2019-07-01') & (df['credit_score'] >= 760) & (df['credit_score'] <= 779),
               (df['month'] >='2019-07-01') & (df['credit_score'] >= 780) & (df['credit_score'] <= 799),
               (df['month'] >='2019-07-01') & (df['credit_score'] >= 800)]
value_list_1 = ['680-699', '700-749', '750+', '680-699', '700-719', '720-739', '740-759', '760-779', '780-799', '800+']
df['credit_score_bin'] = np.select(cond_list_1, value_list_1)
df['month'] = df['month'].astype(str)
df2 = pd.merge(df, df_informa, on=['month', 'credit_score_bin'], how='left')  
df2['loan_abs_diff'] = (df2['requested_amount'] - df2['informa_loan_amount']).abs()
df2.sort_values(by=['id', 'loan_abs_diff'], inplace=True)
df2 = df2.drop_duplicates(subset=['id'], keep='first')
df2 = df2.reset_index(drop=True)
esave(df2, pjoin(dat_dir, 'informa_merge_pl.feather'))

#Create rate difference variable
df = eread(pjoin(dat_dir, 'informa_merge_pl.feather'))
for term in range(2, 8):
    df[f'rate_diff_informa_{term}'] = df[f'min_rate_{term}'] - df[f'informa_rate_{term}']

rate_cols = [f'min_rate_{term}' for term in range(2,8)]
informa_rate_cols = [f'informa_rate_{term}' for term in range(2,8)]
rate_diff_cols = [f'rate_diff_informa_{term}' for term in range(2,8)]
df[rate_cols + informa_rate_cols+ rate_diff_cols].isnull().sum() / df.shape[0]

#Rate missing pct by date
rate_cnts = df.groupby('date_start')[rate_cols + ['id']].count()
for term in range(2, 8):
    rate_cnts[f'missing_pct_{term}'] = 1 - rate_cnts[f'min_rate_{term}'] / rate_cnts['id']
rate_cnts.reset_index(inplace=True)

#Plot missing rate for min_rate
fig, ax = plt.subplots(figsize=(12,6))
for col in [f'missing_pct_{term}' for term in range(2, 8)]:
    ax.plot(rate_cnts['date_start'], rate_cnts[col], '.-', label=col, linewidth=0.4, markersize=1)
ax.legend(loc=8, ncol=6, bbox_to_anchor=(0.5, -0.15))
save_fig_to_pdf(fig, pjoin(dat_dir, 'missing_by_date.pdf'))

#Plot missing rate for informa_rate
rate_cnts = df.groupby('date_start')[informa_rate_cols + ['id']].count()
for term in range(2, 8):
    rate_cnts[f'missing_pct_{term}'] = 1 - rate_cnts[f'informa_rate_{term}'] / rate_cnts['id']
rate_cnts.reset_index(inplace=True)
fig, ax = plt.subplots(figsize=(12,6))
for col in [f'missing_pct_{term}' for term in range(2, 8)]:
    ax.plot(rate_cnts['date_start'], rate_cnts[col], '.-', label=col, linewidth=0.4, markersize=1)
ax.legend(loc=8, ncol=6, bbox_to_anchor=(0.5, -0.15))
save_fig_to_pdf(fig, pjoin(dat_dir, 'missing_by_date_informa.pdf'))

#Analysis before 19/06

        

#Analysis after 19/07
df_1907 = df[df['date_start'] >= '2019-07-01']
rate_label = 'signed_ind'
term_labels = [f'signed_ind_{term}' for term in range(2, 8)]
pdf = matplotlib.backends.backend_pdf.PdfPages(pjoin(dat_dir, 'informa_data_analysis_1907.pdf'))
for i, plot_var in enumerate(rate_diff_cols):
    print(f"plotting variable: {plot_var}")
    plot_var_gini = calculate_gini(df_1907, 'signed_ind', plot_var)
    title = '{0}, Gini: {1:.1%}'.format(plot_var, plot_var_gini)
    fig = gen_analysis_plot(df_1907, rate_label, term_labels, plot_var, multi_class_target_col='initial_term', title=title)
    pdf.savefig(fig)
    plt.close()
pdf.close() 

def gen_plot(df, pdf_file, plot_vars):
    """Generate plot
    """
    pdf = matplotlib.backends.backend_pdf.PdfPages(pdf_file)
    for i, plot_var in enumerate(plot_vars):
        print(f"plotting variable: {plot_var}")
        plot_var_gini = calculate_gini(df, 'signed_ind', plot_var)
        title = '{0}, Gini: {1:.1%}'.format(plot_var, plot_var_gini)
        fig = gen_analysis_plot(df, rate_label, term_labels, plot_var, multi_class_target_col='initial_term', title=title)
        pdf.savefig(fig)
        plt.close()
    pdf.close() 

gen_plot(df_1907, pjoin(dat_dir, 'rate_benchmark_1907.pdf'), rate_cols)

for i, plot_var in enumerate(rate_diff_cols):
    print(calculate_gini(df_1907, 'signed_ind', plot_var, dropna=True))

"""
-0.07330243929867186
-0.08935006060887918
-0.09112047397467271
-0.08867244000846675
-0.06805680379316315
-0.09272844594111473
"""

for i, plot_var in enumerate(rate_cols):
    print(calculate_gini(df_1907, 'signed_ind', plot_var, dropna=True))
"""
-0.1207645974410102
-0.12300572007268007
-0.12270854208545323
-0.1231481164402739
-0.10983235205660691
-0.106460115976304
"""














"""
       requested_amount
count     599415.000000
mean       32371.360808
std        21569.776018
min         5000.000000
1%          5000.000000
5%          7000.000000
10%        10000.000000
25%        16000.000000
50%        28000.000000
75%        45000.000000
90%        52160.852000
95%        80000.000000
99%       100000.000000
max       100000.000000
"""

def credit_score_bin_1(x):
    if x >= 660 and x <= 699:
        return '660-699'
    elif x >= 700 and x <= 749:
        return '700-749'
    elif x >= 750:
        return '750+'

def credit_score_bin_2(x):
    if x >= 680 and x <= 699:
        return '680-699'
    elif x >= 700 and x <= 749:
        return '700-749'
    elif x >= 750:
        return '750+'
    
def credit_score_bin_3(x):
    if x >= 680 and x <= 699:
        return '680-699'
    elif x >= 700 and x <= 719:
        return '700-719'
    elif x >= 720 and x <= 739:
        return '720-739'
    elif x >= 740 and x <= 759:
        return '740-759'
    elif x >= 760 and x <= 779:
        return '760-779'
    elif x >= 780 and x <= 799:
        return '780-799'
    elif x >= 800:
        return '800+'
    
def credit_score_bin_by_time(row):
    if row['month'] <= '2019-06-01':
        return credit_score_bin_2(row['credit_score'])
    elif row['month'] >= '2019-07-01':
        return credit_score_bin_3(row['credit_score'])

df['credit_score_bin'] = df.apply(credit_score_bin_by_time, axis=1)
