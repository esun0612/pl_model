#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 13 09:21:55 2020

@author: esun

"""
import re
import glob
from os.path import join as pjoin, basename
import pandas as pd
pd.set_option('display.max_columns', 20)
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf
#%matplotlib inline
import sys
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/erika_utils')
from util_data import query_reporting, esave, eread, flat_term_rate_dat, query_snowflake, clean_str, gen_stat, calc_monthly_payments
from metrics import calculate_gini

#1. QC match of interest rate and APRRate
dat_dir = "../competitor_data/lending_tree"
df = eread(pjoin(dat_dir, 'lt_20180101_20191014.feather'))

calc_monthly_payments(4800, 3, 0.0934)
calc_monthly_payments(4896, 3, 0.0796)

df['loan_amount_2'] = df['OfferedLoanAmount'] + df['OriginationFee']
df['mp_1'] = calc_monthly_payments(df['OfferedLoanAmount'], df['LoanTermMonths']/12, df['APRRate']/100)
df['mp_2'] = calc_monthly_payments(df['loan_amount_2'], df['LoanTermMonths']/12, df['OfferedInterestRate']/100)

df['mp_diff'] = df['mp_1'] - df['mp_2']
gen_stat(df, 'mp_diff')
df['SecuredFlag'].value_counts()
df2 = df[df['mp_diff'].abs() > 5]
gen_stat(df2, 'mp_diff')

#How to calculate apr from monthly payment
>>> import numpy as np
>>> monthly_payment = 200.0
>>> number_of_payments = 10
>>> initial_loan_amount = 1500.0
>>> np.rate(number_of_payments, -monthly_payment, initial_loan_amount, 0.0)
0.056044636451588969

np.rate(3*12, -153.33251649977015, 4800, 0) * 12
np.pmt(0.09310144213018837/12, 3*12, 4800)
np.pmt(0.00775845351084903/12, 3*12, 4800)
calc_monthly_payments(4800, 3, 0.007758453510849)

np.pmt(0.075/12, 12*15, 200000)
np.rate(12*15, -1854.0247200054619, 200000, 0) * 12