#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 30 14:58:17 2019

@author: esun

Lending tree data
"""
import re
import glob
from os.path import join as pjoin, basename
import pandas as pd
pd.set_option('display.max_columns', 20)
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf
#%matplotlib inline
import sys
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/erika_utils')
from util_data import query_reporting, esave, eread, flat_term_rate_dat, query_snowflake, clean_str, gen_stat
from metrics import calculate_gini
from util_analysis import gen_analysis_plot, gen_analysis_plot_2

dat_dir = "../competitor_data/lending_tree"
#1. Internal data query
#1a. QForumName and email
qry = """
      SELECT
      created_dt,
      CAST(partner_tracking_id AS UNSIGNED) AS QFormName,
      CASE -- if request starts with { email in form of "email":"{email}" else <EmailAddress IsMasked="N">{email}<
           WHEN LEFT(request,1)='{' THEN UPPER(substring_index(substring_index(substring_index(request,'\"email\"',-1),'\"',2),'\"',-1))
           ELSE UPPER(substring_index(substring_index(request, 'EmailAddress IsMasked=\"N\">', -1),'<', 1))
      END AS email
      FROM partner_offer_attempt
      WHERE partner_offer_account_uuid = '900a99f6-9d1d-494a-8860-60d201846141'
      AND created_dt >= '2018-01-01'
"""
#qry = """select created_dt, partner_tracking_id, request from partner_offer_attempt where created_dt >= '2018-01-01' limit 10"""
#df.to_csv(pjoin(dat_dir, 'partner_offer_attempt_test.csv'), index=None)
df = query_reporting(qry)
esave(df, pjoin(dat_dir, 'qform_email.feather'))
#Drop duplicated QFormName
df = df.drop_duplicates(subset=['QFormName'])
esave(df.reset_index(drop=True), pjoin(dat_dir, 'qform_email_dedup.feather'))

#2. Lending tree data
#Set up Workato recipe to mirror google drive lending tree data folder to s3 folder, trigger event to be new files added to google drive folder
#google drive folder: https://drive.google.com/drive/folders/12YIUljpk25MoI1iYN4f87Mdz2cSG5Qbi
#s3 folder: https://console.aws.amazon.com/s3/buckets/workato.sofi.com/esun/lending_tree_data/?region=us-west-2&tab=overview
#Workato recipe: https://www.workato.com/recipes/1067261-googledrivefolder_to_s3folder_lending_tree#jobs

#2-1.Copy s3 folder to SageMaker
#!aws s3 cp --recursive s3://workato.sofi.com/esun/lending_tree_data ../competitor_data/lending_tree/raw_data
dat_dir_lending_tree = pjoin(dat_dir, 'raw_data')
lending_tree_files = sorted(glob.glob(pjoin(dat_dir_lending_tree, 'LCR_*.csv')))

def select_lt_files_by_date(files, start_dt, end_dt):
    """Select lending tree files by date (between start_dt and end_dt)
    start_dt and end_dt must in string format'yyyymmdd' (example: '20180101')
    """
    output_files = []
    for file in files:
        file_base_nm = basename(file)
        file_date = re.search(r'LCR_(\d+)_\w+', file_base_nm).group(1)
        if file_date >= start_dt and file_date <= end_dt:
            output_files.append(file)
    return output_files

lt_files = select_lt_files_by_date(lending_tree_files, '20180101', '20191014')

def read_files_to_df(files):
    """Read files into a single DataFrame
    """
    df = pd.DataFrame()
    for iter, file in enumerate(files):
        print(f"Reading file {iter} / {len(files)}: {file}")
        df_iter = pd.read_csv(file)
        df = pd.concat([df, df_iter], axis=0)
    return df

df = read_files_to_df(lt_files)
df.reset_index(drop=True, inplace=True)
esave(df, pjoin(dat_dir, 'lt_20180101_20191014.feather'))

#2-2. Clean data
#a. Exclude SoFi records
df = eread(pjoin(dat_dir, 'lt_20180101_20191014.feather'))
df = df[df['LenderName'] == 'Confidential']
#b. Take only term in range(2, 8)
PL_terms = list(range(2, 8))
df['term'] = df['LoanTermMonths'] / 12
df['term'] = df['term'].astype(int)
df = df[df['term'].isin(PL_terms)]
#c. Take only records whose OfferedLoanAmount >= RequestedLoanAmount
df = df[df['OfferedLoanAmount'] >= df['RequestedLoanAmount']]
#d. dedup: Use OfferedInterestRate for now
df.sort_values(by=['QFormName', 'term', 'OfferedInterestRate'], inplace=True)
df2 = df.drop_duplicates(subset=['QFormName', 'term'], keep='first')
#Each QFormName only appear on one QFCompleteDate
df3 = flat_term_rate_dat(df2, flat_cols=['OfferedInterestRate'], consolidate_cols=[], term_col='term', id_col='QFormName')
df3.reset_index(inplace=True)
df3.columns = ['QFormName'] + [f'lt_rate_{term}' for term in range(2, 8)]
df2_date = df2[['QFormName', 'QFCompleteDate', 'RequestedLoanAmount']].drop_duplicates(subset=['QFormName'])
df4 = pd.merge(df3, df2_date, on='QFormName')
#e. Cut date
df4['QFCompleteDate'] = pd.to_datetime(df4['QFCompleteDate'])
df5 = df4[(df4['QFCompleteDate'] >= pd.to_datetime('2018-01-01')) & (df4['QFCompleteDate'] <= pd.to_datetime('2019-10-14'))]
esave(df5.reset_index(drop=True), pjoin(dat_dir, 'lt_data_clean_20180101_20191014.feather'))

#3. Merge lending_tree_data with email on QFormName, 98.7% data can find email
lt = eread(pjoin(dat_dir, 'lt_data_clean_20180101_20191014.feather'))
qforum_email = eread(pjoin(dat_dir, 'qform_email_dedup.feather'))
print(lt.shape, lt['QFormName'].nunique(), qforum_email.shape, qforum_email['QFormName'].nunique())
#(392860, 9) 392860 (540454, 3) 540454
df = pd.merge(lt, qforum_email, on='QFormName')
print(df.shape, df['QFormName'].nunique())
#(387690, 11) 387690, 387690 / 392860 = 0.9868400957083948
df.rename(columns={'EMAIL': 'email'}, inplace=True)
df['email'].nunique()
#311363 / 387690 = 0.8031236297041451
esave(df, pjoin(dat_dir, 'lt_email.feather'))

#4. Pull email and id data from applications_file
start_date = '2018-01-01'
end_date = '2019-10-14'
product = f"'PL'"
date_range = f"af.date_start>='{start_date}' and af.date_start<='{end_date}'"
PL_qry = f"""
SELECT
af.id,
af.date_start,
af.date_submit,
af.attr_affiliate_referrer,
upper(b.sofi_login_email) AS email
FROM dwmart.applications_file af
JOIN borrowers b ON b.user_id = af.applicant_id
WHERE af.application_type = {product} AND {date_range}
AND (af.interest_rate_type = 'FIXED' OR af.interest_rate_type IS NULL)
AND af.current_decision = 'ACCEPT'
"""
df = query_snowflake(PL_qry)
df['attr_affiliate_referrer'] = clean_str(df['attr_affiliate_referrer'])
print(df.shape, df['id'].nunique(), df['email'].notnull().sum(), df['email'].nunique())
#(730483, 5) 730483 730483 627653 
#627653/730483 = 0.859230125820861
esave(df, pjoin(dat_dir, 'id_email.feather'))

#5. Merge lending_tree data to applications_file data on email, and dedup by date (date difference minimum)
df = eread(pjoin(dat_dir, 'id_email.feather'))
lt = eread(pjoin(dat_dir, 'lt_email.feather'))
print(lt.shape, lt['QFormName'].nunique(), lt['email'].notnull().sum(), lt['email'].nunique())
#(387690, 10) 387690 387690 311363
#Check created_dt and QFCompleteDate -> match
#lt['day_diff'] = (lt['created_dt'] - lt['QFCompleteDate']).dt.days
#lt[lt['day_diff'].abs() > 1].shape
df2 = pd.merge(df, lt, on='email')
print(df2.shape, df2['id'].nunique(), df2['email'].nunique())
#(109981, 14) 72918 59626
df2['date_start'] = pd.to_datetime(df2['date_start'])
df2['date_submit'] = pd.to_datetime(df2['date_submit'])
df2['day_diff_qs_qf'] = (df2['date_start'] - df2['QFCompleteDate']).dt.days
df2['day_diff_qs_submit'] = (df2['date_submit'] - df2['date_start']).dt.days
df2 = df2[((df2['day_diff_qs_qf'] >= 0) & (df2['day_diff_qs_qf'] <= 30)) | 
          ((df2['day_diff_qs_qf'] < 0) & (-df2['day_diff_qs_qf'] <= df2['day_diff_qs_submit']) & (df2['day_diff_qs_qf'] >= -30))]
print(df2.shape, df2['id'].nunique(), df2['email'].nunique())
#(48272, 16) 41446 39968
#41446 / 730483 = 0.05673780224864918
df2['day_diff_qs_qf_abs'] = df2['day_diff_qs_qf'].abs()
df2 = df2.sort_values(by=['id', 'day_diff_qs_qf_abs'])
df2.drop_duplicates(subset=['id'], keep='first', inplace=True)
print(df2.shape, df2['id'].nunique(), df2['email'].nunique(), df2[df2['attr_affiliate_referrer'] == 'lending_tree'].shape)
#(41446, 17) 41446 39968 (23919, 17)
print(df[df['attr_affiliate_referrer'] == 'lending_tree'].shape)
#(35072, 5) (23919 / 35072 = 0.6819970346715328)
esave(df2.reset_index(drop=True), pjoin(dat_dir, 'id_lt.feather'))

#6/ Merge with original data on id
df2 = eread(pjoin(dat_dir, 'id_lt.feather'))
df3 = df2[['id', 'email', 'QFormName', 'lt_rate_2', 'lt_rate_3', 'lt_rate_4', 'lt_rate_5',
           'lt_rate_6', 'lt_rate_7', 'QFCompleteDate', 'RequestedLoanAmount']]

def read_concat(data_files):
    """Read data_files and concat on axis=0
    """
    df = pd.DataFrame()
    for data_file in data_files:
        df_iter = eread(data_file)
        df = pd.concat([df, df_iter])
    return df
        
df =  read_concat(['/home/ec2-user/SageMaker/round_2/data/PL_rate_train_7b.csv',
                   '/home/ec2-user/SageMaker/round_2/data/PL_rate_oos_7b.csv',
                   '/home/ec2-user/SageMaker/round_2/data/PL_rate_oot_7b.csv'])
print(df.shape, df['id'].nunique())
df = df[['id', 'date_start', 'initial_term', 'signed_ind', 'requested_amount', 'credit_score', 'free_cash_flow_pre'] + \
        [f'signed_ind_{term}' for term in range(2, 8)] + [f'min_rate_{term}_orig' for term in range(2, 8)]]
df.shape
#Out[1655]: (571570, 19)
esave(df.reset_index(drop=True), pjoin(dat_dir, 'orig_dat.feather'))

df = eread(pjoin(dat_dir, 'orig_dat.feather'))
df = pd.merge(df, df3, on='id')
print(df.shape)
#(35315, 29)

#Plot
df['loan_amt_diff'] = (df['RequestedLoanAmount'] - df['requested_amount']) / df['requested_amount']
gen_stat(df, 'loan_amt_diff')
df = df[df['loan_amt_diff'].abs() <= 0.2]
df.shape
#24523 / 35315 = 0.5753362593798669
esave(df, pjoin(dat_dir, 'final_lt.feather'))

for term in range(2, 8):
    df[f'rate_diff_{term}'] = df[f'min_rate_{term}_orig'] - df[f'lt_rate_{term}'] / 100

rate_diff_cols = [f'rate_diff_{term}' for term in range(2,8)]
result = []
for i, plot_var in enumerate(rate_diff_cols):
    result.append([calculate_gini(df, 'signed_ind', plot_var, dropna=True), df[plot_var].notnull().sum() / df.shape[0]])
"""
-0.16769448809400167 0.22512058273452112
-0.23859689089570368 0.9212520917413132
-0.19599312289729987 0.6186632542573088
-0.26317741017342167 0.9462053351707845
-0.2355612585589577 0.19391672408701643
-0.2654993342864582 0.06708337434786889
"""

rate_cols = [f'min_rate_{term}_orig' for term in range(2, 8)]
for i, plot_var in enumerate(rate_cols):
    print(calculate_gini(df, 'signed_ind', plot_var, dropna=True), df[plot_var].notnull().sum() / df.shape[0])
"""
-0.21604320690512002 0.4664337041047347
-0.15279279409983249 0.9278964465006398
-0.16522012915513484 0.9388227187715327
-0.1763716391108765 0.9549168225219018
-0.0775667081432323 0.4071266856974112
-0.08158465405341675 0.39674180529579683
"""
rate_label = 'signed_ind'
term_labels = [f'signed_ind_{term}' for term in range(2, 8)]

def gen_plot(df, pdf_file, plot_vars):
    """Generate plot
    """
    pdf = matplotlib.backends.backend_pdf.PdfPages(pdf_file)
    for i, plot_var in enumerate(plot_vars):
        print(f"plotting variable: {plot_var}")
        plot_var_gini = calculate_gini(df, 'signed_ind', plot_var)
        title = '{0}, Gini: {1:.1%}'.format(plot_var, plot_var_gini)
        fig = gen_analysis_plot(df, rate_label, term_labels, plot_var, multi_class_target_col='initial_term', title=title)
        pdf.savefig(fig)
        plt.close()
    pdf.close() 

gen_plot(df, pjoin(dat_dir, 'rate_diff_v1.pdf'), rate_diff_cols)

def gen_plot_2(df, pdf_file, plot_vars):
    """Generate plot
    """
    pdf = matplotlib.backends.backend_pdf.PdfPages(pdf_file)
    for i, plot_var in enumerate(plot_vars):
        print(f"plotting variable: {plot_var}")
        plot_var_gini_keep_na = calculate_gini(df, 'signed_ind', plot_var)
        plot_var_gini_drop_na = calculate_gini(df, 'signed_ind', plot_var, dropna=True)
        title = '{0}, Gini keep NA: {1:.1%}, Gini drop NA: {2:.1%}'.format(plot_var, plot_var_gini_keep_na, plot_var_gini_drop_na)
        fig = gen_analysis_plot_2(df, rate_label, plot_var, multi_class_target_col='initial_term', title=title)
        pdf.savefig(fig)
        plt.close()
    pdf.close() 
    
gen_plot_2(df, pjoin(dat_dir, 'rate_diff_v1b.pdf'), rate_diff_cols)

#Conclusion: the data is very useful!
#V2: Raw lending tree data keep all requested_amount, and when merge with internal application_files data, select records with min abs diff