#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 30 14:58:17 2019

@author: esun

Lending tree data

V2: Raw lending tree data keep all requested_amount, and when merge with internal application_files data, select records with min abs diff
"""
import re
import glob
from os.path import join as pjoin, basename
import pandas as pd
pd.set_option('display.max_columns', 20)
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf
#%matplotlib inline
import sys
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/erika_utils')
from util_data import query_reporting, esave, eread, flat_term_rate_dat, query_snowflake, clean_str, gen_stat
from metrics import calculate_gini
from util_analysis import gen_analysis_plot, gen_analysis_plot_2

dat_dir = "../competitor_data/lending_tree"

#1. Clean lending tree data
#a. Exclude SoFi records
df = eread(pjoin(dat_dir, 'lt_20180101_20191014.feather'))
df = df[df['LenderName'] == 'Confidential']
#b. Take only term in range(2, 8)
PL_terms = list(range(2, 8))
df['term'] = df['LoanTermMonths'] / 12
df['term'] = df['term'].astype(int)
df = df[df['term'].isin(PL_terms)]
#d. dedup: Use APRRate for now
df.sort_values(by=['QFormName', 'term', 'OfferedLoanAmount', 'APRRate'], inplace=True)
df2 = df.drop_duplicates(subset=['QFormName', 'term', 'OfferedLoanAmount'], keep='first')
#Each QFormName only appear on one QFCompleteDate
df3 = flat_term_rate_dat(df2, flat_cols=['APRRate'], consolidate_cols=[], term_col='term', id_col=['QFormName', 'OfferedLoanAmount'])
df3.reset_index(inplace=True)
df3.columns = ['QFormName', 'OfferedLoanAmount'] + [f'lt_rate_{term}' for term in range(2, 8)]
df2_date = df2[['QFormName', 'QFCompleteDate']].drop_duplicates(subset=['QFormName'])
df4 = pd.merge(df3, df2_date, on='QFormName')
#e. Cut date
esave(df4.reset_index(drop=True), pjoin(dat_dir, 'lt_data_clean_20180101_20191014_v2.feather'))

#2. Merge lending_tree_data with email on QFormName, 98.7% data can find email
lt = eread(pjoin(dat_dir, 'lt_data_clean_20180101_20191014_v2.feather'))
qforum_email = eread(pjoin(dat_dir, 'qform_email_dedup.feather'))
print(lt.shape, lt['QFormName'].nunique(), qforum_email.shape, qforum_email['QFormName'].nunique())
#(3699377, 9) 439216 (540454, 3) 540454
df = pd.merge(lt, qforum_email, on='QFormName', how='left')
df = df[df['email'].notnull()]
print(df.shape, df['QFormName'].nunique())
#(3643570, 11) 433039, 433039 / 439216 = 0.9859363046883538
df['email'].nunique()
#342648 / 433039 = 0.791263604432857
esave(df.reset_index(drop=True), pjoin(dat_dir, 'lt_email_v2.feather'))

#3. Merge lending_tree data to applications_file data on email to get id and subset based on day_diff
df = eread(pjoin(dat_dir, 'id_email.feather'))
df['date_start'] = pd.to_datetime(df['date_start'])
df['date_submit'] = pd.to_datetime(df['date_submit'])
lt = eread(pjoin(dat_dir, 'lt_email_v2.feather'))
print(lt.shape, lt['QFormName'].nunique(), lt['email'].notnull().sum(), lt['email'].nunique())
#(3643570, 11) 433039 3643570 342648
df2 = pd.merge(df, lt, on='email')
print(df2.shape, df2['id'].nunique(), df2['email'].nunique())
#(1098338, 15) 79156 64719
#Select records with condition on date
df2['QFCompleteDate'] = pd.to_datetime(df2['QFCompleteDate'])
df2['day_diff_qs_qf'] = (df2['date_start'] - df2['QFCompleteDate']).dt.days
df2['day_diff_qs_submit'] = (df2['date_submit'] - df2['date_start']).dt.days
df2 = df2[((df2['day_diff_qs_qf'] >= 0) & (df2['day_diff_qs_qf'] <= 30)) | 
          ((df2['day_diff_qs_qf'] < 0) & (-df2['day_diff_qs_qf'] <= df2['day_diff_qs_submit']) & (df2['day_diff_qs_qf'] >= -30))]
print(df2.shape, df2['id'].nunique(), df2['email'].nunique())
#(496436, 17) 45513 43778
#45513 / 730483 = 0.06230535139079212

#4. Merge with original data on id
df3 = df2[['id', 'email', 'QFormName', 'OfferedLoanAmount', 'lt_rate_2', 'lt_rate_3', 'lt_rate_4', 'lt_rate_5',
           'lt_rate_6', 'lt_rate_7', 'QFCompleteDate', 'day_diff_qs_qf']]
df = eread(pjoin(dat_dir, 'orig_dat.feather'))
df = pd.merge(df, df3, on='id')
print(df.shape, df['id'].nunique())
#(425413, 30) 38761
#Dedup based on loan_amt difference and day_diff
df['loan_amt_diff_abs'] = (df['requested_amount'] - df['OfferedLoanAmount']).abs()
df['day_diff_qs_qf_abs'] = df['day_diff_qs_qf'].abs()
df = df.sort_values(by=['id', 'loan_amt_diff_abs', 'day_diff_qs_qf_abs'])
df = df.drop_duplicates(subset=['id'], keep='first')
print(df.shape, df['id'].nunique())
#(38761, 32) 38761
#Only select observations with loan diff ratio within 20%
df['loan_amt_diff'] = df['OfferedLoanAmount'] - df['requested_amount']
df['loan_amt_diff_ratio'] = df['loan_amt_diff'] / df['requested_amount']
gen_stat(df, 'loan_amt_diff_ratio')
df = df[(df['loan_amt_diff_ratio'] >= -0.2) & (df['loan_amt_diff_ratio'] <= 0.2)]
df.shape
#30846 / 38761 = 0.7957999019633136
esave(df, pjoin(dat_dir, 'final_lt_v2.feather'))

df = eread(pjoin(dat_dir, 'final_lt_v2.feather'))
for term in range(2, 8):
    df[f'rate_diff_{term}'] = df[f'min_rate_{term}_orig'] - df[f'lt_rate_{term}'] / 100

rate_diff_cols = [f'rate_diff_{term}' for term in range(2,8)]
result = []
for i, plot_var in enumerate(rate_diff_cols):
    result.append([calculate_gini(df, 'signed_ind', plot_var, dropna=True), df[plot_var].notnull().sum() / df.shape[0]])
"""
-0.12016808343503205 0.1673474680671724
-0.19863872453338294 0.870777410361149
-0.17621634101867534 0.46897490760552424
-0.20410013241244596 0.8863385852298515
-0.2395265485675404 0.13479867730013617
-0.2885769244344052 0.046715943720417555
"""

rate_cols = [f'min_rate_{term}_orig' for term in range(2, 8)]
for i, plot_var in enumerate(rate_cols):
    print(calculate_gini(df, 'signed_ind', plot_var, dropna=True), df[plot_var].notnull().sum() / df.shape[0])
"""
-0.19812478845844772 0.41853076573948
-0.12936018634449142 0.9002788043830643
-0.14008650824750313 0.9097127666472152
-0.15109621201180967 0.946962328989172
-0.05247239297547612 0.4207352655125462
-0.057987123330541235 0.4155806263372885
"""
rate_label = 'signed_ind'
term_labels = [f'signed_ind_{term}' for term in range(2, 8)]

def gen_plot_2(df, pdf_file, plot_vars):
    """Generate plot
    """
    pdf = matplotlib.backends.backend_pdf.PdfPages(pdf_file)
    for i, plot_var in enumerate(plot_vars):
        print(f"plotting variable: {plot_var}")
        plot_var_gini_keep_na = calculate_gini(df, 'signed_ind', plot_var)
        plot_var_gini_drop_na = calculate_gini(df, 'signed_ind', plot_var, dropna=True)
        title = '{0}, Gini keep NA: {1:.1%}, Gini drop NA: {2:.1%}'.format(plot_var, plot_var_gini_keep_na, plot_var_gini_drop_na)
        fig = gen_analysis_plot_2(df, rate_label, plot_var, multi_class_target_col='initial_term', title=title)
        pdf.savefig(fig)
        plt.close()
    pdf.close() 
    
gen_plot_2(df, pjoin(dat_dir, 'rate_diff_v4.pdf'), rate_diff_cols)

