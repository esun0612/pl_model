#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 13 12:29:32 2020

@author: esun

For records in original data without lending tree rate, find the corresponding rate using KNN
KNN varialbe: date, FICO, loan amount
lending_tree v3 (m1_APRRate) selected
"""
from os.path import join as pjoin
import numpy as np
import pandas as pd
pd.set_option('display.max_columns', 20)
from sklearn.neighbors import KNeighborsRegressor
import sys
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/erika_utils')
from util_data import query_reporting, esave, eread, flat_term_rate_dat, query_snowflake, clean_str, gen_stat
from metrics import calculate_gini, gini
from util_analysis import compare_dat_3

dat_dir = "../competitor_data/lending_tree"
df = eread(pjoin(dat_dir, 'final_lt_v3.feather'))

print(calculate_gini(df, 'credit_score', 'lt_rate_3'))
#-0.34969868290383366
print(calculate_gini(df, 'free_cash_flow_pre', 'lt_rate_3')) 
#-0.08192128322461653 -> do not use free cash flow
print(calculate_gini(df, 'free_cash_flow_pre', 'min_rate_3_orig')) 
#-0.33165104008155927 -> only SoFi use free cash flow pre
print(calculate_gini(df, 'requested_amount', 'lt_rate_3'))
#0.21220024695750409

df['date_start'] = pd.to_datetime(df['date_start'])
min_start_date = df['date_start'].min()
#min_start_date = '2018-01-01'
df['date_start_year'] =  df['date_start'].dt.year
df['ref_day'] = df['date_start'].dt.dayofyear + (df['date_start_year'] - min_start_date.year) * 365
df_lt = df

df = eread(pjoin(dat_dir, 'orig_dat.feather'))
df['date_start'] = pd.to_datetime(df['date_start'])
min_start_date = pd.to_datetime('2018-01-01')
df['date_start_year'] =  df['date_start'].dt.year
df['ref_day'] = df['date_start'].dt.dayofyear + (df['date_start_year'] - min_start_date.year) * 365

#QC term 3 -> pass
KNN = KNeighborsRegressor(n_neighbors=30, weights='distance', algorithm='ball_tree')
KNN.fit(df_lt.loc[df_lt['lt_rate_3'].notnull(), ['ref_day', 'credit_score', 'requested_amount']], df_lt.loc[df_lt['lt_rate_3'].notnull(), 'lt_rate_3'])
df_lt.loc[df_lt['lt_rate_3'].notnull(), 'pred_rate_3'] = KNN.predict(df_lt.loc[df_lt['lt_rate_3'].notnull(), ['ref_day', 'credit_score', 'requested_amount']])
gini(df_lt.loc[df_lt['lt_rate_3'].notnull(), 'lt_rate_3'], df_lt.loc[df_lt['lt_rate_3'].notnull(), 'pred_rate_3'])
#0.9959976236688411
compare_dat_3(df_lt.loc[df_lt['lt_rate_3'].notnull()], 'lt_rate_3', 'pred_rate_3')

#QC term 7 -> pass
KNN = KNeighborsRegressor(n_neighbors=30, weights='distance', algorithm='ball_tree')
KNN.fit(df_lt.loc[df_lt['lt_rate_7'].notnull(), ['ref_day', 'credit_score', 'requested_amount']], df_lt.loc[df_lt['lt_rate_7'].notnull(), 'lt_rate_7'])
df_lt.loc[df_lt['lt_rate_7'].notnull(), 'pred_rate_7'] = KNN.predict(df_lt.loc[df_lt['lt_rate_7'].notnull(), ['ref_day', 'credit_score', 'requested_amount']])
gini(df_lt.loc[df_lt['lt_rate_7'].notnull(), 'lt_rate_7'], df_lt.loc[df_lt['lt_rate_7'].notnull(), 'pred_rate_7'])
#0.9959976236688411
compare_dat_3(df_lt.loc[df_lt['lt_rate_7'].notnull()], 'lt_rate_7', 'pred_rate_7')

##############
#Code for model
##############
df[['ref_day', 'credit_score', 'requested_amount']].describe()

#Scaling data
for var, min_value, max_value in zip(['ref_day', 'credit_score', 'requested_amount'], [1, 680, 5000], [652, 850, 100000]):
    df[var + '_pp'] = (df[var]-min_value) / (max_value-min_value)
    df_lt[var + '_pp'] = (df_lt[var]-min_value) / (max_value-min_value)
    
for term in range(2, 8):
    print(f"fitting term {term}")
    KNN = KNeighborsRegressor(n_neighbors=30, weights='distance', algorithm='ball_tree')
    df_iter = df_lt.loc[df_lt[f'lt_rate_{term}'].notnull()].copy()
    df_iter.reset_index(drop=True, inplace=True)
    KNN.fit(df_iter[['ref_day_pp', 'credit_score_pp', 'requested_amount_pp']], df_iter[f'lt_rate_{term}'])
    df[f'pred_rate_{term}'] = KNN.predict(df[['ref_day_pp', 'credit_score_pp', 'requested_amount_pp']])
    
for term in range(2, 8):
    df[f'rate_diff_{term}'] = df[f'min_rate_{term}_orig'] - df[f'pred_rate_{term}'] / 100

rate_diff_cols = [f'rate_diff_{term}' for term in range(2,8)]
rate_cols = [f'min_rate_{term}_orig' for term in range(2, 8)]
result = []
for i, (plot_var, orig_var) in enumerate(zip(rate_diff_cols, rate_cols)):
    result.append([calculate_gini(df, 'signed_ind', plot_var, dropna=True), calculate_gini(df, 'signed_ind', orig_var, dropna=True)])
pd.DataFrame(result)

esave(df, pjoin(dat_dir, 'orig_dat_with_rate_diff.feather'))

#Merge with df_lt data
df = pd.merge(df, df_lt[['id', 'lt_rate_2', 'lt_rate_3', 'lt_rate_4', 'lt_rate_5', 'lt_rate_6', 'lt_rate_7']], how='left', on='id')
for term in range(2, 8):
    df[f'lt_rate_{term}'] = np.where(df[f'lt_rate_{term}'].notnull(), df[f'lt_rate_{term}'], df[f'pred_rate_{term}'])

for term in range(2, 8):
    df[f'rate_diff_{term}'] = df[f'min_rate_{term}_orig'] - df[f'lt_rate_{term}'] / 100
result = []
for i, (plot_var, orig_var) in enumerate(zip(rate_diff_cols, rate_cols)):
    result.append([calculate_gini(df, 'signed_ind', plot_var, dropna=True), calculate_gini(df, 'signed_ind', orig_var, dropna=True)])
pd.DataFrame(result)
esave(df, pjoin(dat_dir, 'orig_dat_with_rate_diff.feather'))

gen_plot_2(df, pjoin(dat_dir, 'rate_diff_entire.pdf'), rate_diff_cols)

KNN.kneighbors(df.loc[1:1, ['ref_day_pp', 'credit_score_pp', 'requested_amount_pp']], return_distance=False)

df.loc[1:1, ['ref_day', 'credit_score', 'requested_amount']]

df_iter.loc[[3013, 1314, 2117, 3372, 2072,  186,   10,  799, 1412, 3888, 3018,
         341, 3374,  620, 3670, 2289, 3677, 2224, 2327, 3682, 2082, 2053,
        3883, 1030,  127, 2059, 3881, 3675, 3666, 1902], ['ref_day', 'credit_score', 'requested_amount']]




