#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 31 17:30:38 2019

@author: esun
"""

import pandas as pd
import numpy as np
import sys
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/erika_utils')
from util_data import query_snowflake, calc_monthly_payments, query_sofidw

start_date = '2018-01-01'
end_date = '2019-09-30'
product = f"'PL'"
date_range = f"af.date_start>='{start_date}' and af.date_start<='{end_date}'"

#11/04: add final_loan_amount
qry = f"""SELECT
af.id,
af.requested_amount,
p.product_term,
of.min_rate,
SUBSTRING(o.min_tier_code,'[1-9]')::INT AS tier,
of.max_amount,
of.min_amount,
paf.final_loan_amount
FROM dwmart.applications_file af
left JOIN product_application_facts paf on af.dw_application_id = paf.application_id
JOIN underwriting_info ui ON ui.underwriting_info_id = coalesce(NULLIF(paf.final_uw_id, 0), NULLIF(paf.selected_uw_id, 0), NULLIF(paf. initial_uw_id, 0))
JOIN offer_facts of ON of.underwriting_info_id = ui.underwriting_info_id
JOIN offer_details o on of.offer_details_id = o.offer_details_id
JOIN products p ON p.product_id = of.product_id
WHERE o.rate_type_code = 'FIXED' AND o.eligible = TRUE
and af.application_type = {product} AND {date_range}
AND (af.interest_rate_type = 'FIXED' OR af.interest_rate_type IS NULL) 
AND af.current_decision = 'ACCEPT'
"""
df_query = query_sofidw(qry)
print(df_query.shape, df_query['id'].nunique())
# (7918223, 7) 714651
df_query.to_csv('/home/ec2-user/SageMaker/input/180101_190930_raw_rates.csv', index=None)

def def_monthly_payment(df, request_amt_col='requested_amount', max_amt_col='max_amount', term_col='product_term', rate_col='min_rate'):
    """Calculate monthly payments
    """
    df['take_loan_amt'] = np.where(df[max_amt_col] >= df[request_amt_col], df[request_amt_col], df[max_amt_col])
    df['monthly_payment'] = calc_monthly_payments(df['take_loan_amt'], df[term_col], df[rate_col])
    return df

def flat_term_rate_dat(df_orig, flat_cols, term_col='product_term', 
                       id_col='id', term_values=[2, 3, 4, 5, 6, 7],
                       consolidate_cols=['tier'], rename_postfix=None):
    """Flatten the data based on term_col
    """
    df_flat = df_orig.copy()
    df_flat = df_flat[df_flat[term_col].isin(term_values)]
    df_flat = df_flat.pivot(index=id_col, columns=term_col, values=flat_cols)
    df_flat.columns = ['_'.join([col_level_0, str(col_level_1)]) for col_level_0, col_level_1 in zip(list(df_flat.columns.get_level_values(0)), list(df_flat.columns.get_level_values(1)))]
    for consolidate_col in consolidate_cols:
        orig_cols = [consolidate_col + '_{}'.format(term) for term in [2, 3, 4, 5, 6, 7]]
        df_flat[consolidate_col] = df_flat[orig_cols].max(axis=1, skipna=True)
        df_flat.drop(columns=orig_cols, inplace=True)
    if rename_postfix is not None:
        df_rename_dict = {col: col + '_' + rename_postfix for col in df_flat.columns}
        df_flat.rename(columns=df_rename_dict, inplace=True)
    return df_flat

df = pd.read_csv('/home/ec2-user/SageMaker/input/180101_190930_raw_rates.csv')
df.sort_values(by=['id', 'product_term', 'max_amount'], inplace=True)
df2 = df.drop_duplicates(subset=['id', 'product_term', 'min_rate'], keep='last') #For each unique id, product_term and min_rate, keep the largest max_amount

#df3 is the minimum rate
df3 = df2.drop_duplicates(subset=['id', 'product_term'], keep='first')
df3 = def_monthly_payment(df3)

#df4: max_amount just above requested amount or the maximum of max_amount
df_max = df2.drop_duplicates(subset=['id', 'product_term'], keep='last')
df_max['max_ind'] = 1
df4 = pd.merge(df2, df_max[['id', 'product_term', 'max_amount', 'min_amount', 'max_ind']], how='left', on=['id', 'product_term', 'max_amount', 'min_amount'])
df4 = df4[(df4['max_amount'] >= df4['requested_amount']) | (df4['max_ind'] == 1)]
df4 = df4.sort_values(by=['id', 'product_term', 'max_amount'])
df4 = df4.drop_duplicates(subset=['id', 'product_term'], keep='first')
df4 = def_monthly_payment(df4)

#df5:max amount just below requested amount or the minimum of max_amount
df_min = df2.drop_duplicates(subset=['id', 'product_term'], keep='first')
df_min['min_ind'] = 1
df5 = pd.merge(df2, df_min[['id', 'product_term', 'max_amount', 'min_amount', 'min_ind']], how='left', on=['id', 'product_term', 'max_amount', 'min_amount'])
df5 = df5[(df5['max_amount'] <= df5['requested_amount']) | (df5['min_ind'] == 1)]
df5 = df5.sort_values(by=['id', 'product_term', 'max_amount'])
df5 = df5.drop_duplicates(subset=['id', 'product_term'], keep='last')
df5 = def_monthly_payment(df5)

#Flatten df3, df4 and df5
df3_new = flat_term_rate_dat(df3, flat_cols=['min_rate', 'tier', 'max_amount', 'monthly_payment', 'take_loan_amt'], rename_postfix='min')
df4_new = flat_term_rate_dat(df4, flat_cols=['min_rate', 'tier', 'max_amount', 'monthly_payment', 'take_loan_amt'], rename_postfix='maxnear')
df5_new = flat_term_rate_dat(df5, flat_cols=['min_rate', 'tier', 'max_amount', 'monthly_payment', 'take_loan_amt'], rename_postfix='minnear')

#Merge 3 DataFrames
df_3_rates = pd.merge(df3_new, pd.merge(df4_new, df5_new, on='id'), on='id')

#QC
print(df.shape, df_3_rates.shape, df['id'].nunique())
#(7918223, 7) (714650, 75) 714651

df_3_rates.to_csv('/home/ec2-user/SageMaker/input/180101_190930_process_rates.csv')
