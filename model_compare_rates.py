#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  6 16:24:15 2019

@author: esun
"""
from os.path import join as pjoin
import pandas as pd
import numpy as np
import pickle
import sys
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/erika_utils')
from util_model import Header

#Generate headers
def gen_header(orig_header_file, new_header_file, take_vars, label_vars):
    """From orig_header_file, only use take_vars as independent variables and label_vars as lable variable
    Output the new header to new_header_file
    """
    orig_header = pd.read_csv(orig_header_file)
    #Set I for all columns not in take_vars
    all_cols = list(orig_header.columns)
    for col in all_cols:
        if col not in take_vars:
            orig_header.at[0, col] = 'I'
        else:
            if orig_header.at[0, col] == 'I':
               orig_header.at[0, col] = 'N'
    orig_header.loc[0, label_vars] = 'L'
    header = Header(header=orig_header)
    assert len(header.num_cols) + len(header.cat_cols) == len(take_vars), "Header independent variable numbers do not match!"
    assert set(header.num_cols).union(set(header.cat_cols)) == set(take_vars), "Header independent variable set do not match!"
    if not isinstance(label_vars, list):
        label_vars = [label_vars]
    assert len(header.label_cols) == len(label_vars), "Header dependent variable numbers do not match!"
    assert set(header.label_cols) == set(label_vars), "Header dependent variable set do not match!"
    orig_header.to_csv(new_header_file, index=None)

def add_cols_to_header(orig_header_file, new_header_file, add_vars, add_cat_vars=None):
    """Based on orig_header_file, add add_vars to indepedent variables and output to new_header_file
    """
    new_header = pd.read_csv(orig_header_file)
    if add_vars is None:
        add_vars = []
    if add_cat_vars is None:
        add_cat_vars = []
        
    for var in add_vars:
        new_header.at[0, var] = 'N'
    for var in add_cat_vars:
        new_header.at[0, var] = 'C'
        
    orig_header = Header(header_file=orig_header_file)
    new_header = Header(header=new_header)
    
    assert len(orig_header.num_cols) + len(add_vars) == len(new_header.num_cols), "Header numerical variable numbers do not match!"
    assert set(orig_header.num_cols).union(set(add_vars)) == set(new_header.num_cols), "Header numerical independent variable set do not match!"
    assert len(orig_header.cat_cols) + len(add_cat_vars) == len(new_header.cat_cols), "Header categorical variable numbers do not match!"
    assert set(orig_header.cat_cols).union(set(add_cat_vars)) == set(new_header.cat_cols), "Header categorical independent variable set do not match!"
    new_header.header.to_csv(new_header_file, index=None)
    
    
df_test = pd.read_csv('/home/ec2-user/SageMaker/input/1801_1909_modeling.csv', nrows=2)
model_lou = pickle.load(open('/home/ec2-user/SageMaker/input/louis_model/lgbm_new_pickle.pkl','rb'))
lou_vars = model_lou.feature_name() 
miss_vars = set(lou_vars) - set(df_test.columns)
have_vars = set(df_test.columns).intersection(set(lou_vars))

model_lou_term = pickle.load(open('/home/ec2-user/SageMaker/input/louis_model/lgbm_term2_new_pickle.pkl','rb'))
lou_vars_term = model_lou_term.feature_name() 

PL_terms = [2, 3, 4, 5, 6, 7]
rate_label = 'signed_ind'
term_labels = 'initial_term'
header_dir = '/home/ec2-user/SageMaker/headers'

#exp_1
take_vars = list(have_vars) + ['free_cash_flow_pre'] + [f'min_rate_{term}_min' for term in PL_terms]
gen_header(pjoin(header_dir, 'orig_header.csv'), pjoin(header_dir, 'exp_1_rate_header.csv'), take_vars, rate_label)
gen_header(pjoin(header_dir, 'orig_header.csv'), pjoin(header_dir, 'exp_1_term_header.csv'), take_vars, term_labels)

#exp_2
take_vars = list(have_vars) + ['free_cash_flow_pre'] + [f'min_rate_{term}' for term in PL_terms]
gen_header(pjoin(header_dir, 'orig_header.csv'), pjoin(header_dir, 'exp_2_rate_header.csv'), take_vars, rate_label)
gen_header(pjoin(header_dir, 'orig_header.csv'), pjoin(header_dir, 'exp_2_term_header.csv'), take_vars, term_labels)

#exp_3
add_cols_to_header(pjoin(header_dir, 'exp_2_rate_header.csv'), pjoin(header_dir, 'exp_3_rate_header.csv'), ['requested_amount'])
add_cols_to_header(pjoin(header_dir, 'exp_2_term_header.csv'), pjoin(header_dir, 'exp_3_term_header.csv'), ['requested_amount'])

#exp_4
add_cols_to_header(pjoin(header_dir, 'exp_3_rate_header.csv'), pjoin(header_dir, 'exp_4_rate_header.csv'), ['tier_pre'])
add_cols_to_header(pjoin(header_dir, 'exp_3_term_header.csv'), pjoin(header_dir, 'exp_4_term_header.csv'), ['tier_pre'])

#exp_5: Only tier in rate model
add_cols_to_header(pjoin(header_dir, 'exp_2_rate_header.csv'), pjoin(header_dir, 'exp_5_rate_header.csv'), ['tier_pre'])