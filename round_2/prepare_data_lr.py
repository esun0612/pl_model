#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 16 10:17:11 2019

@author: esun

Prepare data
"""

from os.path import join as pjoin
import numpy as np
import pandas as pd
import sys
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/erika_utils')
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/pl_model')
from util_model import score_model, load_json, score_term_model, Header
from util_data import add_signed_ind, clean_str, esave, eread, flat_term_rate_dat, flat_rate, query_sofidw, query_snowflake, impute_rate, divide_train_oos 
from util_data import calc_monthly_payments, calc_payments, check_trans_to_list, change_cols, cut_cat_var
from header_to_model import merge_all_pred, report_performance_metrics

#1. Query raw data and raw rates
start_date = '2018-01-01'
end_date = '2019-11-15'
product = f"'PL'"
date_range = f"af.date_start>='{start_date}' and af.date_start<='{end_date}'"

PL_qry = f"""
SELECT
af.id,
af.date_signed,
af.date_doc_upload,
af.date_start,
af.initial_term,
af.interest_rate,
af.requested_amount,
af.gross_income,
af.app_created_via_mobile,
af.g_program,
af.g_grad_year,
af.credit_score,
af.employer_name,
af.years_of_experience,
af.pl_funds_use,
af.tax_burden_amount,
af.consolidated_channel,
af.attr_affiliate_referrer,
af.campaign_welcome_bonus,
af.housing_status,
acaf.pil0438,
af.registration_date,
af.revolving_credit_amount,
af.tier,
af.ug_ctgry,
af.ug_grad_year,
af.ug_program,
af.member_indicator,
acaf.iln5020,
acaf.alx8220,
acaf.all8020,
acaf.iln5820,
acaf.all0416,
acaf.REV0416,
acaf.mtf5020,
acaf.mta5020,
acaf.mta5830,
acaf.mtj5030,
acaf.mtj5820,
acaf.all5820,
acaf.all7516,
acaf.all8220,
acaf.all5020,
acaf.bcc7110,
acaf.bcc8322,
acaf.bcx3421,
acaf.bcx3422,
acaf.bcx5020,
acaf.bcx5320,
acaf.bcx7110,
acaf.rev5020,
acaf.rev8320,
acaf.rta7300,
acaf.rtr5030,
acaf.all7517,
acaf.iln0416,
acaf.mta0416,
acaf.mta1370,
acaf.mta2800,
acaf.mta8150,
acaf.mta8153,
acaf.mta8157,
acaf.mta8160,
acaf.mtf0416,
acaf.mtf4260,
acaf.mtf8166,
acaf.mts5020,
af.free_cash_flow_pre,
CASE WHEN af.date_doc_upload IS NOT NULL THEN 1 ELSE 0 END AS docs_ind,
CASE WHEN af.date_signed IS NOT NULL THEN 1 ELSE 0 END AS signed_ind
FROM dwmart.applications_file af
JOIN dwmart.application_credit_attributes_file acaf ON af.dw_application_id = acaf.dw_application_id
WHERE af.application_type = {product} AND {date_range}
AND (af.interest_rate_type = 'FIXED' OR af.interest_rate_type IS NULL)
AND af.current_decision = 'ACCEPT'
"""

rate_qry = f"""SELECT
af.id,
af.requested_amount,
p.product_term,
of.min_rate,
SUBSTRING(o.min_tier_code,'[1-9]')::INT AS tier,
of.max_amount
FROM dwmart.applications_file af
left JOIN product_application_facts paf on af.dw_application_id = paf.application_id
JOIN underwriting_info ui ON ui.underwriting_info_id = coalesce(NULLIF(paf.final_uw_id, 0), NULLIF(paf.selected_uw_id, 0), NULLIF(paf. initial_uw_id, 0))
JOIN offer_facts of ON of.underwriting_info_id = ui.underwriting_info_id
JOIN offer_details o on of.offer_details_id = o.offer_details_id
JOIN products p ON p.product_id = of.product_id
WHERE o.rate_type_code = 'FIXED' AND o.eligible = TRUE
and af.application_type = {product} AND {date_range}
AND af.current_decision = 'ACCEPT'
"""
#1b. Raw data
df = query_snowflake(PL_qry)
df.shape
#(761360, 70)
dat_dir = '../round_2/data_lr'
esave(df, pjoin(dat_dir, 'entire_raw_1216.feather'))

#1c. raw rate data
df_rate = query_sofidw(rate_qry)
print(df_rate.shape, df_rate['id'].nunique())
#(8665182, 6) 771576
esave(df_rate, pjoin(dat_dir, 'entire_rate_raw_1216.feather'))

#2. Clean rate data
df_rate = eread(pjoin(dat_dir, 'entire_rate_raw_1216.feather'))
df_rate.sort_values(by=['id', 'product_term', 'max_amount'], inplace=True)
df2 = df_rate.drop_duplicates(subset=['id', 'product_term', 'min_rate'], keep='last') #For each unique id, product_term and min_rate, keep the largest max_amount
df4 = df2[df2['max_amount'] >= df2['requested_amount']]
df4 = df4.sort_values(by=['id', 'product_term', 'max_amount'])
df4 = df4.drop_duplicates(subset=['id', 'product_term'], keep='first')
df4_new = flat_term_rate_dat(df4, flat_cols=['min_rate', 'tier'], consolidate_cols=[])
df4_new.shape
#(720413, 12)
df4_new.to_csv(pjoin(dat_dir, 'entire_rate_1216.csv'))

#3. Clean main data
df = eread(pjoin(dat_dir, 'entire_raw_1216.feather'))
df4_new = eread(pjoin(dat_dir, 'entire_rate_1216.csv'))
df = pd.merge(df, df4_new, on='id')
df.shape
#(710278, 82)
#Preprocess data
PL_terms = [2, 3, 4, 5, 6, 7]
df = add_signed_ind(df, terms=PL_terms)
df.shape
#(693392, 88) (97.6% data)
df = df.loc[df['requested_amount'] != 0]
df.shape
#(599415, 88) (86.4% data)

def process_1(df):
    #Member indicator, grad to 0/1 variable
    df['member_indicator'] = df['member_indicator'].map({False: 0, True: 1})
    df['grad'] = np.where(df['g_program'].notnull(), 1, 0)
    #Define date related variables
    min_start_date = df['date_start'].min()
    df['date_start_year'] =  df['date_start'].dt.year
    df['date_start_month'] = df['date_start'].dt.month
    df['date_start_day'] = df['date_start'].dt.day
    df['date_start_weekday'] = df['date_start'].dt.weekday
    df['ref_month'] = df['date_start_year'] * 12 + df['date_start_month'] - (min_start_date.year * 12 + min_start_date.month)
    df['ref_day'] = df['date_start'].dt.dayofyear + (df['date_start_year'] - min_start_date.year) * 365
    #Clean affiliate variable
    df['attr_affiliate_referrer'] = clean_str(df['attr_affiliate_referrer'])
    df['attr_affiliate_referrer_new'] = np.where(df['attr_affiliate_referrer'].isin(['www.sofi.com','www.google.com','credit_karma','lending_tree']),
                                                     df['attr_affiliate_referrer'], 'other')
    #Create indicator variables for selected each term or not
    for term in PL_terms:
        df[f'initial_term_{term}'] = np.where((df['initial_term'] == term), 1, 0)
    #Map interest_rate to individual term columns
    df = flat_rate(df, PL_terms)
    #Consolidate tier columns
    df['tier_pre'] = df[[f'tier_{term}' for term in PL_terms]].min(axis=1)
    
    #Cut and group categorical variables
    g_program_list = ['Business - MBA', 'Other', 'Law - JD', 'Engineering/Computer Science - MS or higher',
                                  'Business - MS or MA or higher', 'Social Sciences - MS or MA or higher']
    channel_list = ['www.sofi.com', 'Affiliate', 'SEM', 'Organic Search', 'MKT-DM', 'P2P']
    affiliate_list = ['www.sofi.com', 'www.google.com', '2019_pl_sofi_mail', 'credit_karma', 'lending_tree', 'nerdwallet']
    df = cut_cat_var(df, 'g_program', g_program_list)
    df = cut_cat_var(df, 'consolidated_channel', channel_list)
    df = cut_cat_var(df, 'attr_affiliate_referrer', affiliate_list)
    
    #Clean date variables
    df['date_start'] = pd.to_datetime(df['date_start'])
    df['g_grad_year'] = df['date_start'].dt.year - df['g_grad_year']
    df['ug_grad_year'] = df['date_start'].dt.year - df['ug_grad_year']
    #registration_date: date_start - var
    df['registration_date'] = pd.to_datetime(df['registration_date'])
    df['registration_date'] = (df['date_start'] - df['registration_date']).dt.days
    
    #add rate_diff columns
    for term in range(2, 7):
        df[f'min_rate_diff_{term+1}_{term}'] = df[f'min_rate_{term+1}'] - df[f'min_rate_{term}']
    
    #Consolidated_channel variable
    df['consolidated_channel_model'] = df['consolidated_channel']
    df['consolidated_channel_model'] = np.where(df['consolidated_channel'].isin(['www.sofi.com', 'Organic Search']), 'combined_organic', df['consolidated_channel_model'])
    df['consolidated_channel_model'] = np.where(df['consolidated_channel'] == 'P2P', 'Rest', df['consolidated_channel_model'])
    df_channel_dummy = pd.get_dummies(df['consolidated_channel_model'])
    df = pd.concat([df, df_channel_dummy], axis=1)
    
    return df

df = process_1(df)
df.to_csv(pjoin(dat_dir, 'PL_all.csv'), index=None)
esave(df.reset_index(drop=True), pjoin(dat_dir, 'PL_all.feather'))

#Divide to dev, oos and oot
df_oot = df[df['date_start'] >= '2019-10-15']
df_oot.shape
#(27943, 120)
df_oot.to_csv(pjoin(dat_dir, 'PL_rate_oot.csv'), index=None)
df_oot_term = df_oot[df_oot['initial_term'].notnull()]
df_oot_term.shape
Out[39]: (17590, 120)
df_oot_term.to_csv(pjoin(dat_dir, 'PL_term_oot.csv'), index=None)

df_train, df_oos = divide_train_oos(df[df['date_start'] <= '2019-10-13'], random_state=612)
df_train.to_csv(pjoin(dat_dir, 'PL_rate_train.csv'), index=None)
df_oos.to_csv(pjoin(dat_dir, 'PL_rate_oos.csv'), index=None)
print(df_train.shape, df_oos.shape)
#(399312, 120) (171133, 120)

df_train_term = df_train[df_train['initial_term'].notnull()]
df_oos_term = df_oos[df_oos['initial_term'].notnull()]
print(df_train_term.shape, df_oos_term.shape)
#(308082, 120) (131859, 120)
df_train_term.to_csv(pjoin(dat_dir, 'PL_term_train.csv'), index=None)
df_oos_term.to_csv(pjoin(dat_dir, 'PL_term_oos.csv'), index=None)

#Try the new defined rate_diff columns
#a. Calculate rate_diff mins and maxs on training dataset
df = eread(pjoin(dat_dir, 'PL_rate_train.csv'))
rate_diff_cols = [f'min_rate_diff_{term+1}_{term}' for term in range(2, 7)]
df_rate_diff_mins = np.round(df[rate_diff_cols].min(), 5).to_list()
#[-0.04419, -0.0506, -0.04875, -0.0456, -0.02064]
df_rate_diff_maxs = np.round(df[rate_diff_cols].max(), 5).to_list()
#[0.0155, 0.0271, 0.0463, 0.02423, 0.0154]
rate_cols = [f'min_rate_{term}' for term in range(2, 8)]
df[rate_cols].max().max()
#0.18383

def process_16(in_data_file, out_data_file):
    df = pd.read_csv(in_data_file)
    for idx, term in enumerate(range(2, 7)):
        df[f'min_rate_diff_impute_{term+1}_{term}'] = np.where(df[f'min_rate_diff_{term+1}_{term}'].notnull(), 
                                                               df[f'min_rate_diff_{term+1}_{term}'],
                                                               np.where(df[f'min_rate_{term}'].isnull(), df_rate_diff_mins[idx], df_rate_diff_maxs[idx]))
    df.to_csv(out_data_file, index=None)
    
for model in ['rate', 'term']:
    for time in ['train', 'oos', 'oot']:
        process_16(pjoin(dat_dir, f'PL_{model}_{time}.csv'), pjoin(dat_dir, f'PL_{model}_{time}_16.csv'))

#b. Impute rate by maximum
def process_17(in_data_file, out_data_file):
    df = pd.read_csv(in_data_file)
    for term in range(2, 8):
        df[f'min_rate_{term}_impute'] = df[f'min_rate_{term}'].fillna(0.18383)
    for idx, term in enumerate(range(2, 7)):
        df[f'min_rate_diff_impute_{term+1}_{term}'] = df[f'min_rate_{term+1}_impute'] - df[f'min_rate_{term}_impute']
    df.to_csv(out_data_file, index=None)
        
process_17(pjoin(dat_dir, f'PL_term_train.csv'), pjoin(dat_dir, f'PL_term_train_17.csv'))
process_17(pjoin(dat_dir, f'PL_rate_oot.csv'), pjoin(dat_dir, f'PL_rate_oot_17.csv'))
        
df = eread(pjoin(dat_dir, f'PL_term_train_17.csv'))      
model_cols = ['credit_score', 'consolidated_channel_model', 'requested_amount'] + [f'min_rate_diff_impute_{term+1}_{term}' for term in range(2, 7)] + rate_cols
df[model_cols].corr().to_csv('../erika_git/optimization/residual_model/mnlogit/corr.csv')
             
        
        
        
        
        
        
        
        