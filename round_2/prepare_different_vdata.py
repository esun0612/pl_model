#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 14 14:42:09 2019

@author: esun

Prepare different version of data

Training and OOS: 70/30 split 2018/01/01 - 2019/09/14
OOT: 2019/09/15 - 2019/10/14
"""
from os.path import join as pjoin
import numpy as np
import pandas as pd
import sys
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/erika_utils')
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/pl_model')
from util_model import score_model, load_json, score_term_model, Header
from util_data import add_signed_ind, clean_str, esave, eread, flat_term_rate_dat, flat_rate, query_sofidw, query_snowflake, impute_rate, divide_train_oos 
from util_data import calc_monthly_payments, calc_payments, check_trans_to_list, change_cols
from header_to_model import merge_all_pred, report_performance_metrics
from metrics import calculate_gini, gini

#1. Query raw data and raw rates
start_date = '2018-01-01'
end_date = '2019-10-14'
product = f"'PL'"
date_range = f"af.date_start>='{start_date}' and af.date_start<='{end_date}'"

df = query_snowflake(PL_qry)
df.shape
#Out[14]: (730492, 70)
dat_dir = '/home/ec2-user/SageMaker/round_2/data'
esave(df, pjoin(dat_dir, 'entire_raw_1119.feather'))

df_rate = query_sofidw(rate_qry)
print(df_rate.shape, df_rate['id'].nunique())
#(8242836, 6) 739994
esave(df_rate, pjoin(dat_dir, 'entire_rate_raw_1119.feather'))

rate_qry = f"""SELECT
af.id,
af.requested_amount,
p.product_term,
of.min_rate,
SUBSTRING(o.min_tier_code,'[1-9]')::INT AS tier,
of.max_amount
FROM dwmart.applications_file af
left JOIN product_application_facts paf on af.dw_application_id = paf.application_id
JOIN underwriting_info ui ON ui.underwriting_info_id = coalesce(NULLIF(paf.final_uw_id, 0), NULLIF(paf.selected_uw_id, 0), NULLIF(paf. initial_uw_id, 0))
JOIN offer_facts of ON of.underwriting_info_id = ui.underwriting_info_id
JOIN offer_details o on of.offer_details_id = o.offer_details_id
JOIN products p ON p.product_id = of.product_id
WHERE o.rate_type_code = 'FIXED' AND o.eligible = TRUE
and af.application_type = {product} AND {date_range}
AND af.current_decision = 'ACCEPT'
"""

PL_qry = f"""
SELECT
af.id,
af.date_signed,
af.date_doc_upload,
af.date_start,
af.initial_term,
af.interest_rate,
af.requested_amount,
af.gross_income,
af.app_created_via_mobile,
af.g_program,
af.g_grad_year,
af.credit_score,
af.employer_name,
af.years_of_experience,
af.pl_funds_use,
af.tax_burden_amount,
af.consolidated_channel,
af.attr_affiliate_referrer,
af.campaign_welcome_bonus,
af.housing_status,
acaf.pil0438,
af.registration_date,
af.revolving_credit_amount,
af.tier,
af.ug_ctgry,
af.ug_grad_year,
af.ug_program,
af.member_indicator,
acaf.iln5020,
acaf.alx8220,
acaf.all8020,
acaf.iln5820,
acaf.all0416,
acaf.REV0416,
acaf.mtf5020,
acaf.mta5020,
acaf.mta5830,
acaf.mtj5030,
acaf.mtj5820,
acaf.all5820,
acaf.all7516,
acaf.all8220,
acaf.all5020,
acaf.bcc7110,
acaf.bcc8322,
acaf.bcx3421,
acaf.bcx3422,
acaf.bcx5020,
acaf.bcx5320,
acaf.bcx7110,
acaf.rev5020,
acaf.rev8320,
acaf.rta7300,
acaf.rtr5030,
acaf.all7517,
acaf.iln0416,
acaf.mta0416,
acaf.mta1370,
acaf.mta2800,
acaf.mta8150,
acaf.mta8153,
acaf.mta8157,
acaf.mta8160,
acaf.mtf0416,
acaf.mtf4260,
acaf.mtf8166,
acaf.mts5020,
af.free_cash_flow_pre,
CASE WHEN af.date_doc_upload IS NOT NULL THEN 1 ELSE 0 END AS docs_ind,
CASE WHEN af.date_signed IS NOT NULL THEN 1 ELSE 0 END AS signed_ind
FROM dwmart.applications_file af
JOIN dwmart.application_credit_attributes_file acaf ON af.dw_application_id = acaf.dw_application_id
WHERE af.application_type = {product} AND {date_range}
AND (af.interest_rate_type = 'FIXED' OR af.interest_rate_type IS NULL)
AND af.current_decision = 'ACCEPT'
"""

add_qry = f"""
SELECT
af.id,
af.adjusted_gross_income,
af.max_loan_amount
FROM dwmart.applications_file af
JOIN dwmart.application_credit_attributes_file acaf ON af.dw_application_id = acaf.dw_application_id
WHERE af.application_type = {product} AND {date_range}
AND (af.interest_rate_type = 'FIXED' OR af.interest_rate_type IS NULL)
AND af.current_decision = 'ACCEPT'
"""
df_add = query_snowflake(add_qry)
df_add.shape
#Out[14]: (730485, 3)
dat_dir = '/home/ec2-user/SageMaker/round_2/data'
esave(df_add, pjoin(dat_dir, 'entire_raw_1119_add.feather'))

#2. Clean rate data
df_rate = eread(pjoin(dat_dir, 'entire_rate_raw_1119.feather'))
df_rate.sort_values(by=['id', 'product_term', 'max_amount'], inplace=True)
df2 = df_rate.drop_duplicates(subset=['id', 'product_term', 'min_rate'], keep='last') #For each unique id, product_term and min_rate, keep the largest max_amount
df4 = df2[df2['max_amount'] >= df2['requested_amount']]
df4 = df4.sort_values(by=['id', 'product_term', 'max_amount'])
df4 = df4.drop_duplicates(subset=['id', 'product_term'], keep='first')
df4_new = flat_term_rate_dat(df4, flat_cols=['min_rate', 'tier'], consolidate_cols=[])
df4_new.shape
# (691571, 12)
df4_new.to_csv(pjoin(dat_dir, 'entire_rate_1119.csv'))

#3. Clean main data
df = eread(pjoin(dat_dir, 'entire_raw_1119.feather'))
df4_new = eread(pjoin(dat_dir, 'entire_rate_1119.csv'))
df = pd.merge(df, df4_new, on='id')
df.shape
#(682123, 82)

#Preprocess data
PL_terms = [2, 3, 4, 5, 6, 7]
df = add_signed_ind(df, terms=PL_terms)
df.shape
#(665547, 88)
def process_1(df):
    #Member indicator, grad to 0/1 variable
    df['member_indicator'] = df['member_indicator'].map({False: 0, True: 1})
    df['grad'] = np.where(df['g_program'].notnull(), 1, 0)
    #Define date related variables
    min_start_date = df['date_start'].min()
    df['date_start_year'] =  df['date_start'].dt.year
    df['date_start_month'] = df['date_start'].dt.month
    df['date_start_day'] = df['date_start'].dt.day
    df['date_start_weekday'] = df['date_start'].dt.weekday
    df['ref_month'] = df['date_start_year'] * 12 + df['date_start_month'] - (min_start_date.year * 12 + min_start_date.month)
    df['ref_day'] = df['date_start'].dt.dayofyear + (df['date_start_year'] - min_start_date.year) * 365
    #Clean affiliate variable
    df['attr_affiliate_referrer'] = clean_str(df['attr_affiliate_referrer'])
    df['attr_affiliate_referrer_new'] = np.where(df['attr_affiliate_referrer'].isin(['www.sofi.com','www.google.com','credit_karma','lending_tree']),
                                                     df['attr_affiliate_referrer'], 'other')
    #Create indicator variables for selected each term or not
    for term in PL_terms:
        df[f'initial_term_{term}'] = np.where((df['initial_term'] == term), 1, 0)
    #Map interest_rate to individual term columns
    df = flat_rate(df, PL_terms)
    #Consolidate tier columns
    df['tier_pre'] = df[[f'tier_{term}' for term in PL_terms]].min(axis=1)
    return df

df = process_1(df)
df, impute_rate_dict = impute_rate(df, rate_cols=[f'min_rate_{term}' for term in PL_terms])
{'min_rate_2': 0.16677,
 'min_rate_3': 0.16677,
 'min_rate_4': 0.17436,
 'min_rate_5': 0.18133,
 'min_rate_6': 0.15313,
 'min_rate_7': 0.16115}
df.to_csv(pjoin(dat_dir, 'data_p1.csv'), index=None)

#OOT Data
df_oot = df[df['date_start'] >= '2019-09-15']
df_oot.shape
#(31710, 115)
df_oot.to_csv(pjoin(dat_dir, 'PL_rate_oot_1.csv'), index=None)
df_oot_term = df_oot[df_oot['initial_term'].notnull()]
df_oot_term.to_csv(pjoin(dat_dir, 'PL_term_oot_1.csv'), index=None)

#Raw train and oos data without filter
df_train, df_oos = divide_train_oos(df[df['date_start'] < '2019-09-15'], random_state=612)
df_train.to_csv(pjoin(dat_dir, 'PL_rate_train_0.csv'), index=None)
df_oos.to_csv(pjoin(dat_dir, 'PL_rate_oos_0.csv'), index=None)
print(df_train.shape, df_oos.shape)
#443687, 190152

#Exp 1 - 1b: Remove f
def handle_req_amount(df):
    df = df.loc[df['requested_amount'] != 0]
    return df
df_train = handle_req_amount(df_train)
df_oos = handle_req_amount(df_oos)
df_train.to_csv(pjoin(dat_dir, 'PL_rate_train_1.csv'), index=None)
df_oos.to_csv(pjoin(dat_dir, 'PL_rate_oos_1.csv'), index=None)
print(df_train.shape, df_oos.shape)
#(377971, 115) (161889, 115)
df_train_term = df_train[df_train['initial_term'].notnull()]
df_oos_term = df_oos[df_oos['initial_term'].notnull()]
df_train_term.to_csv(pjoin(dat_dir, 'PL_term_train_1.csv'), index=None)
df_oos_term.to_csv(pjoin(dat_dir, 'PL_term_oos_1.csv'), index=None)

#Exp 2: remove data before 2018-10-18
df_train = pd.read_csv(pjoin(dat_dir, 'PL_rate_train_0.csv'))
df_oos = pd.read_csv(pjoin(dat_dir, 'PL_rate_oos_0.csv'))
print(df_train.shape, df_oos.shape)
def handle_req_amount_2(df):
    df = df.loc[df['date_start'] > '2018-10-18']
    return df
df_train = handle_req_amount_2(df_train)
df_oos = handle_req_amount_2(df_oos)
print(df_train.shape, df_oos.shape)
#(224202, 115) (96158, 115)
df_train.to_csv(pjoin(dat_dir, 'PL_rate_train_2.csv'), index=None)
df_oos.to_csv(pjoin(dat_dir, 'PL_rate_oos_2.csv'), index=None)
df_train_term = df_train[df_train['initial_term'].notnull()]
df_oos_term = df_oos[df_oos['initial_term'].notnull()]
df_train_term.to_csv(pjoin(dat_dir, 'PL_term_train_2.csv'), index=None)
df_oos_term.to_csv(pjoin(dat_dir, 'PL_term_oos_2.csv'), index=None)

#Prepare header
#Exp 1
header_dir = '/home/ec2-user/SageMaker/round_2/header'
df.iloc[:2].to_csv(pjoin(header_dir, 'header_row.csv'), index=None)
#validate header
header_1 = Header(pjoin(header_dir, 'round2_1_rate_header.csv'))
header_2 = Header('/home/ec2-user/SageMaker/score_new/redevelopment/exp_3_rate_header.csv')
print(header_1.num_cols == header_2.num_cols, header_1.cat_cols == header_2.cat_cols, header_1.label_cols == header_2.label_cols)
set(header_1.num_cols) - set(header_2.num_cols)
set(header_2.num_cols) - set(header_1.num_cols)

header_1 = Header(pjoin(header_dir, 'round2_1_term_header.csv'))
header_2 = Header('/home/ec2-user/SageMaker/score_new/redevelopment/exp_3_term_header.csv')
print(header_1.num_cols == header_2.num_cols, header_1.cat_cols == header_2.cat_cols, header_1.label_cols == header_2.label_cols)
set(header_1.num_cols) - set(header_2.num_cols)
set(header_2.num_cols) - set(header_1.num_cols)

#Exp 1b
change_cols(pjoin(header_dir, 'round2_1_rate_header.csv'), pjoin(header_dir, 'round2_1b_rate_header.csv'), 
            [f'min_rate_{term}' for term in PL_terms], [f'min_rate_{term}_orig' for term in PL_terms])
change_cols(pjoin(header_dir, 'round2_1_term_header.csv'), pjoin(header_dir, 'round2_1b_term_header.csv'), 
            [f'min_rate_{term}' for term in PL_terms], [f'min_rate_{term}_orig' for term in PL_terms])

#Validate
header_1 = Header(pjoin(header_dir, 'round2_1_rate_header.csv'))
header_2 = Header(pjoin(header_dir, 'round2_1b_rate_header.csv'))
print(header_1.cat_cols == header_2.cat_cols, header_1.label_cols == header_2.label_cols)
print(set(header_1.num_cols) - set(header_2.num_cols))
print(set(header_2.num_cols) - set(header_1.num_cols))
    
#Exp 7a
change_cols(pjoin(header_dir, 'round2_1_rate_header.csv'), pjoin(header_dir, 'round2_7a_rate_header.csv'), 
                  ['date_start_year', 'date_start_month', 'date_start_day'], ['ref_month', 'ref_day'])
change_cols(pjoin(header_dir, 'round2_1_term_header.csv'), pjoin(header_dir, 'round2_7a_term_header.csv'), 
                  ['date_start_year', 'date_start_month', 'date_start_day'], ['ref_month', 'ref_day'])

#Exp 7b: add new variables
def cut_cat_var(df, cat_var, cat_var_value_list):
    """For a given cat_var in a DataFrame df, keep values in cat_var_value_list and group other values in 'Rest'
    """
    df[cat_var] = np.where((df[cat_var].isin(cat_var_value_list)) | (df[cat_var].isnull()), df[cat_var], 'Rest')
    return df

def process_3(in_data_file, out_data_file):
    df = pd.read_csv(in_data_file)
    #Cut and group categorical variables
    g_program_list = ['Business - MBA', 'Other', 'Law - JD', 'Engineering/Computer Science - MS or higher',
                                  'Business - MS or MA or higher', 'Social Sciences - MS or MA or higher']
    channel_list = ['www.sofi.com', 'Affiliate', 'SEM', 'Organic Search', 'MKT-DM', 'P2P']
    affiliate_list = ['www.sofi.com', 'www.google.com', '2019_pl_sofi_mail', 'credit_karma', 'lending_tree', 'nerdwallet']
    df = cut_cat_var(df, 'g_program', g_program_list)
    df = cut_cat_var(df, 'consolidated_channel', channel_list)
    df = cut_cat_var(df, 'attr_affiliate_referrer', affiliate_list)
    
    #Clean date variables
    df['date_start'] = pd.to_datetime(df['date_start'])
    df['g_grad_year'] = df['date_start'].dt.year - df['g_grad_year']
    df['ug_grad_year'] = df['date_start'].dt.year - df['ug_grad_year']
    #registration_date: date_start - var
    df['registration_date'] = pd.to_datetime(df['registration_date'])
    df['registration_date'] = (df['date_start'] - df['registration_date']).dt.days
    
    #employer_name: clean and categorize into at least 'self employed', 'RETIRED', missing (<-'employerName')
    df['employer_name'] = df['employer_name'].str.lower()
    df['employer_name_new'] = 'other'
    df['employer_name_new'] = np.where((df['employer_name'].str.contains('self', na=False)) & (df['employer_name'].str.contains('employ', na=False)), 'self_employed', df['employer_name_new'])
    df['employer_name_new'] = np.where(df['employer_name'].str.contains('retire', na=False), 'retire', df['employer_name_new'])
    df['employer_name_new'] = np.where((df['employer_name'].str.contains('employername', na=False)) | (df['employer_name'].isnull()) | (df['employer_name'] == ' '), 'missing', df['employer_name_new'])
    df.drop('employer_name', axis=1, inplace=True)
    df.rename(columns={'employer_name_new': 'employer_name'}, inplace=True)
    
    #add variables
    rate_cols = [f'min_rate_{term}' for term in range(2,8)]
    df['min_all_rates'] = df[rate_cols].min(axis=1) #Minimum of all rates
    df['no_terms'] = df[rate_cols].notnull().sum(axis=1) #No of terms available
    df.to_csv(out_data_file, index=None)

process_3(pjoin(dat_dir, 'PL_rate_train_1.csv'), pjoin(dat_dir, 'PL_rate_train_7b.csv'))
process_3(pjoin(dat_dir, 'PL_rate_oos_1.csv'), pjoin(dat_dir, 'PL_rate_oos_7b.csv'))
process_3(pjoin(dat_dir, 'PL_rate_oot_1.csv'), pjoin(dat_dir, 'PL_rate_oot_7b.csv'))

for model in ['term']:
    for time in ['train', 'oos', 'oot']:
        process_3(pjoin(dat_dir, f'PL_{model}_{time}_1.csv'), pjoin(dat_dir, f'PL_{model}_{time}_7b.csv'))

#Change label variable


change_cols(pjoin(header_dir, 'round2_7b_rate_header.csv'), pjoin(header_dir, 'round2_7b_term_header.csv'), 
            'signed_ind', 'initial_term', add_type='L')

#Exp 7c
change_cols(pjoin(header_dir, 'round2_7b_rate_header.csv'), pjoin(header_dir, 'round2_7c_rate_header.csv'), 
            remove_cols='employer_name')
change_cols(pjoin(header_dir, 'round2_7b_term_header.csv'), pjoin(header_dir, 'round2_7c_term_header.csv'), 
            remove_cols='employer_name')

#Exp 7d
def add_columns(orig_data_file, new_data_file, add_date_file, add_columns, idvar):
    """Add additional columns to a given data
    """
    add_columns = check_trans_to_list(add_columns)
    df = eread(orig_data_file)
    df_add = eread(add_date_file)
    df = pd.merge(df, df_add[[idvar] + add_columns], on=idvar, how='left')
    esave(df, new_data_file)

for model in ['rate', 'term']:
    for time in ['train', 'oos', 'oot']:
        add_columns(pjoin(dat_dir, f'PL_{model}_{time}_7b.csv'), pjoin(dat_dir, f'PL_{model}_{time}_7c.csv'),
                    pjoin(dat_dir, 'entire_raw_1119_add.feather'), ['adjusted_gross_income', 'max_loan_amount'], 'id')
    
for model in ['rate', 'term']:
    change_cols(pjoin(header_dir, f'round2_7b_{model}_header.csv'), pjoin(header_dir, f'round2_7d_{model}_header.csv'), 
            add_cols='adjusted_gross_income')

#Exp 7e add monthly payment features
df_rate = eread(pjoin(dat_dir, 'entire_rate_raw_1119.feather'))
df_rate.sort_values(by=['id', 'product_term', 'max_amount'], inplace=True)
df2 = df_rate.drop_duplicates(subset=['id', 'product_term', 'min_rate'], keep='last') #For each unique id, product_term and min_rate, keep the largest max_amount
df4 = df2[df2['max_amount'] >= df2['requested_amount']]
df4 = df4.sort_values(by=['id', 'product_term', 'max_amount'])
df4 = df4.drop_duplicates(subset=['id', 'product_term'], keep='first')
df4 = def_monthly_payment(df4)
df4['total_interest'] = df4['total_payment'] / df4['requested_amount'] - 1
df4_new = flat_term_rate_dat(df4, flat_cols=['min_rate', 'tier', 'monthly_payment', 'total_payment', 'total_interest'], consolidate_cols=[])
df4_new.shape
# (691571, 30)
df4_new.to_csv(pjoin(dat_dir, 'entire_rate_monthly_payment_1119.csv'))
    
def def_monthly_payment(df, request_amt_col='requested_amount', term_col='product_term', rate_col='min_rate'):
    """Calculate monthly payments
    """
    df['monthly_payment'], df['total_payment'] = calc_payments(df[request_amt_col], df[term_col], df[rate_col])
    return df

def process_4(in_data_file, out_data_file, df_rate):
    df = pd.read_csv(in_data_file)
    PL_terms = [2, 3, 4, 5, 6, 7]
    monthly_payment_cols = [f'monthly_payment_{term}' for term in PL_terms]
    total_interest_cols = [f'total_interest_{term}' for term in PL_terms]
    df = pd.merge(df, df_rate[monthly_payment_cols+ total_interest_cols] , how='left', left_on='id', right_index=True)
    df['min_monthly_payment'] = df[monthly_payment_cols].min(axis=1)
    df['min_total_interest'] = df[total_interest_cols].min(axis=1)
    df['free_cash_flow_post_max'] = df['free_cash_flow_pre'] - df['min_monthly_payment']
    for term in PL_terms:
        df[f'free_cash_flow_post_{term}'] = df['free_cash_flow_pre'] - df[f'monthly_payment_{term}']
    df.to_csv(out_data_file, index=None)
    
for model in ['rate', 'term']:
    for time in ['train', 'oos', 'oot']:
        process_4(pjoin(dat_dir, f'PL_{model}_{time}_7b.csv'), pjoin(dat_dir, f'PL_{model}_{time}_7e.csv'), df4_new)
        
#Exp 7f
monthly_payment_cols = [f'monthly_payment_{term}' for term in range(2,8)]
for model in ['rate', 'term']:
    change_cols(pjoin(header_dir, f'round2_7e_{model}_header.csv'), pjoin(header_dir, f'round2_7f_{model}_header.csv'), 
                remove_cols=['min_total_interest'] + monthly_payment_cols)
    
#Exp 7g: add rate_diff to term selection model
def process_5(in_data_file, out_data_file):
    df = pd.read_csv(in_data_file)
    for term in range(2, 7):
        df[f'min_rate_diff_{term+1}_{term}'] = df[f'min_rate_{term+1}'] - df[f'min_rate_{term}']
    df.to_csv(out_data_file, index=None)

for model in ['rate', 'term']:
    for time in ['train', 'oos', 'oot']:
        process_5(pjoin(dat_dir, f'PL_{model}_{time}_7e.csv'), pjoin(dat_dir, f'PL_{model}_{time}_7g.csv'))
        
change_cols(pjoin(header_dir, 'round2_7f_term_header.csv'), pjoin(header_dir, 'round2_7g_term_header.csv'), 
            add_cols=[f'min_rate_diff_{term+1}_{term}' for term in range(2,7)])

change_cols(pjoin(header_dir, 'round2_7c_term_header.csv'), pjoin(header_dir, 'round2_7h_term_header.csv'), 
            add_cols=[f'min_rate_diff_{term+1}_{term}' for term in range(2,7)])


#Exp 8: only use doc upload for term selection model
def process_6(in_data_file, out_data_file):
    df = pd.read_csv(in_data_file)
    print("before:", df.shape)
    df = df[df['date_doc_upload'].notnull()]
    print(df.shape)
    df.to_csv(out_data_file, index=None)

for model in ['term']:
    for time in ['train', 'oos', 'oot']:
        process_6(pjoin(dat_dir, f'PL_{model}_{time}_7g.csv'), pjoin(dat_dir, f'PL_{model}_{time}_8.csv'))

#Exp 11: Test informa data
df_informa = pd.read_csv('../competitor_data/informa_data_clean.csv')

def process_7(in_data_file, out_data_file):
    df = pd.read_csv(in_data_file)
    df['month'] = df['date_start'].values.astype('datetime64[M]')
    cond_list_1 = [(df['month'] <= '2019-06-01') & (df['credit_score'] >= 680) & (df['credit_score'] <= 699), 
               (df['month'] <= '2019-06-01') & (df['credit_score'] >= 700) & (df['credit_score'] <= 749),
               (df['month'] <= '2019-06-01') & (df['credit_score'] >= 750),
               (df['month'] >='2019-07-01') & (df['credit_score'] >= 680) & (df['credit_score'] <= 699),
               (df['month'] >='2019-07-01') & (df['credit_score'] >= 700) & (df['credit_score'] <= 719),
               (df['month'] >='2019-07-01') & (df['credit_score'] >= 720) & (df['credit_score'] <= 739),
               (df['month'] >='2019-07-01') & (df['credit_score'] >= 740) & (df['credit_score'] <= 759),
               (df['month'] >='2019-07-01') & (df['credit_score'] >= 760) & (df['credit_score'] <= 779),
               (df['month'] >='2019-07-01') & (df['credit_score'] >= 780) & (df['credit_score'] <= 799),
               (df['month'] >='2019-07-01') & (df['credit_score'] >= 800)]
    value_list_1 = ['680-699', '700-749', '750+', '680-699', '700-719', '720-739', '740-759', '760-779', '780-799', '800+']
    df['credit_score_bin'] = np.select(cond_list_1, value_list_1)
    df['month'] = df['month'].astype(str)
    df = pd.merge(df, df_informa, on=['month', 'credit_score_bin'], how='left')  
    df['loan_abs_diff'] = (df['requested_amount'] - df['informa_loan_amount']).abs()
    df.sort_values(by=['id', 'loan_abs_diff'], inplace=True)
    df = df.drop_duplicates(subset=['id'], keep='first')
    df = df.reset_index(drop=True)
    for term in range(2, 8):
        df[f'rate_diff_informa_{term}'] = df[f'min_rate_{term}'] - df[f'informa_rate_{term}']
    df.to_csv(out_data_file, index=None)
    
for model in ['rate']:
    for time in ['train', 'oos', 'oot']:
        process_7(pjoin(dat_dir, f'PL_{model}_{time}_7g.csv'), pjoin(dat_dir, f'PL_{model}_{time}_11.csv'))

rate_diff_cols = [f'rate_diff_informa_{term}' for term in range(2,8)]        
for model in ['rate']:
    change_cols(pjoin(header_dir, f'round2_7c_{model}_header.csv'), pjoin(header_dir, f'round2_11_{model}_header.csv'), 
                add_cols=rate_diff_cols)

#Exp 12: Test lending tree data
df_lt = eread('../competitor_data/lending_tree/orig_dat_with_rate_diff.feather')
lt_rate_cols = ['lt_rate_2', 'lt_rate_3', 'lt_rate_4', 'lt_rate_5', 'lt_rate_6', 'lt_rate_7']

def process_7(in_data_file, out_data_file):
    df = pd.read_csv(in_data_file)
    df = pd.merge(df, df_lt[['id'] + lt_rate_cols])
    for term in range(2, 8):
        df[f'rate_diff_lt_{term}'] = df[f'min_rate_{term}'] - df[f'lt_rate_{term}'] / 100
    df.to_csv(out_data_file, index=None)
    return df
    
for model in ['rate']:
    for time in ['train', 'oos', 'oot']:
        process_7(pjoin(dat_dir, f'PL_{model}_{time}_7g.csv'), pjoin(dat_dir, f'PL_{model}_{time}_12.csv'))

rate_diff_cols = [f'rate_diff_lt_{term}' for term in range(2,8)]        
for model in ['rate']:
    change_cols(pjoin(header_dir, f'round2_7c_{model}_header.csv'), pjoin(header_dir, f'round2_12_{model}_header.csv'), 
                add_cols=rate_diff_cols)

#Try new imputation -> Use 2 3 6 7
for term in range(2, 8):
        df[f'rate_diff_lt_{term}_i1'] = df[f'min_rate_{term}_i1'] - df[f'lt_rate_{term}'] / 100
rate_diff_cols_i1 = [f'rate_diff_lt_{term}_i1' for term in range(2, 8)]
rate_diff_cols = [f'rate_diff_lt_{term}' for term in range(2, 8)]
result = []
for plot_var_1, plot_var_2 in zip(rate_diff_cols, rate_diff_cols_i1):
    result.append([gini(df['signed_ind'], df[plot_var_1]), gini(df['signed_ind'], df[plot_var_2])])
pd.DataFrame(result)
              0         1
0  0.067503 -0.072685
1 -0.123700 -0.139538
2 -0.127349 -0.118874
3 -0.164216 -0.155619
4 -0.134064 -0.143458
5 -0.102224 -0.109246

df2 = df[df['attr_affiliate_referrer'] == 'lending_tree']
for plot_var_1, plot_var_2 in zip(rate_diff_cols, rate_diff_cols_i1):
    result.append([gini(df2['signed_ind'], df2[plot_var_1]), gini(df2['signed_ind'], df2[plot_var_2])])
pd.DataFrame(result)
        0         1
0  0.001590  0.000229
1 -0.085673 -0.085596
2 -0.044542 -0.044083

#Check correlation of rate and rate diff variable
df = eread(pjoin(dat_dir, f'PL_rate_train_12.csv'))
for term in range(2, 8):
    print (df[f'rate_diff_lt_{term}'].corr(df[f'min_rate_{term}']), gini(df[f'min_rate_{term}'], df[f'rate_diff_lt_{term}']))
0.7527512885192935 0.839339521175099
0.6588772570496856 0.6801616199883315
0.6591091868061325 0.6531170982082788
0.5944424609150434 0.6114052847632737
0.6796541993129723 0.7970047975443272
0.5099713210099637 0.8621174099064972

for term in range(2, 8):
    print(gini(df['signed_ind'], df[f'rate_diff_lt_{term}']), gini(df['signed_ind'], df[f'min_rate_{term}']))
0.06750309657796105 0.09083606772337394
-0.12370044888217946 -0.10306595106407823
-0.12734924022586092 -0.13346962383999428
-0.16421640683529104 -0.1482589255049721
-0.13406364890637912 -0.12766015369383021
-0.10222370508505596 -0.12643636940788805

gen_plot_2(df, pjoin(dat_dir, 'rate_diff_train.pdf'), rate_diff_cols)

#??How to impute for missing

#Test missing imputation
df = eread(pjoin(dat_dir, f'PL_rate_train_12.csv'))
orig_rate_cols = [f'min_rate_{term}_orig' for term in range(2, 8)]
result = []
for i, (rate_col, rate_col_orig) in enumerate(zip(rate_cols, orig_rate_cols), 2):
    result.append([gini(df['signed_ind'], df[rate_col]), calculate_gini(df, 'signed_ind', rate_col_orig, dropna=True)])
pd.DataFrame(result)
"""
          0         1
2  0.090836 -0.079687
3 -0.103066 -0.117041
4 -0.133470 -0.123548
5 -0.148259 -0.138763
6 -0.127660 -0.061602
7 -0.126436 -0.063049
"""
#Try impute by row
#a. Calculate median of rate difference
for term in range(2, 7):
    df[f'min_rate_diff_{term+1}_{term}_orig'] = df[f'min_rate_{term+1}_orig'] - df[f'min_rate_{term}_orig']
rate_lag_cols = [f'min_rate_diff_{term+1}_{term}_orig' for term in range(2, 7)]
medians = df[rate_lag_cols].median()
"""
min_rate_diff_3_2_orig    0.00250
min_rate_diff_4_3_orig    0.00688
min_rate_diff_5_4_orig    0.00687
min_rate_diff_6_5_orig    0.00813
min_rate_diff_7_6_orig    0.00561
"""
df['min_rate_2_i1'] = np.where(df['min_rate_2_orig'].notnull(), df['min_rate_2_orig'], 
                               np.where(df['min_rate_3_orig'].notnull(), df['min_rate_3_orig'] - 0.0025, 
                               np.where(df['min_rate_4_orig'].notnull(), df['min_rate_4_orig'] - 0.0025 - 0.00688, 
                               np.where(df['min_rate_5_orig'].notnull(), df['min_rate_5_orig'] - 0.0025 - 0.00688 - 0.00687,
                               np.where(df['min_rate_6_orig'].notnull(), df['min_rate_6_orig'] - 0.0025 - 0.00688 - 0.00687 - 0.00813,
                               df['min_rate_7_orig'] - 0.0025 - 0.00688 - 0.00687 - 0.00813- 0.00561)))))
#gini: -0.11736122959950002

df['min_rate_3_i1'] = np.where(df['min_rate_3_orig'].notnull(), df['min_rate_3_orig'], 
                               np.where(df['min_rate_4_orig'].notnull(), df['min_rate_4_orig'] - 0.00688,
                               np.where(df['min_rate_2_orig'].notnull(), df['min_rate_2_orig'] + 0.0025, 
                               np.where(df['min_rate_5_orig'].notnull(), df['min_rate_5_orig'] - 0.00688 - 0.00687,
                               np.where(df['min_rate_6_orig'].notnull(), df['min_rate_6_orig'] - 0.00688 - 0.00687 - 0.00813,
                               df['min_rate_7_orig'] - 0.00688 - 0.00687 - 0.00813- 0.00561)))))
#gini:  -0.11870779318066227

df['min_rate_4_i1'] = np.where(df['min_rate_4_orig'].notnull(), df['min_rate_4_orig'], 
                               np.where(df['min_rate_5_orig'].notnull(), df['min_rate_5_orig'] - 0.00687, 
                               np.where(df['min_rate_3_orig'].notnull(), df['min_rate_3_orig'] + 0.00688, 
                               np.where(df['min_rate_2_orig'].notnull(), df['min_rate_2_orig'] + 0.0025 + 0.00688,
                               np.where(df['min_rate_6_orig'].notnull(), df['min_rate_6_orig'] - 0.00687 - 0.00813,
                               df['min_rate_7_orig'] - 0.00687 - 0.00813- 0.00561)))))
#gini: -0.1270073487468013 -> not good

df['min_rate_5_i1'] = np.where(df['min_rate_5_orig'].notnull(), df['min_rate_5_orig'], 
                               np.where(df['min_rate_4_orig'].notnull(), df['min_rate_4_orig'] + 0.00687, 
                               np.where(df['min_rate_6_orig'].notnull(), df['min_rate_6_orig'] - 0.00813, 
                               np.where(df['min_rate_3_orig'].notnull(), df['min_rate_3_orig'] + 0.00687 + 0.00688,
                               np.where(df['min_rate_7_orig'].notnull(), df['min_rate_7_orig'] - 0.00813- 0.00561,
                               df['min_rate_2_orig'] + 0.0025 + 0.00688 + + 0.00687)))))
#-0.1378008956900173 -> not good

df['min_rate_6_i1'] = np.where(df['min_rate_6_orig'].notnull(), df['min_rate_6_orig'], 
                               np.where(df['min_rate_7_orig'].notnull(), df['min_rate_7_orig'] - 0.00561, 
                               np.where(df['min_rate_5_orig'].notnull(), df['min_rate_5_orig'] + 0.00813, 
                               np.where(df['min_rate_4_orig'].notnull(), df['min_rate_4_orig'] + 0.00687 + 0.00813,
                               np.where(df['min_rate_3_orig'].notnull(), df['min_rate_3_orig'] + 0.00687 + 0.00813 + 0.00688,
                               df['min_rate_2_orig'] + 0.00687 + 0.00813 + 0.0068+ 0.0025)))))
#-0.13874489464648898

df['min_rate_7_i1'] = np.where(df['min_rate_7_orig'].notnull(), df['min_rate_7_orig'], 
                               np.where(df['min_rate_6_orig'].notnull(), df['min_rate_6_orig'] + 0.00561, 
                               np.where(df['min_rate_5_orig'].notnull(), df['min_rate_5_orig'] + 0.00561 + 0.00813, 
                               np.where(df['min_rate_4_orig'].notnull(), df['min_rate_4_orig'] + 0.00561 + 0.00813 + 0.00687 ,
                               np.where(df['min_rate_3_orig'].notnull(), df['min_rate_3_orig'] + 0.00561 + 0.00813 + 0.0068 + 0.00688,
                               df['min_rate_2_orig'] + 0.00561 + 0.00813 + 0.0068 + 0.006 + 0.0025)))))
#-0.13849301152481375

#13 second version of lending tree rate differece
def process_8(in_data_file, out_data_file):
    df = pd.read_csv(in_data_file)
    df = pd.merge(df, df_lt[['id'] + lt_rate_cols])
    df['min_rate_2_i1'] = np.where(df['min_rate_2_orig'].notnull(), df['min_rate_2_orig'], 
                               np.where(df['min_rate_3_orig'].notnull(), df['min_rate_3_orig'] - 0.0025, 
                               np.where(df['min_rate_4_orig'].notnull(), df['min_rate_4_orig'] - 0.0025 - 0.00688, 
                               np.where(df['min_rate_5_orig'].notnull(), df['min_rate_5_orig'] - 0.0025 - 0.00688 - 0.00687,
                               np.where(df['min_rate_6_orig'].notnull(), df['min_rate_6_orig'] - 0.0025 - 0.00688 - 0.00687 - 0.00813,
                               df['min_rate_7_orig'] - 0.0025 - 0.00688 - 0.00687 - 0.00813- 0.00561)))))
    df['min_rate_3_i1'] = np.where(df['min_rate_3_orig'].notnull(), df['min_rate_3_orig'], 
                                   np.where(df['min_rate_4_orig'].notnull(), df['min_rate_4_orig'] - 0.00688,
                                   np.where(df['min_rate_2_orig'].notnull(), df['min_rate_2_orig'] + 0.0025, 
                                   np.where(df['min_rate_5_orig'].notnull(), df['min_rate_5_orig'] - 0.00688 - 0.00687,
                                   np.where(df['min_rate_6_orig'].notnull(), df['min_rate_6_orig'] - 0.00688 - 0.00687 - 0.00813,
                                   df['min_rate_7_orig'] - 0.00688 - 0.00687 - 0.00813- 0.00561)))))
    df['min_rate_4_i1'] = np.where(df['min_rate_4_orig'].notnull(), df['min_rate_4_orig'], 
                                   np.where(df['min_rate_5_orig'].notnull(), df['min_rate_5_orig'] - 0.00687, 
                                   np.where(df['min_rate_3_orig'].notnull(), df['min_rate_3_orig'] + 0.00688, 
                                   np.where(df['min_rate_2_orig'].notnull(), df['min_rate_2_orig'] + 0.0025 + 0.00688,
                                   np.where(df['min_rate_6_orig'].notnull(), df['min_rate_6_orig'] - 0.00687 - 0.00813,
                                   df['min_rate_7_orig'] - 0.00687 - 0.00813- 0.00561)))))
    df['min_rate_5_i1'] = np.where(df['min_rate_5_orig'].notnull(), df['min_rate_5_orig'], 
                                   np.where(df['min_rate_4_orig'].notnull(), df['min_rate_4_orig'] + 0.00687, 
                                   np.where(df['min_rate_6_orig'].notnull(), df['min_rate_6_orig'] - 0.00813, 
                                   np.where(df['min_rate_3_orig'].notnull(), df['min_rate_3_orig'] + 0.00687 + 0.00688,
                                   np.where(df['min_rate_7_orig'].notnull(), df['min_rate_7_orig'] - 0.00813- 0.00561,
                                   df['min_rate_2_orig'] + 0.0025 + 0.00688 + + 0.00687)))))
    df['min_rate_6_i1'] = np.where(df['min_rate_6_orig'].notnull(), df['min_rate_6_orig'], 
                                   np.where(df['min_rate_7_orig'].notnull(), df['min_rate_7_orig'] - 0.00561, 
                                   np.where(df['min_rate_5_orig'].notnull(), df['min_rate_5_orig'] + 0.00813, 
                                   np.where(df['min_rate_4_orig'].notnull(), df['min_rate_4_orig'] + 0.00687 + 0.00813,
                                   np.where(df['min_rate_3_orig'].notnull(), df['min_rate_3_orig'] + 0.00687 + 0.00813 + 0.00688,
                                   df['min_rate_2_orig'] + 0.00687 + 0.00813 + 0.0068+ 0.0025)))))
    df['min_rate_7_i1'] = np.where(df['min_rate_7_orig'].notnull(), df['min_rate_7_orig'], 
                                   np.where(df['min_rate_6_orig'].notnull(), df['min_rate_6_orig'] + 0.00561, 
                                   np.where(df['min_rate_5_orig'].notnull(), df['min_rate_5_orig'] + 0.00561 + 0.00813, 
                                   np.where(df['min_rate_4_orig'].notnull(), df['min_rate_4_orig'] + 0.00561 + 0.00813 + 0.00687 ,
                                   np.where(df['min_rate_3_orig'].notnull(), df['min_rate_3_orig'] + 0.00561 + 0.00813 + 0.0068 + 0.00688,
                                   df['min_rate_2_orig'] + 0.00561 + 0.00813 + 0.0068 + 0.006 + 0.0025)))))
    for term in range(2, 8):
        df[f'rate_diff_lt_{term}_i1'] = df[f'min_rate_{term}_i1'] - df[f'lt_rate_{term}'] / 100
    df.to_csv(out_data_file, index=None)
    return df

for model in ['rate']:
    for time in ['train', 'oos', 'oot']:
        process_8(pjoin(dat_dir, f'PL_{model}_{time}_12.csv'), pjoin(dat_dir, f'PL_{model}_{time}_13.csv'))
  
for model in ['rate']:
    change_cols(pjoin(header_dir, f'round2_7c_{model}_header.csv'), pjoin(header_dir, f'round2_13_{model}_header.csv'), 
                add_cols=['rate_diff_lt_2_i1', 'rate_diff_lt_3_i1',
                          'rate_diff_lt_4', 'rate_diff_lt_5',
                          'rate_diff_lt_6_i1', 'rate_diff_lt_7_i1'])
    
#14. 7c term not impute + term diff
def process_14(in_data_file, out_data_file):
    df = pd.read_csv(in_data_file)
    for term in range(2, 7):
        df[f'min_rate_diff_{term+1}_{term}_orig'] = df[f'min_rate_{term+1}_orig'] - df[f'min_rate_{term}_orig']
    df.to_csv(out_data_file, index=None)

for model in ['rate', 'term']:
    for time in ['train', 'oos', 'oot']:
        process_14(pjoin(dat_dir, f'PL_{model}_{time}_7g.csv'), pjoin(dat_dir, f'PL_{model}_{time}_14.csv'))
        
change_cols(pjoin(header_dir, 'round2_7c_term_header.csv'), pjoin(header_dir, 'round2_14_term_header.csv'), 
            remove_cols=[f'min_rate_{term}' for term in range(2, 8)],
            add_cols=[f'min_rate_diff_{term+1}_{term}_orig' for term in range(2,7)] + [f'min_rate_{term}_orig' for term in range(2, 8)])

#15. impute term diff smartly
df = eread(pjoin(dat_dir, 'PL_rate_train_14.csv'))
rate_diff_cols = [f'min_rate_diff_{term+1}_{term}_orig' for term in range(2,7)]
df_rate_diff_mins = np.round(df[rate_diff_cols].min(), 5).to_list()
#[-0.04419, -0.0506, -0.04875, -0.0456, -0.02064]
df_rate_diff_maxs = np.round(df[rate_diff_cols].max(), 5).to_list()
#[0.009, 0.0271, 0.0463, 0.02423, 0.0154]

def process_15(in_data_file, out_data_file):
    df = pd.read_csv(in_data_file)
    for idx, term in enumerate(range(2, 7)):
        df[f'min_rate_diff_impute_{term+1}_{term}'] = np.where(df[f'min_rate_diff_{term+1}_{term}_orig'].notnull(), 
                                                               df[f'min_rate_diff_{term+1}_{term}_orig'],
                                                               np.where(df[f'min_rate_{term}'].isnull(), df_rate_diff_mins[idx],
                                                                        df_rate_diff_maxs[idx]))
    df.to_csv(out_data_file, index=None)

for model in ['rate', 'term']:
    for time in ['train', 'oos', 'oot']:
        process_15(pjoin(dat_dir, f'PL_{model}_{time}_14.csv'), pjoin(dat_dir, f'PL_{model}_{time}_15.csv'))

change_cols(pjoin(header_dir, 'round2_14_term_header.csv'), pjoin(header_dir, 'round2_15_term_header.csv'), 
            remove_cols=[f'min_rate_diff_{term+1}_{term}_orig' for term in range(2,7)],
            add_cols=[f'min_rate_diff_impute_{term+1}_{term}'for term in range(2,7)])
