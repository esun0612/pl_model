#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  5 09:32:18 2019

@author: esun

Calculate new tier
"""

from util_data import query_products
dat_dir = '/home/ec2-user/SageMaker/adhoc/check_optimization'
sql_ml = """
select mm.*, mrv.response_value as ml_sc
from ml_model mm
left join
(select *  from ml_response_value
where response_key in ('score')) as mrv
on mrv.ml_model_id=mm.ml_model_id
"""

ml = query_products(sql_ml)
esave(ml, pjoin(dat_dir, 'ml.feather'))
print(ml.shape, ml['application_id'].nunique())
Out[829]: (1762005, 12) 1070953

start_date = '2019-10-04'
end_date = '2019-11-04'
product = f"'PL'"
date_range = f"af.date_start>='{start_date}' and af.date_start<='{end_date}'"

PL_qry = f"""
SELECT
af.id,
af.applicant_id
FROM dwmart.applications_file af
JOIN dwmart.application_credit_attributes_file acaf ON af.dw_application_id = acaf.dw_application_id
WHERE af.application_type = {product} AND {date_range}
AND (af.interest_rate_type = 'FIXED' OR af.interest_rate_type IS NULL)
AND af.current_decision = 'ACCEPT'
"""

df = query_snowflake(PL_qry)

#Dedup
ml = eread(pjoin(dat_dir, 'ml.feather'))
ml = ml[ml['ml_sc'].notnull()]
print(ml.shape, ml['application_id'].nunique())
#(1646140, 12) 996840
ml.fillna(value=pd.np.nan, inplace=True)
ml['ml_sc'] = ml['ml_sc'].astype('float')
ml.sort_values(by=['application_id', 'created_dt'], inplace=True)
ml = ml.drop_duplicates(subset=['application_id'], keep='first')
print(ml.shape, ml['application_id'].nunique())
#(996840, 12) 996840

df2 = df.merge(ml, left_on='id', right_on='application_id', how='left')

prov_query = """
  SELECT
     distinct (pd.target_id),
     pd.provenir_data_id                                                       AS provenir_data_id,
     pd.created_dt                                                             AS provenir_request_dt,
     pd.response -> 'messages' ->> 'TIER_SCORE_VALUE'                          AS combined_ml,
     pd.response -> 'primary_borrower' ->> 'ml_2nd_look_score'                 AS p_ml,
     pd.request -> 'primary_borrower' ->> 'RSFICOV8'                           AS p_fico,
     pd.request -> 'primary_borrower' ->> 'pre_fcf'                            AS primary_pre_fcf,
     pd.response -> 'secondary_borrower' ->> 'ml_2nd_look_score'               AS s_ml,
     pd.request -> 'secondary_borrower' ->> 'RSFICOV8'                         AS s_fico,
     pd.request -> 'secondary_borrower' ->> 'pre_fcf'                          AS secondary_pre_fcf
  FROM products.provenir_data as pd
  WHERE pd.response ->> 'version' = '3' and pd.response ->> 'decision' = 'ACCEPT'
"""

prov = query_products(prov_query)
esave(prov, pjoin(dat_dir, 'prov.feather'))
prov.sort_values(by=['target_id', 'provenir_data_id', 'provenir_request_dt'], inplace=True)
prov.fillna(value=pd.np.nan, inplace=True)
prov['target_id'] = prov['target_id'].astype(int)
prov['p_ml'] = prov['p_ml'].astype(float)
prov['primary_pre_fcf'] = prov['primary_pre_fcf'].astype(float)
prov['p_fico'] = prov['p_fico'].astype(float)
print(prov.shape, prov['target_id'].nunique())
#(653360, 10) 201817
prov_2 = prov.drop_duplicates(subset=['target_id'], keep='last')
print(prov_2.shape, prov_2['target_id'].nunique())
#(201817, 10) 201817

df3 = pd.merge(df2, prov_2, left_on='id', right_on='target_id', how='left')
print(df3[(df3['ml_sc'].isnull()) & (df3['combined_ml'].isnull())].shape[0] / df3.shape[0],
      df3[df3['ml_sc'].isnull()].shape[0] / df3.shape[0],
      df3[df3['combined_ml'].isnull()].shape[0] / df3.shape[0])
#0.08455513784461152 0.4327067669172932 0.22791353383458646

#Test logic of payment
df = eread('/home/ec2-user/SageMaker/adhoc/AS_historical/raw_rate_as_historical.feather')
calc_monthly_payments(16000, 5, 0.12970)

loan_amt = 16000
n_5 = 60
rate_5 = 0.1297
i_5 = rate_5/12
d_5 = ((1+i_5)**n_5-1) / (i_5 *(1+i_5)**n_5) #discount factor
p_5 = loan_amt / d_5 #final payment amount

#p_5 is the monthly_payment








