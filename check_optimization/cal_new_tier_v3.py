#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  5 09:32:18 2019

@author: esun

Calculate new tier
"""

from util_data import query_products
dat_dir = '/home/ec2-user/SageMaker/adhoc/check_optimization'
sql_ml = """
select mm.*, mrv.response_value as ml_sc
from ml_model mm
left join
(select *  from ml_response_value
where response_key in ('score')) as mrv
on mrv.ml_model_id=mm.ml_model_id
"""

ml = query_products(sql_ml)
esave(ml, pjoin(dat_dir, 'ml.feather'))
print(ml.shape, ml['application_id'].nunique())
#Out[829]: (1762005, 12) 1070953

df = eread('/home/ec2-user/SageMaker/adhoc/AS_historical/as_with_rate.feather')
df.shape
#Out[896]: (28996, 84)
#Dedup
ml = eread(pjoin(dat_dir, 'ml.feather'))
ml = ml[ml['ml_sc'].notnull()]
print(ml.shape, ml['application_id'].nunique())
#(1646140, 12) 996840
ml.fillna(value=pd.np.nan, inplace=True)
ml['ml_sc'] = ml['ml_sc'].astype('float')
ml.sort_values(by=['application_id', 'created_dt', 'ml_sc'], ascending=[True, True, False], inplace=True)
ml = ml.drop_duplicates(subset=['application_id'], keep='first')
print(ml.shape, ml['application_id'].nunique())
#(996840, 12) 996840
ml.rename(columns={'application_id': 'id'}, inplace=True)
df2 = df.merge(ml[['id', 'ml_sc']], on='id', how='left')

prov_query = """
  SELECT
     distinct (pd.target_id),
     pd.provenir_data_id                                                       AS provenir_data_id,
     pd.created_dt                                                             AS provenir_request_dt,
     pd.response -> 'messages' ->> 'TIER_SCORE_VALUE'                          AS combined_ml,
     pd.response -> 'primary_borrower' ->> 'ml_2nd_look_score'                 AS p_ml,
     pd.request -> 'primary_borrower' ->> 'RSFICOV8'                           AS p_fico,
     pd.request -> 'primary_borrower' ->> 'pre_fcf'                            AS primary_pre_fcf,
     pd.response -> 'secondary_borrower' ->> 'ml_2nd_look_score'               AS s_ml,
     pd.request -> 'secondary_borrower' ->> 'RSFICOV8'                         AS s_fico,
     pd.request -> 'secondary_borrower' ->> 'pre_fcf'                          AS secondary_pre_fcf
  FROM products.provenir_data as pd
  WHERE pd.response ->> 'version' = '3' and pd.response ->> 'decision' = 'ACCEPT'
"""

prov = query_products(prov_query)
esave(prov, pjoin(dat_dir, 'prov.feather'))
prov = eread(pjoin(dat_dir, 'prov.feather'))
prov.sort_values(by=['target_id', 'provenir_data_id', 'provenir_request_dt'], inplace=True)
prov.fillna(value=pd.np.nan, inplace=True)
prov['target_id'] = prov['target_id'].astype(int)
prov['combined_ml'] = prov['p_ml'].astype(float)
prov['primary_pre_fcf'] = prov['primary_pre_fcf'].astype(float)
prov['p_fico'] = prov['p_fico'].astype(float)
print(prov.shape, prov['target_id'].nunique())
#(653360, 10) 201817
prov_2 = prov.drop_duplicates(subset=['target_id'], keep='last')
print(prov_2.shape, prov_2['target_id'].nunique())
#(201817, 10) 201817
prov_2.rename(columns={'target_id': 'id'}, inplace=True)

df3 = pd.merge(df2, prov_2[['id', 'combined_ml', 'primary_pre_fcf', 'p_fico']], on='id', how='left')
df3.shape
#(28996, 88)
print(df3[(df3['ml_sc'].isnull()) & (df3['combined_ml'].isnull())].shape[0] / df3.shape[0],
      df3[df3['ml_sc'].isnull()].shape[0] / df3.shape[0],
      df3[df3['combined_ml'].isnull()].shape[0] / df3.shape[0])
#0.0824941371223617 0.43309421989239894 0.22147882466547109

#QC -> pass
compare_dat_3(df3, 'combined_ml', 'ml_sc')
compare_dat_3(df3, 'credit_score', 'p_fico')

df3['final_ml_sc'] = np.where(df3['combined_ml'].notnull(), df3['combined_ml'], df3['ml_sc'])
esave(df3, pjoin(dat_dir, 'as_with_ml_score.feather'))

#Generate tier
rate_qry = f"""SELECT
af.id,
af.requested_amount,
af.credit_score,
af.fico,
af.free_cash_flow_pre,
ui.MONTHLY_LINE_ITEM_EXPENSE,
p.product_term,
of.min_rate,
SUBSTRING(o.min_tier_code,'[1-9]')::INT AS tier,
of.max_amount
FROM dwmart.applications_file af
left JOIN product_application_facts paf on af.dw_application_id = paf.application_id
JOIN underwriting_info ui ON ui.underwriting_info_id = coalesce(NULLIF(paf.final_uw_id, 0), NULLIF(paf.selected_uw_id, 0), NULLIF(paf. initial_uw_id, 0))
JOIN offer_facts of ON of.underwriting_info_id = ui.underwriting_info_id
JOIN offer_details o on of.offer_details_id = o.offer_details_id
JOIN products p ON p.product_id = of.product_id
WHERE o.rate_type_code = 'FIXED' AND o.eligible = TRUE
and af.application_type = {product} AND {date_range}
AND af.current_decision = 'ACCEPT'
"""

df_rate = query_sofidw(rate_qry)
print(df_rate.shape, df_rate['id'].nunique())
#(437291, 10) 32725
df_rate.sort_values(by=['id', 'product_term', 'max_amount'], inplace=True)
df2 = df_rate.drop_duplicates(subset=['id', 'product_term', 'min_rate'], keep='last') #For each unique id, product_term and min_rate, keep the largest max_amount
df4 = df2[df2['max_amount'] >= df2['requested_amount']]
df4 = df4.sort_values(by=['id', 'product_term', 'max_amount'])
df4 = df4.drop_duplicates(subset=['id', 'product_term'], keep='first')
print(df4.shape, df4['id'].nunique())
#(124707, 10) 29785

df4['monthly_payment'] = calc_monthly_payments(df4['requested_amount'], df4['product_term'], df4['min_rate'])
df4['free_cash_flow_post'] = df4['free_cash_flow_pre'] - df4['monthly_payment']
df4['pni_post'] = df4['free_cash_flow_post'] - df4['monthly_line_item_expense']
esave(df4, pjoin(dat_dir, 'rate_as.csv'))

df = df4
#Create free_cash_flow_post bins
conditions = [(df['free_cash_flow_post']>=6000),
        ((df['free_cash_flow_post'] >=5000)&(df['free_cash_flow_post'] <6000)),
        ((df['free_cash_flow_post'] >=4000)&(df['free_cash_flow_post'] <5000)),
        ((df['free_cash_flow_post'] >=3000)&(df['free_cash_flow_post'] <4000)),
        ((df['free_cash_flow_post'] >=2000)&(df['free_cash_flow_post'] <3000)),
        ((df['free_cash_flow_post'] >=1000)&(df['free_cash_flow_post'] <2000)) ]
choices = [1, 2, 3, 4, 5, 6]
df['fcf_bins'] = np.select(conditions, choices)

#Create ml_score bins
df_all = eread(pjoin(dat_dir, 'as_with_ml_score.feather'))
df = pd.merge(df, df_all[['id', 'final_ml_sc']], how='left', on='id')
#df[df['final_ml_sc'].isnull()]['id'].nunique()
#Out[954]: 3179
df = df[df['final_ml_sc'].notnull()]
print(df.shape, df['id'].nunique())
#(112138, 15) 26603
df = df[df['final_ml_sc'] > 61.2]
print(df.shape, df['id'].nunique())
#(112060, 15) 26576
bins = [61.199000000000005, 62.5,64.4, 65.622,67.943,69.898, 71.756, 73.739, 76.051, 79.241, 100]
df['ml_bins'] = pd.cut(df['final_ml_sc'], bins, right=True)
conditions = [ ((df['final_ml_sc'] >61.2)&(df['final_ml_sc'] <=62.5)),
        ((df['final_ml_sc'] >62.5)&(df['final_ml_sc'] <=64.4)),
        ((df['final_ml_sc'] >64.4)&(df['final_ml_sc'] <=65.62)),
        ((df['final_ml_sc'] >65.62)&(df['final_ml_sc'] <=67.94)),
        ((df['final_ml_sc'] >67.94)&(df['final_ml_sc'] <=69.90)),
        ((df['final_ml_sc'] >69.90)&(df['final_ml_sc'] <=71.756)),
        ((df['final_ml_sc'] >71.756)&(df['final_ml_sc'] <=73.74)),
        ((df['final_ml_sc'] >73.74)&(df['final_ml_sc'] <=76.05)),
        ((df['final_ml_sc'] >76.05)&(df['final_ml_sc'] <=79.24)),
        ((df['final_ml_sc'] >79.24)&(df['final_ml_sc'] <=90))]
choices = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
df['ml_bin'] = np.select(conditions, choices)

#PNI condistion
conditions = [ (df['pni_post']>=0),
                      ((df['pni_post'] <0)&(df['pni_post'] >=-500)),
                      (df['pni_post'] <-500)]
choices = [1, 2, 3]
df['post_pin_bins'] = np.select(conditions, choices)

#assign tiers
m = {
    '(79.241, 100.0]11': '1',
    '(79.241, 100.0]21': '1',
    '(79.241, 100.0]31': '1',
    '(79.241, 100.0]41': '2',
    '(79.241, 100.0]51': '2',
    '(79.241, 100.0]61': '2',
    '(79.241, 100.0]12': '2',
    '(79.241, 100.0]22': '2',
    '(79.241, 100.0]32': '2',
    '(79.241, 100.0]42': '2',
    '(79.241, 100.0]52': '2',
    '(79.241, 100.0]62': '2',
    '(76.051, 79.241]11': '1',
    '(76.051, 79.241]21': '2',
    '(76.051, 79.241]31': '2',
    '(76.051, 79.241]41': '3',
    '(76.051, 79.241]51': '3',
    '(76.051, 79.241]61': '3',
    '(76.051, 79.241]12': '3',
    '(76.051, 79.241]22': '3',
    '(76.051, 79.241]32': '3',
    '(76.051, 79.241]42': '3',
    '(76.051, 79.241]52': '3',
    '(76.051, 79.241]62': '3',   
    '(73.739, 76.051]11': '1',
    '(73.739, 76.051]21': '2',
    '(73.739, 76.051]31': '2',
    '(73.739, 76.051]41': '3',
    '(73.739, 76.051]51': '3',
    '(73.739, 76.051]61': '4',
    '(73.739, 76.051]12': '4',
    '(73.739, 76.051]22': '4',
    '(73.739, 76.051]32': '4',
    '(73.739, 76.051]42': '4',
    '(73.739, 76.051]52': '4',
    '(73.739, 76.051]62': '4',
    '(71.756, 73.739]11': '2',
    '(71.756, 73.739]21': '3',
    '(71.756, 73.739]31': '3',
    '(71.756, 73.739]41': '4',
    '(71.756, 73.739]51': '4',
    '(71.756, 73.739]61': '5',
    '(71.756, 73.739]12': '5',
    '(71.756, 73.739]22': '5',
    '(71.756, 73.739]32': '5',
    '(71.756, 73.739]42': '5',
    '(71.756, 73.739]52': '5',
    '(71.756, 73.739]62': '5',
    '(69.898, 71.756]11': '2',
    '(69.898, 71.756]21': '3',
    '(69.898, 71.756]31': '4',
    '(69.898, 71.756]41': '4',
    '(69.898, 71.756]51': '5',
    '(69.898, 71.756]61': '6',
    '(69.898, 71.756]12': '6',
    '(69.898, 71.756]22': '6',
    '(69.898, 71.756]32': '6',
    '(69.898, 71.756]42': '6',
    '(69.898, 71.756]52': '6',
    '(69.898, 71.756]62': '6', 
    '(67.943, 69.898]11': '3',
    '(67.943, 69.898]21': '3',
    '(67.943, 69.898]31': '4',
    '(67.943, 69.898]41': '5',
    '(67.943, 69.898]51': '5',
    '(67.943, 69.898]61': '6',    
    '(67.943, 69.898]12': '6',
    '(67.943, 69.898]22': '6',
    '(67.943, 69.898]32': '6',
    '(67.943, 69.898]42': '6',
    '(67.943, 69.898]52': '6',
    '(67.943, 69.898]62': '6',
    '(65.622, 67.943]11': '4',
    '(65.622, 67.943]21': '4',
    '(65.622, 67.943]31': '5',
    '(65.622, 67.943]41': '6',
    '(65.622, 67.943]51': '6',
    '(65.622, 67.943]61': '7',
    '(65.622, 67.943]12': '7',
    '(65.622, 67.943]22': '7',
    '(65.622, 67.943]32': '7',
    '(65.622, 67.943]42': '7',
    '(65.622, 67.943]52': '7',
    '(65.622, 67.943]62': '7',
    '(61.199, 62.5]11': '4',
    '(61.199, 62.5]21': '5',
    '(61.199, 62.5]31': '6',
    '(61.199, 62.5]41': '7',
    '(61.199, 62.5]51': '7',
    '(61.199, 62.5]61': '7',
    '(61.199, 62.5]12': '7',
    '(61.199, 62.5]22': '7',
    '(61.199, 62.5]32': '7',
    '(61.199, 62.5]42': '7',
    '(61.199, 62.5]52': '7',
    '(61.199, 62.5]62': '7',
    '(62.5, 64.4]11': '4',
    '(62.5, 64.4]21': '5',
    '(62.5, 64.4]31': '6',
    '(62.5, 64.4]41': '7',
    '(62.5, 64.4]51': '7',
    '(62.5, 64.4]61': '7', 
    '(62.5, 64.4]12': '7',
    '(62.5, 64.4]22': '7',
    '(62.5, 64.4]32': '7',
    '(62.5, 64.4]42': '7',
    '(62.5, 64.4]52': '7',
    '(62.5, 64.4]62': '7',     
    '(64.4, 65.622]11': '4',
    '(64.4, 65.622]21': '5',
    '(64.4, 65.622]31': '6',
    '(64.4, 65.622]41': '7',
    '(64.4, 65.622]51': '7',
    '(64.4, 65.622]61': '7',  
    '(64.4, 65.622]12': '4',
    '(64.4, 65.622]22': '5',
    '(64.4, 65.622]32': '6',
    '(64.4, 65.622]42': '7',
    '(64.4, 65.622]52': '7',
    '(64.4, 65.622]62': '7'}

df['indxxno'] = df.apply(lambda x: str(x['ml_bins'])+str(x['fcf_bins'])+str(x['post_pin_bins']), axis=1)
df['new_tier'] = df['indxxno'].map(m)
df['new_tier'].isnull().sum()
#70 <=> post_pin_bins = 3
df = df.dropna(subset=['new_tier'])
df['new_tier'] = df['new_tier'].astype(int)
print(df.shape, df['id'].nunique())
#(111990, 20) 26528 droped 11% population

#
df['new_tier_final']=np.where( (df.new_tier==1) & (df.fico<700), df.new_tier+3, np.where(

(df.new_tier==1) & (df.fico<720),df.new_tier+2,np.where(

(df.new_tier==1) & (df.fico<750),df.new_tier+1,np.where(
    
(df.new_tier==2) & (df.fico<700), df.new_tier+3, np.where(

(df.new_tier==2) & (df.fico<710),df.new_tier+2,np.where(

(df.new_tier==2) & (df.fico<730),df.new_tier+1,np.where(
    
(df.new_tier==3) & (df.fico<710), df.new_tier+2, np.where(

(df.new_tier==3) & (df.fico<720),df.new_tier+1,np.where(  
    
(df.new_tier==4) & (df.fico<700), df.new_tier+2, np.where(

(df.new_tier==4) & (df.fico<710),df.new_tier+1,np.where(
(df.new_tier==5) & (df.fico<700), df.new_tier+1, 
np.where((df.new_tier==6) & (df.fico<700),df.new_tier+1,df.new_tier )
)))))))))))

df_pivot = flat_term_rate_dat(df, flat_cols=['min_rate', 'tier', 'new_tier_final'], consolidate_cols=[])
print(df_pivot.shape)
#(26528, 18)

df_pivot.to_csv(pjoin(dat_dir, 'new_tier_as.csv'))

#QC -> about 20% not match regardless of date
df = eread('/home/ec2-user/SageMaker/adhoc/AS_historical/as_with_rate.feather')
df = pd.merge(df, df_pivot[[f'new_tier_final_{term}' for term in range(2,8)]], left_on='id', right_index=True)
df['date_start'] = pd.to_datetime(df['date_start'])
for term in range(2, 8):
    compare_dat_3(df[df['date_start'] < '2019-10-14'], f'tier_{term}', f'new_tier_final_{term}')
    
#Score using new tier
#4. Read rate from grid
df = eread('/home/ec2-user/SageMaker/adhoc/AS_historical/raw_as_historical.feather')
df = pd.merge(df, df_pivot[[f'new_tier_final_{term}' for term in range(2,8)]], left_on='id', right_index=True)
df.rename(columns={f'new_tier_final_{term}': f'tier_{term}' for term in range(2, 8)}, inplace=True)
grid = read_grid('/home/ec2-user/SageMaker/adhoc/AS_historical/as_grid.xlsx', rate_col_nm='min_rate')
df = create_fico_loan_bin(df)
df = merge_grid_price(df, grid, terms=range(2,8), rate_col_nm='min_rate')
esave(df, pjoin(dat_dir, 'as_with_rate.feather'))

#5. process data
df = eread(pjoin(dat_dir, 'as_with_rate.feather'))
impute_rate_dict={'min_rate_2': 0.16677,
                 'min_rate_3': 0.16677,
                 'min_rate_4': 0.17436,
                 'min_rate_5': 0.18133,
                 'min_rate_6': 0.15313,
                 'min_rate_7': 0.16115}

def process(df, impute_rate_dict):
    #Member indicator, grad to 0/1 variable
    df['grad'] = np.where(df['g_program'].notnull(), 1, 0)
    #Define date related variables
    df['date_start'] = pd.to_datetime(df['date_start'])
    min_start_date = df['date_start'].min()
    df['date_start_year'] =  df['date_start'].dt.year
    df['date_start_month'] = df['date_start'].dt.month
    df['date_start_day'] = df['date_start'].dt.day
   
    df['attr_affiliate_referrer'] = clean_str(df['attr_affiliate_referrer'])
    g_program_list = ['Business - MBA', 'Other', 'Law - JD', 'Engineering/Computer Science - MS or higher',
                                  'Business - MS or MA or higher', 'Social Sciences - MS or MA or higher']
    channel_list = ['www.sofi.com', 'Affiliate', 'SEM', 'Organic Search', 'MKT-DM', 'P2P']
    affiliate_list = ['www.sofi.com', 'www.google.com', '2019_pl_sofi_mail', 'credit_karma', 'lending_tree', 'nerdwallet']
    df = cut_cat_var(df, 'g_program', g_program_list)
    df = cut_cat_var(df, 'consolidated_channel', channel_list)
    df = cut_cat_var(df, 'attr_affiliate_referrer', affiliate_list)
    
    #Create indicator variables for selected each term or not
    PL_terms = [2, 3, 4, 5, 6, 7]
    for term in PL_terms:
        df[f'initial_term_{term}'] = np.where((df['initial_term'] == term), 1, 0)
    #Map interest_rate to individual term columns
    df = flat_rate(df, PL_terms)
    #Consolidate tier columns
    df['tier_pre'] = df[[f'tier_{term}' for term in PL_terms]].min(axis=1)

    df['g_grad_year'] = df['date_start'].dt.year - df['g_grad_year']
    df['ug_grad_year'] = df['date_start'].dt.year - df['ug_grad_year']
    #registration_date: date_start - var
    df['registration_date'] = pd.to_datetime(df['registration_date'])
    df['registration_date'] = (df['date_start'] - df['registration_date']).dt.days
    
    df.rename(columns={f'rate_{term}': f'min_rate_{term}' for term in PL_terms}, inplace=True)
    df, _ = impute_rate(df, rate_cols=[f'min_rate_{term}' for term in PL_terms], impute_rate_dict=impute_rate_dict)
    rate_cols = [f'min_rate_{term}' for term in range(2,8)]
    df['min_all_rates'] = df[rate_cols].min(axis=1) #Minimum of all rates
    
    for term in PL_terms:
        df[f'monthly_payment_{term}'] = calc_monthly_payments(df['requested_amount'], term, df[f'min_rate_{term}_orig'])
        df[f'free_cash_flow_post_{term}'] = df['free_cash_flow_pre'] - df[f'monthly_payment_{term}']
    monthly_payment_cols = [f'monthly_payment_{term}' for term in PL_terms]
    df['min_monthly_payment'] = df[monthly_payment_cols].min(axis=1)
    df['min_monthly_payment'].fillna(df['min_monthly_payment'].max(), inplace=True)
    df['free_cash_flow_post_max'] = df['free_cash_flow_pre'] - df['min_monthly_payment']
    
    for term in range(2, 7):
        df[f'min_rate_diff_{term+1}_{term}'] = df[f'min_rate_{term+1}'] - df[f'min_rate_{term}']
    return df

df2 = process(df, impute_rate_dict)
df2.describe().transpose().to_csv(pjoin(dat_dir, 'AS_stat.csv'))
PL_terms = [2, 3, 4, 5, 6, 7]
df2 = add_signed_ind(df2, terms=PL_terms)
df2.shape
#(26341, 133)
df2.to_csv(pjoin(dat_dir, 'AS.csv'), index=None)

model_dir = '/home/ec2-user/SageMaker/round_2/experiments/7g_2019-11-26_16-29-29'
out_dir = dat_dir
#Score rate
raw_rate = score_model(glob.glob(pjoin(model_dir, 'model_rate*.txt'))[0], pjoin(out_dir, 'rate_pred_raw.csv'), 
            data_file=pjoin(dat_dir, 'AS.csv'), header_file=glob.glob(pjoin(model_dir, '*_rate_header.csv'))[0])
pred_rate = merge_rate_model(pjoin(dat_dir, 'AS.csv'), pjoin(out_dir, 'rate_pred_raw.csv'), pjoin(out_dir, 'rate_pred.csv'))

#Score term
score_model(glob.glob(pjoin(model_dir, 'model_term*.txt'))[0], pjoin(out_dir, 'term_pred.csv'), 
            data_file=pjoin(dat_dir, 'AS.csv'), header_file=glob.glob(pjoin(model_dir, '*_term_header.csv'))[0])

params_rate = load_json(glob.glob(pjoin(model_dir, 'params_rate_*.json'))[0]) 
terms = params_rate['terms']
rate_cols = ['{0}_{1}'.format(params_rate['term_col_prefix'], term) for term in terms]
pred_term_col = 'term_wgt_prob'
pred_term_prob_cols = [col + '_prob_adj' for col in rate_cols]
rate_orig_cols = [col + '_orig' for col in rate_cols]
target_term_col = 'initial_term'
target_term_prob_cols = [f'{target_term_col}_{term}' for term in terms]

term_score = score_term_model(data_file=pjoin(dat_dir, 'AS.csv'), raw_model_output_file=pjoin(out_dir, 'term_pred.csv'), 
                 output_file=pjoin(out_dir, 'pred_term_final.feather'), 
                 impute_rate_cols=rate_cols, orig_rate_cols=rate_orig_cols, terms=terms, target_term_prob_cols=target_term_prob_cols)

#Evaluation
act_term_prob_cols = ['signed_ind_{}'.format(term) for term in terms]  
rate_map_cols = [f'rate_map_{term}' for term in terms]
df_final = merge_all_pred(pjoin(out_dir, 'rate_pred.csv'), pjoin(out_dir, 'pred_term_final.feather'), 
                          pjoin(dat_dir, 'AS.csv'), act_term_prob_cols, rate_map_cols, output_file=pjoin(out_dir, 'pred_final.feather'))

final_result = report_performance_metrics([df_final], ['oot'], terms, act_term_prob_cols,
                                          pred_term_prob_cols, rate_map_cols, rate_cols, 'tier', 'tier_pre')
final_result.to_csv(pjoin(out_dir, "final_metrics.csv"))