#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  5 09:32:18 2019

@author: esun

Calculate new tier
"""

#1. Query data
start_date = '2019-08-04'
end_date = '2019-09-03'
date_range = f"af.date_start>='{start_date}' and af.date_start<='{end_date}'"

PL_qry = f"""
SELECT
af.id,
af.date_signed,
af.date_doc_upload,
af.date_start,
af.initial_term,
af.interest_rate,
af.requested_amount,
af.gross_income,
af.app_created_via_mobile,
af.g_program,
af.g_grad_year,
af.credit_score,
af.employer_name,
af.years_of_experience,
af.pl_funds_use,
af.tax_burden_amount,
af.consolidated_channel,
af.attr_affiliate_referrer,
af.campaign_welcome_bonus,
af.housing_status,
acaf.pil0438,
af.registration_date,
af.revolving_credit_amount,
af.tier,
af.ug_ctgry,
af.ug_grad_year,
af.ug_program,
af.member_indicator,
acaf.iln5020,
acaf.alx8220,
acaf.all8020,
acaf.iln5820,
acaf.all0416,
acaf.REV0416,
acaf.mtf5020,
acaf.mta5020,
acaf.mta5830,
acaf.mtj5030,
acaf.mtj5820,
acaf.all5820,
acaf.all7516,
acaf.all8220,
acaf.all5020,
acaf.bcc7110,
acaf.bcc8322,
acaf.bcx3421,
acaf.bcx3422,
acaf.bcx5020,
acaf.bcx5320,
acaf.bcx7110,
acaf.rev5020,
acaf.rev8320,
acaf.rta7300,
acaf.rtr5030,
acaf.all7517,
acaf.iln0416,
acaf.mta0416,
acaf.mta1370,
acaf.mta2800,
acaf.mta8150,
acaf.mta8153,
acaf.mta8157,
acaf.mta8160,
acaf.mtf0416,
acaf.mtf4260,
acaf.mtf8166,
acaf.mts5020,
af.free_cash_flow_pre,
CASE WHEN af.date_doc_upload IS NOT NULL THEN 1 ELSE 0 END AS docs_ind,
CASE WHEN af.date_signed IS NOT NULL THEN 1 ELSE 0 END AS signed_ind
FROM dwmart.applications_file af
JOIN dwmart.application_credit_attributes_file acaf ON af.dw_application_id = acaf.dw_application_id
WHERE af.application_type = {product} AND {date_range}
AND (af.interest_rate_type = 'FIXED' OR af.interest_rate_type IS NULL)
AND af.current_decision = 'ACCEPT'
"""

rate_qry = f"""SELECT
af.id,
af.requested_amount,
af.credit_score,
af.fico,
af.free_cash_flow_pre,
ui.MONTHLY_LINE_ITEM_EXPENSE,
p.product_term,
of.min_rate,
SUBSTRING(o.min_tier_code,'[1-9]')::INT AS tier,
of.max_amount
FROM dwmart.applications_file af
left JOIN product_application_facts paf on af.dw_application_id = paf.application_id
JOIN underwriting_info ui ON ui.underwriting_info_id = coalesce(NULLIF(paf.final_uw_id, 0), NULLIF(paf.selected_uw_id, 0), NULLIF(paf. initial_uw_id, 0))
JOIN offer_facts of ON of.underwriting_info_id = ui.underwriting_info_id
JOIN offer_details o on of.offer_details_id = o.offer_details_id
JOIN products p ON p.product_id = of.product_id
WHERE o.rate_type_code = 'FIXED' AND o.eligible = TRUE
and af.application_type = {product} AND {date_range}
AND af.current_decision = 'ACCEPT'
"""
dat_dir = '/home/ec2-user/SageMaker/adhoc/check_optimization/aug'
df = query_snowflake(PL_qry)
df.shape
#(36639, 70)
esave(df, pjoin(dat_dir, 'raw.feather'))

df_rate = query_sofidw(rate_qry)
print(df_rate.shape, df_rate['id'].nunique())
#(507707, 10) 37320
esave(df_rate, pjoin(dat_dir, 'raw_rate.feather'))

ml = eread('/home/ec2-user/SageMaker/adhoc/check_optimization/ml_process.csv')
df = df.merge(ml[['id', 'ml_sc']], on='id')
df.shape
#(36636, 71)
esave(df, pjoin(dat_dir, 'raw_ml.feather'))


df_rate.sort_values(by=['id', 'product_term', 'max_amount'], inplace=True)
df2 = df_rate.drop_duplicates(subset=['id', 'product_term', 'min_rate'], keep='last') #For each unique id, product_term and min_rate, keep the largest max_amount
df4 = df2[df2['max_amount'] >= df2['requested_amount']]
df4 = df4.sort_values(by=['id', 'product_term', 'max_amount'])
df4 = df4.drop_duplicates(subset=['id', 'product_term'], keep='first')
print(df4.shape, df4['id'].nunique())
#(138619, 10) 33411

df4['monthly_payment'] = calc_monthly_payments(df4['requested_amount'], df4['product_term'], df4['min_rate'])
df4['free_cash_flow_post'] = df4['free_cash_flow_pre'] - df4['monthly_payment']
df4['pni_post'] = df4['free_cash_flow_post'] - df4['monthly_line_item_expense']
esave(df4, pjoin(dat_dir, 'rate.csv'))

df = df4
#Create free_cash_flow_post bins
conditions = [(df['free_cash_flow_post']>=6000),
        ((df['free_cash_flow_post'] >=5000)&(df['free_cash_flow_post'] <6000)),
        ((df['free_cash_flow_post'] >=4000)&(df['free_cash_flow_post'] <5000)),
        ((df['free_cash_flow_post'] >=3000)&(df['free_cash_flow_post'] <4000)),
        ((df['free_cash_flow_post'] >=2000)&(df['free_cash_flow_post'] <3000)),
        ((df['free_cash_flow_post'] >=1000)&(df['free_cash_flow_post'] <2000)) ]
choices = [1, 2, 3, 4, 5, 6]
df['fcf_bins'] = np.select(conditions, choices)

#Create ml_score bins
df_all = eread(pjoin(dat_dir, 'raw_ml.feather'))
df_all.rename(columns={'ml_sc': 'final_ml_sc'}, inplace=True)
df = pd.merge(df, df_all[['id', 'final_ml_sc']], how='left', on='id')
#df[df['final_ml_sc'].isnull()]['id'].nunique()
#Out[954]: 872
df = df[df['final_ml_sc'].notnull()]
print(df.shape, df['id'].nunique())
#(135894, 15) 32733
df = df[df['final_ml_sc'] > 61.2]
print(df.shape, df['id'].nunique())
#(135530, 15) 32610
bins = [61.199000000000005, 62.5,64.4, 65.622,67.943,69.898, 71.756, 73.739, 76.051, 79.241, 100]
df['ml_bins'] = pd.cut(df['final_ml_sc'], bins, right=True)
conditions = [ ((df['final_ml_sc'] >61.2)&(df['final_ml_sc'] <=62.5)),
        ((df['final_ml_sc'] >62.5)&(df['final_ml_sc'] <=64.4)),
        ((df['final_ml_sc'] >64.4)&(df['final_ml_sc'] <=65.62)),
        ((df['final_ml_sc'] >65.62)&(df['final_ml_sc'] <=67.94)),
        ((df['final_ml_sc'] >67.94)&(df['final_ml_sc'] <=69.90)),
        ((df['final_ml_sc'] >69.90)&(df['final_ml_sc'] <=71.756)),
        ((df['final_ml_sc'] >71.756)&(df['final_ml_sc'] <=73.74)),
        ((df['final_ml_sc'] >73.74)&(df['final_ml_sc'] <=76.05)),
        ((df['final_ml_sc'] >76.05)&(df['final_ml_sc'] <=79.24)),
        ((df['final_ml_sc'] >79.24)&(df['final_ml_sc'] <=90))]
choices = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
df['ml_bin'] = np.select(conditions, choices)

#PNI condistion
conditions = [ (df['pni_post']>=0),
                      ((df['pni_post'] <0)&(df['pni_post'] >=-500)),
                      (df['pni_post'] <-500)]
choices = [1, 2, 3]
df['post_pin_bins'] = np.select(conditions, choices)

#assign tiers
m = {
    '(79.241, 100.0]11': '1',
    '(79.241, 100.0]21': '1',
    '(79.241, 100.0]31': '1',
    '(79.241, 100.0]41': '2',
    '(79.241, 100.0]51': '2',
    '(79.241, 100.0]61': '2',
    '(79.241, 100.0]12': '2',
    '(79.241, 100.0]22': '2',
    '(79.241, 100.0]32': '2',
    '(79.241, 100.0]42': '2',
    '(79.241, 100.0]52': '2',
    '(79.241, 100.0]62': '2',
    '(76.051, 79.241]11': '1',
    '(76.051, 79.241]21': '2',
    '(76.051, 79.241]31': '2',
    '(76.051, 79.241]41': '3',
    '(76.051, 79.241]51': '3',
    '(76.051, 79.241]61': '3',
    '(76.051, 79.241]12': '3',
    '(76.051, 79.241]22': '3',
    '(76.051, 79.241]32': '3',
    '(76.051, 79.241]42': '3',
    '(76.051, 79.241]52': '3',
    '(76.051, 79.241]62': '3',   
    '(73.739, 76.051]11': '1',
    '(73.739, 76.051]21': '2',
    '(73.739, 76.051]31': '2',
    '(73.739, 76.051]41': '3',
    '(73.739, 76.051]51': '3',
    '(73.739, 76.051]61': '4',
    '(73.739, 76.051]12': '4',
    '(73.739, 76.051]22': '4',
    '(73.739, 76.051]32': '4',
    '(73.739, 76.051]42': '4',
    '(73.739, 76.051]52': '4',
    '(73.739, 76.051]62': '4',
    '(71.756, 73.739]11': '2',
    '(71.756, 73.739]21': '3',
    '(71.756, 73.739]31': '3',
    '(71.756, 73.739]41': '4',
    '(71.756, 73.739]51': '4',
    '(71.756, 73.739]61': '5',
    '(71.756, 73.739]12': '5',
    '(71.756, 73.739]22': '5',
    '(71.756, 73.739]32': '5',
    '(71.756, 73.739]42': '5',
    '(71.756, 73.739]52': '5',
    '(71.756, 73.739]62': '5',
    '(69.898, 71.756]11': '2',
    '(69.898, 71.756]21': '3',
    '(69.898, 71.756]31': '4',
    '(69.898, 71.756]41': '4',
    '(69.898, 71.756]51': '5',
    '(69.898, 71.756]61': '6',
    '(69.898, 71.756]12': '6',
    '(69.898, 71.756]22': '6',
    '(69.898, 71.756]32': '6',
    '(69.898, 71.756]42': '6',
    '(69.898, 71.756]52': '6',
    '(69.898, 71.756]62': '6', 
    '(67.943, 69.898]11': '3',
    '(67.943, 69.898]21': '3',
    '(67.943, 69.898]31': '4',
    '(67.943, 69.898]41': '5',
    '(67.943, 69.898]51': '5',
    '(67.943, 69.898]61': '6',    
    '(67.943, 69.898]12': '6',
    '(67.943, 69.898]22': '6',
    '(67.943, 69.898]32': '6',
    '(67.943, 69.898]42': '6',
    '(67.943, 69.898]52': '6',
    '(67.943, 69.898]62': '6',
    '(65.622, 67.943]11': '4',
    '(65.622, 67.943]21': '4',
    '(65.622, 67.943]31': '5',
    '(65.622, 67.943]41': '6',
    '(65.622, 67.943]51': '6',
    '(65.622, 67.943]61': '7',
    '(65.622, 67.943]12': '7',
    '(65.622, 67.943]22': '7',
    '(65.622, 67.943]32': '7',
    '(65.622, 67.943]42': '7',
    '(65.622, 67.943]52': '7',
    '(65.622, 67.943]62': '7',
    '(61.199, 62.5]11': '4',
    '(61.199, 62.5]21': '5',
    '(61.199, 62.5]31': '6',
    '(61.199, 62.5]41': '7',
    '(61.199, 62.5]51': '7',
    '(61.199, 62.5]61': '7',
    '(61.199, 62.5]12': '7',
    '(61.199, 62.5]22': '7',
    '(61.199, 62.5]32': '7',
    '(61.199, 62.5]42': '7',
    '(61.199, 62.5]52': '7',
    '(61.199, 62.5]62': '7',
    '(62.5, 64.4]11': '4',
    '(62.5, 64.4]21': '5',
    '(62.5, 64.4]31': '6',
    '(62.5, 64.4]41': '7',
    '(62.5, 64.4]51': '7',
    '(62.5, 64.4]61': '7', 
    '(62.5, 64.4]12': '7',
    '(62.5, 64.4]22': '7',
    '(62.5, 64.4]32': '7',
    '(62.5, 64.4]42': '7',
    '(62.5, 64.4]52': '7',
    '(62.5, 64.4]62': '7',     
    '(64.4, 65.622]11': '4',
    '(64.4, 65.622]21': '5',
    '(64.4, 65.622]31': '6',
    '(64.4, 65.622]41': '7',
    '(64.4, 65.622]51': '7',
    '(64.4, 65.622]61': '7',  
    '(64.4, 65.622]12': '4',
    '(64.4, 65.622]22': '5',
    '(64.4, 65.622]32': '6',
    '(64.4, 65.622]42': '7',
    '(64.4, 65.622]52': '7',
    '(64.4, 65.622]62': '7'}

df['indxxno'] = df.apply(lambda x: str(x['ml_bins'])+str(x['fcf_bins'])+str(x['post_pin_bins']), axis=1)
df['new_tier'] = df['indxxno'].map(m)
df['new_tier'].isnull().sum()
#119 <=> post_pin_bins = 3
df = df.dropna(subset=['new_tier'])
df['new_tier'] = df['new_tier'].astype(int)
print(df.shape, df['id'].nunique())
#(135411, 20) 32517 droped 0.5% population

df['new_tier_final']=np.where( (df.new_tier==1) & (df.fico<700), df.new_tier+3, np.where(

(df.new_tier==1) & (df.fico<720),df.new_tier+2,np.where(

(df.new_tier==1) & (df.fico<750),df.new_tier+1,np.where(
    
(df.new_tier==2) & (df.fico<700), df.new_tier+3, np.where(

(df.new_tier==2) & (df.fico<710),df.new_tier+2,np.where(

(df.new_tier==2) & (df.fico<730),df.new_tier+1,np.where(
    
(df.new_tier==3) & (df.fico<710), df.new_tier+2, np.where(

(df.new_tier==3) & (df.fico<720),df.new_tier+1,np.where(  
    
(df.new_tier==4) & (df.fico<700), df.new_tier+2, np.where(

(df.new_tier==4) & (df.fico<710),df.new_tier+1,np.where(
(df.new_tier==5) & (df.fico<700), df.new_tier+1, 
np.where((df.new_tier==6) & (df.fico<700),df.new_tier+1,df.new_tier )
)))))))))))

df_pivot = flat_term_rate_dat(df, flat_cols=['min_rate', 'tier', 'new_tier_final'], consolidate_cols=[])
print(df_pivot.shape)
#(32517, 18)

df_pivot.to_csv(pjoin(dat_dir, 'new_tier.csv'))

#QC -> about 20% not match regardless of date, 62% population difference
compare_dat_3(df, 'tier', 'new_tier')
    
#Score using new tier
#4. Read rate from grid
df = eread(pjoin(dat_dir, 'raw.feather'))
df = pd.merge(df, df_pivot[[f'new_tier_final_{term}' for term in range(2,8)]], left_on='id', right_index=True)
df.rename(columns={f'new_tier_final_{term}': f'tier_{term}' for term in range(2, 8)}, inplace=True)
grid = read_grid('/home/ec2-user/SageMaker/adhoc/AS_historical/as_grid.xlsx', rate_col_nm='min_rate')
df = create_fico_loan_bin(df)
df = merge_grid_price(df, grid, terms=range(2,8), rate_col_nm='min_rate')
esave(df, pjoin(dat_dir, 'data.feather'))

#5. process data
df = eread(pjoin(dat_dir, 'data.feather'))
impute_rate_dict={'min_rate_2': 0.16677,
                 'min_rate_3': 0.16677,
                 'min_rate_4': 0.17436,
                 'min_rate_5': 0.18133,
                 'min_rate_6': 0.15313,
                 'min_rate_7': 0.16115}

def process(df, impute_rate_dict):
    #Member indicator, grad to 0/1 variable
    df['grad'] = np.where(df['g_program'].notnull(), 1, 0)
    #Define date related variables
    df['date_start'] = pd.to_datetime(df['date_start'])
    min_start_date = df['date_start'].min()
    df['date_start_year'] =  df['date_start'].dt.year
    df['date_start_month'] = df['date_start'].dt.month
    df['date_start_day'] = df['date_start'].dt.day
   
    df['attr_affiliate_referrer'] = clean_str(df['attr_affiliate_referrer'])
    g_program_list = ['Business - MBA', 'Other', 'Law - JD', 'Engineering/Computer Science - MS or higher',
                                  'Business - MS or MA or higher', 'Social Sciences - MS or MA or higher']
    channel_list = ['www.sofi.com', 'Affiliate', 'SEM', 'Organic Search', 'MKT-DM', 'P2P']
    affiliate_list = ['www.sofi.com', 'www.google.com', '2019_pl_sofi_mail', 'credit_karma', 'lending_tree', 'nerdwallet']
    df = cut_cat_var(df, 'g_program', g_program_list)
    df = cut_cat_var(df, 'consolidated_channel', channel_list)
    df = cut_cat_var(df, 'attr_affiliate_referrer', affiliate_list)
    
    #Create indicator variables for selected each term or not
    PL_terms = [2, 3, 4, 5, 6, 7]
    for term in PL_terms:
        df[f'initial_term_{term}'] = np.where((df['initial_term'] == term), 1, 0)
    #Map interest_rate to individual term columns
    df = flat_rate(df, PL_terms)
    #Consolidate tier columns
    df['tier_pre'] = df[[f'tier_{term}' for term in PL_terms]].min(axis=1)

    df['g_grad_year'] = df['date_start'].dt.year - df['g_grad_year']
    df['ug_grad_year'] = df['date_start'].dt.year - df['ug_grad_year']
    #registration_date: date_start - var
    df['registration_date'] = pd.to_datetime(df['registration_date'])
    df['registration_date'] = (df['date_start'] - df['registration_date']).dt.days
    
    df.rename(columns={f'rate_{term}': f'min_rate_{term}' for term in PL_terms}, inplace=True)
    df, _ = impute_rate(df, rate_cols=[f'min_rate_{term}' for term in PL_terms], impute_rate_dict=impute_rate_dict)
    rate_cols = [f'min_rate_{term}' for term in range(2,8)]
    df['min_all_rates'] = df[rate_cols].min(axis=1) #Minimum of all rates
    
    for term in PL_terms:
        df[f'monthly_payment_{term}'] = calc_monthly_payments(df['requested_amount'], term, df[f'min_rate_{term}_orig'])
        df[f'free_cash_flow_post_{term}'] = df['free_cash_flow_pre'] - df[f'monthly_payment_{term}']
    monthly_payment_cols = [f'monthly_payment_{term}' for term in PL_terms]
    df['min_monthly_payment'] = df[monthly_payment_cols].min(axis=1)
    df['min_monthly_payment'].fillna(df['min_monthly_payment'].max(), inplace=True)
    df['free_cash_flow_post_max'] = df['free_cash_flow_pre'] - df['min_monthly_payment']
    
    for term in range(2, 7):
        df[f'min_rate_diff_{term+1}_{term}'] = df[f'min_rate_{term+1}'] - df[f'min_rate_{term}']
    return df

df2 = process(df, impute_rate_dict)
df2.describe().transpose().to_csv(pjoin(dat_dir, 'AS_stat.csv'))
PL_terms = [2, 3, 4, 5, 6, 7]
df2 = add_signed_ind(df2, terms=PL_terms)
df2.shape
#(34640, 133)
df2.to_csv(pjoin(dat_dir, 'data_process.csv'), index=None)

model_dir = '/home/ec2-user/SageMaker/round_2/experiments/7g_2019-11-26_16-29-29'
out_dir = dat_dir
#Score rate
raw_rate = score_model(glob.glob(pjoin(model_dir, 'model_rate*.txt'))[0], pjoin(out_dir, 'rate_pred_raw.csv'), 
            data_file=pjoin(dat_dir, 'data_process.csv'), header_file=glob.glob(pjoin(model_dir, '*_rate_header.csv'))[0])
pred_rate = merge_rate_model(pjoin(dat_dir, 'data_process.csv'), pjoin(out_dir, 'rate_pred_raw.csv'), pjoin(out_dir, 'rate_pred.csv'))

#Score term
score_model(glob.glob(pjoin(model_dir, 'model_term*.txt'))[0], pjoin(out_dir, 'term_pred.csv'), 
            data_file=pjoin(dat_dir, 'data_process.csv'), header_file=glob.glob(pjoin(model_dir, '*_term_header.csv'))[0])

params_rate = load_json(glob.glob(pjoin(model_dir, 'params_rate_*.json'))[0]) 
terms = params_rate['terms']
rate_cols = ['{0}_{1}'.format(params_rate['term_col_prefix'], term) for term in terms]
pred_term_col = 'term_wgt_prob'
pred_term_prob_cols = [col + '_prob_adj' for col in rate_cols]
rate_orig_cols = [col + '_orig' for col in rate_cols]
target_term_col = 'initial_term'
target_term_prob_cols = [f'{target_term_col}_{term}' for term in terms]

term_score = score_term_model(data_file=pjoin(dat_dir, 'data_process.csv'), raw_model_output_file=pjoin(out_dir, 'term_pred.csv'), 
                 output_file=pjoin(out_dir, 'pred_term_final.feather'), 
                 impute_rate_cols=rate_cols, orig_rate_cols=rate_orig_cols, terms=terms, target_term_prob_cols=target_term_prob_cols)

#Evaluation
act_term_prob_cols = ['signed_ind_{}'.format(term) for term in terms]  
rate_map_cols = [f'rate_map_{term}' for term in terms]
df_final = merge_all_pred(pjoin(out_dir, 'rate_pred.csv'), pjoin(out_dir, 'pred_term_final.feather'), 
                          pjoin(dat_dir, 'data_process.csv'), act_term_prob_cols, rate_map_cols, output_file=pjoin(out_dir, 'pred_final.feather'))

final_result = report_performance_metrics([df_final], ['oot'], terms, act_term_prob_cols,
                                          pred_term_prob_cols, rate_map_cols, rate_cols, 'tier', 'tier_pre')
final_result.to_csv(pjoin(out_dir, "final_metrics.csv"))


#Check tier distribution
#Aug
df2['min_tier'] = df[[f'tier_{term}' for term in range(2,8)]].min(axis=1)
df2['min_tier'].value_counts() / df2.shape[0]
1.0    0.197250
2.0    0.192156
3.0    0.178563
4.0    0.140594
5.0    0.120594
6.0    0.094719
7.0    0.076125
