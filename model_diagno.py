#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 21 16:29:26 2019

@author: esun

Diagnoise the model performance to understand why it is working or not
"""
from os.path import join as pjoin
import glob
import pandas as pd
from util_model import Header

def rename_columns(columns, postfix):
    """Add postfix to each column
    """
    return {col: col + '_' + postfix for col in columns}

def merge_var_imp(model_dir, exp_name_1, exp_name_2, model):
    """Merge variable importance for two experiments in a given model_dir for a given model (term or rate)
    """
    assert model in ['rate', 'term'], "Please select model from ['rate', 'term']!"
    var_imp_1 = pd.read_csv(glob.glob(pjoin(model_dir, f'{exp_name_1}_*', f'variable_importance_{model}*'))[0], 
                            usecols=['Variable', 'Rel_imp_gain', 'Rel_imp_split'])
    var_imp_2 = pd.read_csv(glob.glob(pjoin(model_dir, f'{exp_name_2}_*', f'variable_importance_{model}*'))[0],
                            usecols=['Variable', 'Rel_imp_gain', 'Rel_imp_split'])
    var_imp_1.rename(columns=rename_columns(var_imp_1.columns[1:], exp_name_1), inplace=True)
    var_imp_2.rename(columns=rename_columns(var_imp_2.columns[1:], exp_name_2), inplace=True)
    df = pd.merge(var_imp_1, var_imp_2, on='Variable', how='outer')
    df.sort_values(f'Rel_imp_gain_{exp_name_1}', inplace=True, ascending=False)
    df.to_csv(pjoin(model_dir, f'var_imp_compare_{exp_name_1}_{exp_name_2}_{model}.csv'), index=None)

merge_var_imp('/home/ec2-user/SageMaker/round_2/experiments', '7e', '7c', 'rate')
merge_var_imp('/home/ec2-user/SageMaker/round_2/experiments', '7h', '7c', 'term')

#header difference
def header_diff(header_file_1, header_file_2):
    """Output the header_difference
    """
    header_1 = Header(header_file_1)
    header_2 = Header(header_file_2)
    print ("numerical variable difference header1 - header2 = ", set(header_1.num_cols) - set(header_2.num_cols))
    print ("numerical variable differenc header2 - header1 = ", set(header_2.num_cols) - set(header_1.num_cols))
    return

header_dir = '/home/ec2-user/SageMaker/round_2/header'
header_diff(pjoin(header_dir, 'round2_7e_rate_header.csv'), pjoin(header_dir, 'round2_7f_rate_header.csv'))