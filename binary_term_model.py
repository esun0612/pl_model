#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 26 20:06:40 2019

@author: esun
"""
from os.path import join as pjoin 
import time
import sys
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/erika_utils')
import pandas as pd
from util_model import Header, object2cat, read_params, train_lightgbm, score_model, cal_term_rate
from util_data import eread, esave
from util_analysis import cust_format_time

def read_data_from_header_multilabel(header_file, data_file, terms, target_col_root_nm='initial_term', use_cat_cols=True, return_cat_cols=False):
    """Based on data_file and header_file, return X(training variables) and ys (labels)
    ys are for column names target_col_root_nm_{term} for terms
    """
    header = Header(header_file)
    df = eread(data_file)
    X = df[header.num_cols + header.cat_cols]
    ys = [df[col] for col in [f'{target_col_root_nm}_{term}' for term in terms]]
    if use_cat_cols:
        X, cat_cols = object2cat(X, use_cat_cols=header.cat_cols, return_cat_cols=True)
    else:
        X, cat_cols = object2cat(X, return_cat_cols=True)
    if return_cat_cols:
        return X, ys, cat_cols
    else:
        return X, ys
    
def merge_score_multilabel(model_out_dir, output_file, data_file, dat_time, terms,
                           impute_rate_cols, orig_rate_cols, target_term_prob_cols,
                           id_col='id', target_col='initial_term', act_rate_col='interest_rate', loan_amount_col='requested_amount'):
    """Merge raw score of each seperate model for a given time(dev, oos, oot)
    """
    prob_cols = [rate_col + '_prob' for rate_col in impute_rate_cols]
    score = pd.DataFrame()
    for idx, term in enumerate(terms):
        score_iter = pd.read_csv(pjoin(model_out_dir, f'pred_{dat_time}_{term}.csv'))
        score = pd.concat([score, score_iter], axis=1)
    score.columns = [rate_col + '_prob' for rate_col in impute_rate_cols]
    df = pd.read_csv(data_file, usecols=[id_col, target_col, act_rate_col, loan_amount_col] + impute_rate_cols + orig_rate_cols + target_term_prob_cols)
    df = pd.concat([df, score], axis=1)
    df = cal_term_rate(df, impute_rate_cols, orig_rate_cols, terms, prob_cols)
    esave(df, output_file)
    return df

def write_header(X, y, cat_cols, output_file):
    """Write model header to a csv file
    """
    num_cols = list(set(X.columns.tolist()) - set(cat_cols))
    header_dict = {col: 'N' for col in num_cols}
    header_dict.update({col: 'C' for col in cat_cols})
    header_dict.update({y.name: 'L'})
    header = pd.DataFrame(header_dict, index=[0])
    header = header[X.columns]
    header.to_csv(output_file, index=None)
      
def train_term_model_seperate(params_rate, params_term, model_out_dir, target_col_root_nm='signed_ind', use_monotone=True):
    """Train binary classification model for each term
    """
    header_file = params_term['header_file']
    header = Header(header_file)
    free_cash_flow = False
    for col in header.num_cols:
        if 'free_cash_flow_post' in col:
            free_cash_flow = True
            break
    terms = params_term['terms']
    fit_params_term, classifier_params_term = read_params(params_term)
    X_train, ys_train, cat_cols = read_data_from_header_multilabel(header_file, params_term['dev_dat'], terms, return_cat_cols=True)
    X_oos, ys_oos = read_data_from_header_multilabel(header_file, params_term['oos_dat'], terms)
    
    for idx, (y_train, y_oos, term) in enumerate(zip(ys_train, ys_oos, terms)):
        print(f"Term model for term {term} start training")
        time_start = time.time()
        rate_col_nm = '{0}_{1}'.format(params_term['term_col_prefix'], term)
        rate_cols = ['{0}_{1}'.format(params_term['term_col_prefix'], term) for term in terms]
        if free_cash_flow:
            fcfp_col_nm = f'free_cash_flow_post_{term}'
            fcfp_cols = [f'free_cash_flow_post_{term}' for term in terms]
            fcfp_cols.remove(fcfp_col_nm)
        rate_diff_cols = [f'min_rate_diff_{term_plus}_{term}' for term, term_plus in zip(terms[:-1], terms[1:])]
        X_train_iter = X_train.copy()
        X_oos_iter = X_oos.copy()
        rate_cols.remove(rate_col_nm)
        
        if idx == 0:
            monotone_decreasing_cols = [rate_col_nm]
            monotone_increasing_cols = [rate_diff_cols[idx]]
            del(rate_diff_cols[idx])
        elif idx == len(terms)-1:
            monotone_decreasing_cols = [rate_col_nm, rate_diff_cols[idx-1]]
            monotone_increasing_cols = []
            del(rate_diff_cols[idx-1])
        else:
            monotone_decreasing_cols = [rate_col_nm, rate_diff_cols[idx-1]]
            monotone_increasing_cols = [rate_diff_cols[idx]]
            del(rate_diff_cols[idx])
            del(rate_diff_cols[idx-1])
        
        drop_cols = rate_cols + rate_diff_cols
        if free_cash_flow:
            monotone_increasing_cols.append(fcfp_col_nm)
            drop_cols.extend(fcfp_cols)
        
        X_train_iter.drop(drop_cols, axis=1, inplace=True)
        X_oos_iter.drop(drop_cols, axis=1, inplace=True)
        write_header(X_train_iter, y_train, cat_cols, pjoin(model_out_dir, f'header_term_{term}.csv'))
        if not use_monotone:
            monotone_decreasing_cols = []
            monotone_increasing_cols = []
        model = train_lightgbm(fit_params_term, classifier_params_term, X_train_iter, y_train, X_oos_iter, y_oos, 
                               model_out_dir, 
                               model_name=f'term_{term}', 
                               monotone_decreasing_cols=monotone_decreasing_cols,
                               monotone_increasing_cols=monotone_increasing_cols)
        time_end = time.time()
        print("Term model for term {} finish training in {}".format(term, cust_format_time(time_end - time_start)))
        
    
    
        
        
        
        
        
        