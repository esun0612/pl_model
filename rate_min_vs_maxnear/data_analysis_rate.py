#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  4 09:32:19 2019

@author: esun

Understand the rates for PL
i. Percentage people map rate not equal to interest_rate
ii. What is the match rate for the other two rates?
iii. Difference among min and maxnear
"""

import pandas as pd
from util_data import map_term_to_rate
from util_analysis import compare_dat_2

df_rate = pd.read_csv('/home/ec2-user/SageMaker/input/180101_190930_process_rates.csv', nrows=1)
df = pd.read_csv('/home/ec2-user/SageMaker/input/1801_1909_correct_clean_filter.csv', 
                 usecols=['id', 'interest_rate', 'initial_term', 'signed_ind', 'loan_amt', 'requested_amount'] + list(df_rate.columns[1:]))
df.shape
#(620673, 81)

#Map interest rate
PL_terms = [2, 3, 4, 5, 6, 7]
PL_rate_cols = ['min_rate_{}_min'.format(term) for term in PL_terms]
df['map_interest_rate'] = df.apply(lambda x: map_term_to_rate(x, rate_cols=PL_rate_cols, terms=PL_terms), axis=1)

#i. Percentage people map_interest_rate not equal to interest_rate
df_min_diff_true  = df[(df['map_interest_rate'] != df['interest_rate']) & (df['interest_rate'].notnull())]
print(df_min_diff_true.shape[0], df[df['interest_rate'].notnull()].shape[0], df_min_diff_true.shape[0] / df[df['interest_rate'].notnull()].shape[0])
#143873 448146 0.3210404644914827

stat, fig = compare_dat_2(df_min_diff_true, 'interest_rate', 'map_interest_rate', 
                          '/home/ec2-user/SageMaker/output', 'min', sharey=False)
#'map_interest_rate' indeed less than 'interest_rate', overall level difference: -0.86%

#ii. What is the match rate for the other two rates?
#a. maxnear
postfix = 'maxnear'
PL_rate_cols_maxnear = ['min_rate_{}_{}'.format(term, postfix) for term in PL_terms]
df[f'map_interest_rate_{postfix}'] = df.apply(lambda x: map_term_to_rate(x, rate_cols=PL_rate_cols_maxnear, terms=PL_terms), axis=1)
df_maxnear_diff_true  = df[(df[f'map_interest_rate_{postfix}'] != df['interest_rate']) & (df['interest_rate'].notnull())]
print(df_maxnear_diff_true.shape[0], df_maxnear_diff_true.shape[0] / df[df['interest_rate'].notnull()].shape[0])
#29718 0.06631321042695908
stat_maxnear, fig_maxnear = compare_dat_2(df_maxnear_diff_true, 'interest_rate', f'map_interest_rate_{postfix}', 
                                          '/home/ec2-user/SageMaker/output', 'maxnear', sharey=False)
#'map_interest_rate_maxnear' very close to 'interest_rate', overall level difference: -0.0017%

#b. minnear
postfix = 'minnear'
PL_rate_cols_minnear = ['min_rate_{}_{}'.format(term, postfix) for term in PL_terms]
df[f'map_interest_rate_{postfix}'] = df.apply(lambda x: map_term_to_rate(x, rate_cols=PL_rate_cols_minnear, terms=PL_terms), axis=1)
df_minnear_diff_true  = df[(df[f'map_interest_rate_{postfix}'] != df['interest_rate']) & (df['interest_rate'].notnull())]
print(df_minnear_diff_true.shape[0], df_minnear_diff_true.shape[0] / df[df['interest_rate'].notnull()].shape[0])
#106793 0.23829957201447743
stat_minnear, fig_minnear = compare_dat_2(df_minnear_diff_true, 'interest_rate', f'map_interest_rate_{postfix}', 
                                          '/home/ec2-user/SageMaker/output', 'minnear', sharey=False)
#'map_interest_rate_minnear' < 'interest_rate', overall level difference: -0.73%

#Conclusion: based on above observation we should use maxnear in the model


#iii. Difference among min and maxnear => difference is huge!
result = []
for term in PL_terms:
    df_diff = df[(df[f'min_rate_{term}_min'] != df[f'min_rate_{term}_maxnear']) & (df[f'min_rate_{term}_min'].notnull())]
    diff_cnt = df_diff.shape[0]
    diff_pct = diff_cnt / df[df[f'min_rate_{term}_min'].notnull()].shape[0]
    mean_diff = df_diff[f'min_rate_{term}_min'].mean() - df_diff[f'min_rate_{term}_maxnear'].mean()
    loan_amt_min = df_diff[f'max_amount_{term}_min'].mean()
    loan_amt_maxnear = df_diff[f'max_amount_{term}_maxnear'].mean()
    loan_amt_diff = loan_amt_min - loan_amt_maxnear
    result.append([diff_cnt, diff_pct, mean_diff, loan_amt_diff, loan_amt_min, loan_amt_maxnear])

result = pd.DataFrame(result)
result.index = PL_terms
result.columns = ['diff_cnt', 'diff_pct', 'mean_min-maxnear', 'loan_amt_min-maxnear', 'loan_amt_min', 'loan_amt_maxnear']


#iv. Compare requested loan_amt, take_loan_amt and funded_loan_amt
print(df.shape[0], df['loan_amt'].notnull().sum(), df['requested_amount'].notnull().sum(), df['initial_term'].notnull().sum(),
      df['interest_rate'].notnull().sum(), df['signed_ind'].sum())
#620673 620673 620673 448146 448146 199658
df[df['loan_amt'] != df['requested_amount']].shape
#(13, 84) -> Why funded loan amount equals requested loan amount?
    
#Compare requested loan_amt, take_loan_amt_maxnear
result = []
for term in PL_terms:
    df_diff = df[(df[f'take_loan_amt_{term}_maxnear'] != df['requested_amount']) & (df[f'take_loan_amt_{term}_maxnear'].notnull())]
    diff_cnt = df_diff.shape[0]
    diff_pct = diff_cnt / df[df[f'take_loan_amt_{term}_maxnear'].notnull()].shape[0]
    take_loan_amt = df_diff[f'take_loan_amt_{term}_maxnear'].mean()
    request_loan_amt = df_diff[f'requested_amount'].mean()
    loan_amt_diff = take_loan_amt - request_loan_amt
    result.append([diff_cnt, diff_pct, loan_amt_diff, take_loan_amt, request_loan_amt])

result = pd.DataFrame(result)
result.index = PL_terms
result.columns = ['diff_cnt', 'diff_pct', 'take-request', 'take_loan_amt_maxnear', 'request_loan_amt']

#Compare requested loan_amt, take_loan_amt_min
result = []
for term in PL_terms:
    df_diff = df[(df[f'take_loan_amt_{term}_min'] != df['requested_amount']) & (df[f'take_loan_amt_{term}_min'].notnull())]
    diff_cnt = df_diff.shape[0]
    diff_pct = diff_cnt / df[df[f'take_loan_amt_{term}_min'].notnull()].shape[0]
    take_loan_amt = df_diff[f'take_loan_amt_{term}_min'].mean()
    request_loan_amt = df_diff[f'requested_amount'].mean()
    loan_amt_diff = take_loan_amt - request_loan_amt
    result.append([diff_cnt, diff_pct, loan_amt_diff, take_loan_amt, request_loan_amt])

result = pd.DataFrame(result)
result.index = PL_terms
result.columns = ['diff_cnt', 'diff_pct', 'take-request', 'take_loan_amt_min', 'request_loan_amt']