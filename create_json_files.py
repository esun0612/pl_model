#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov  8 11:42:10 2019

@author: esun
"""
from os.path import join as pjoin
from util_model import chg_json

def write_sh(exp_name):
    with open(f'/home/ec2-user/SageMaker/round_2/code/exp_{exp_name}.sh', 'w') as f:
        f.write("/home/ec2-user/anaconda3/envs/python3/bin/python /home/ec2-user/SageMaker/erika_git/pl_model/header_to_model.py " \
                f"/home/ec2-user/SageMaker/round_2/params/params_rate_{exp_name}.json /home/ec2-user/SageMaker/round_2/params/params_term_{exp_name}.json")

#11/19
params_dir = '/home/ec2-user/SageMaker/round_2/params'
header_dir = '/home/ec2-user/SageMaker/round_2/header'
chg_json(pjoin(params_dir, 'params_rate_1.json'), 
         pjoin(params_dir, 'params_rate_1b.json'), 
         change_dict={'exp_name': '1b', 
                      'header_file': pjoin(header_dir, 'round2_1b_rate_header.csv'),
                      'term_col_postfix': 'orig'})
chg_json(pjoin(params_dir, 'params_term_1.json'), 
         pjoin(params_dir, 'params_term_1b.json'), 
         change_dict={'exp_name': '1b', 
                      'header_file': pjoin(header_dir, 'round2_1b_term_header.csv'),
                      'term_col_postfix': 'orig'})

#11/20
chg_json(pjoin(params_dir, 'params_rate_1.json'), 
         pjoin(params_dir, 'params_rate_7a.json'), 
         change_dict={'exp_name': '7a', 
                      'header_file': pjoin(header_dir, 'round2_7a_rate_header.csv')})
chg_json(pjoin(params_dir, 'params_term_1.json'), 
     pjoin(params_dir, 'params_term_7a.json'), 
     change_dict={'exp_name': '7a', 
                  'header_file': pjoin(header_dir, 'round2_7a_term_header.csv')})

#11/21
chg_json(pjoin(params_dir, 'params_rate_1.json'), 
         pjoin(params_dir, 'params_rate_2.json'), 
         change_dict={'exp_name': '2', 
                      'dev_dat': "/home/ec2-user/SageMaker/round_2/data/PL_rate_train_2.csv",
                      'oos_dat': "/home/ec2-user/SageMaker/round_2/data/PL_rate_oos_2.csv"})
    
chg_json(pjoin(params_dir, 'params_term_1.json'), 
     pjoin(params_dir, 'params_term_2.json'), 
     change_dict={'exp_name': '2', 
                  'dev_dat': "/home/ec2-user/SageMaker/round_2/data/PL_term_train_2.csv",
                  'oos_dat': "/home/ec2-user/SageMaker/round_2/data/PL_term_oos_2.csv"})

#7b
chg_json(pjoin(params_dir, 'params_rate_1.json'), 
         pjoin(params_dir, 'params_rate_7b.json'), 
         change_dict={'exp_name': '7b', 
                      'header_file': pjoin(header_dir, 'round2_7b_rate_header.csv'),
                      'dev_dat': "/home/ec2-user/SageMaker/round_2/data/PL_rate_train_7b.csv",
                      'oos_dat': "/home/ec2-user/SageMaker/round_2/data/PL_rate_oos_7b.csv",
                      'oot_dat': "/home/ec2-user/SageMaker/round_2/data/PL_rate_oot_7b.csv"})
chg_json(pjoin(params_dir, 'params_term_1.json'), 
         pjoin(params_dir, 'params_term_7b.json'), 
         change_dict={'exp_name': '7b', 
                      'header_file': pjoin(header_dir, 'round2_7b_term_header.csv'),
                      'dev_dat': "/home/ec2-user/SageMaker/round_2/data/PL_term_train_7b.csv",
                      'oos_dat': "/home/ec2-user/SageMaker/round_2/data/PL_term_oos_7b.csv",
                      'oot_dat': "/home/ec2-user/SageMaker/round_2/data/PL_term_oot_7b.csv"})

#7c
for model in ['rate', 'term']:
    chg_json(pjoin(params_dir, f'params_{model}_7b.json'), 
             pjoin(params_dir, f'params_{model}_7c.json'), 
             change_dict={'exp_name': '7c', 
                          'header_file': pjoin(header_dir, f'round2_7c_{model}_header.csv')})
    
#7d
for model in ['rate', 'term']:
    chg_json(pjoin(params_dir, f'params_{model}_7b.json'), 
             pjoin(params_dir, f'params_{model}_7d.json'), 
             change_dict={'exp_name': '7d', 
                          'header_file': pjoin(header_dir, f'round2_7d_{model}_header.csv'),
                          'dev_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_train_7c.csv",
                          'oos_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_oos_7c.csv",
                          'oot_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_oot_7c.csv"})

#7e
exp_name = '7e'
for model in ['rate', 'term']:
    chg_json(pjoin(params_dir, f'params_{model}_7c.json'), 
             pjoin(params_dir, f'params_{model}_{exp_name}.json'), 
             change_dict={'exp_name': exp_name, 
                          'header_file': pjoin(header_dir, f'round2_{exp_name}_{model}_header.csv'),
                          'dev_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_train_{exp_name}.csv",
                          'oos_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_oos_{exp_name}.csv",
                          'oot_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_oot_{exp_name}.csv"})

#7f
exp_name = '7f'
for model in ['rate', 'term']:
    chg_json(pjoin(params_dir, f'params_{model}_7e.json'), 
             pjoin(params_dir, f'params_{model}_{exp_name}.json'), 
             change_dict={'exp_name': exp_name, 
                          'header_file': pjoin(header_dir, f'round2_{exp_name}_{model}_header.csv')})
write_sh(exp_name)   
    
#7g
exp_name = '7g'
for model in ['rate', 'term']:
    chg_json(pjoin(params_dir, f'params_{model}_7e.json'), 
             pjoin(params_dir, f'params_{model}_{exp_name}.json'), 
             change_dict={'exp_name': exp_name, 
                          'header_file': pjoin(header_dir, f'round2_{exp_name}_{model}_header.csv'),
                          'dev_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_train_{exp_name}.csv",
                          'oos_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_oos_{exp_name}.csv",
                          'oot_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_oot_{exp_name}.csv"})

#7h
exp_name = '7h'
for model in ['rate', 'term']:
    chg_json(pjoin(params_dir, f'params_{model}_7g.json'), 
             pjoin(params_dir, f'params_{model}_{exp_name}.json'), 
             change_dict={'exp_name': exp_name, 
                          'header_file': pjoin(header_dir, f'round2_{exp_name}_{model}_header.csv')})

#8
exp_name = '8'
for model in ['term']:
    chg_json(pjoin(params_dir, f'params_{model}_7g.json'), 
             pjoin(params_dir, f'params_{model}_{exp_name}.json'), 
             change_dict={'exp_name': exp_name, 
                          'header_file': pjoin(header_dir, f'round2_{exp_name}_{model}_header.csv'),
                          'dev_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_train_{exp_name}.csv",
                          'oos_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_oos_{exp_name}.csv",
                          'oot_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_oot_{exp_name}.csv"})
for model in ['rate']:
    chg_json(pjoin(params_dir, f'params_{model}_7g.json'), 
             pjoin(params_dir, f'params_{model}_{exp_name}.json'), 
             change_dict={'exp_name': exp_name})

#9
exp_name = '9'
for model in ['rate']:
    chg_json(pjoin(params_dir, f'params_{model}_7g.json'), 
             pjoin(params_dir, f'params_{model}_{exp_name}.json'), 
             change_dict={'exp_name': exp_name})
for model in ['term']:
    chg_json(pjoin(params_dir, f'params_{model}_7g.json'), 
             pjoin(params_dir, f'params_{model}_{exp_name}.json'), 
             change_dict={'exp_name': exp_name,
                          'seperate_model': 'True',
                          'use_monotone': 'False'})

#9b
exp_name = '9b'
for model in ['rate']:
    chg_json(pjoin(params_dir, f'params_{model}_7g.json'), 
             pjoin(params_dir, f'params_{model}_{exp_name}.json'), 
             change_dict={'exp_name': exp_name})
for model in ['term']:
    chg_json(pjoin(params_dir, f'params_{model}_9.json'), 
             pjoin(params_dir, f'params_{model}_{exp_name}.json'), 
             change_dict={'exp_name': exp_name,
                          'eval_metric': 'auc'})

#10
exp_name = '10'
for model in ['rate']:
    chg_json(pjoin(params_dir, f'params_{model}_7g.json'), 
             pjoin(params_dir, f'params_{model}_{exp_name}.json'), 
             change_dict={'exp_name': exp_name})
for model in ['term']:
    chg_json(pjoin(params_dir, f'params_{model}_9b.json'), 
             pjoin(params_dir, f'params_{model}_{exp_name}.json'), 
             change_dict={'exp_name': exp_name,
                          'use_monotone': 'True'})

#11: 12/30
exp_name = '11'
for model in ['rate']:
    chg_json(pjoin(params_dir, f'params_{model}_7c.json'), 
             pjoin(params_dir, f'params_{model}_{exp_name}.json'), 
             change_dict={'exp_name': exp_name,
                          'header_file': pjoin(header_dir, f'round2_{exp_name}_{model}_header.csv'),
                          'dev_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_train_{exp_name}.csv",
                          'oos_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_oos_{exp_name}.csv",
                          'oot_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_oot_{exp_name}.csv"})

#12: 01/14
exp_name = '12'
for model in ['rate']:
    chg_json(pjoin(params_dir, f'params_{model}_7c.json'), 
             pjoin(params_dir, f'params_{model}_{exp_name}.json'), 
             change_dict={'exp_name': exp_name,
                          'header_file': pjoin(header_dir, f'round2_{exp_name}_{model}_header.csv'),
                          'dev_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_train_{exp_name}.csv",
                          'oos_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_oos_{exp_name}.csv",
                          'oot_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_oot_{exp_name}.csv"})

exp_name = '13'
for model in ['rate']:
    chg_json(pjoin(params_dir, f'params_{model}_7c.json'), 
             pjoin(params_dir, f'params_{model}_{exp_name}.json'), 
             change_dict={'exp_name': exp_name,
                          'header_file': pjoin(header_dir, f'round2_{exp_name}_{model}_header.csv'),
                          'dev_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_train_{exp_name}.csv",
                          'oos_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_oos_{exp_name}.csv",
                          'oot_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_oot_{exp_name}.csv"})

#14: 02/12
exp_name = '14'
for model in ['term']:
    chg_json(pjoin(params_dir, f'params_{model}_7c.json'), 
             pjoin(params_dir, f'params_{model}_{exp_name}.json'), 
             change_dict={'exp_name': exp_name,
                          'header_file': pjoin(header_dir, f'round2_{exp_name}_{model}_header.csv'),
                          'dev_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_train_{exp_name}.csv",
                          'oos_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_oos_{exp_name}.csv",
                          'oot_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_oot_{exp_name}.csv"})
for model in ['rate']:
    chg_json(pjoin(params_dir, f'params_{model}_7c.json'), 
             pjoin(params_dir, f'params_{model}_{exp_name}.json'), 
             change_dict={'exp_name': exp_name,
                          'dev_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_train_{exp_name}.csv",
                          'oos_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_oos_{exp_name}.csv",
                          'oot_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_oot_{exp_name}.csv"})

#15: 02/12
exp_name = '15'
for model in ['term']:
    chg_json(pjoin(params_dir, f'params_{model}_14.json'), 
             pjoin(params_dir, f'params_{model}_{exp_name}.json'), 
             change_dict={'exp_name': exp_name,
                          'header_file': pjoin(header_dir, f'round2_{exp_name}_{model}_header.csv'),
                          'dev_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_train_{exp_name}.csv",
                          'oos_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_oos_{exp_name}.csv",
                          'oot_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_oot_{exp_name}.csv"})
for model in ['rate']:
    chg_json(pjoin(params_dir, f'params_{model}_14.json'), 
             pjoin(params_dir, f'params_{model}_{exp_name}.json'), 
             change_dict={'exp_name': exp_name,
                          'dev_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_train_{exp_name}.csv",
                          'oos_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_oos_{exp_name}.csv",
                          'oot_dat': f"/home/ec2-user/SageMaker/round_2/data/PL_{model}_oot_{exp_name}.csv"})
    
    
    
params_dir = '/home/ec2-user/SageMaker/params'
chg_json(pjoin(params_dir, 'params_rate_1.json'), 
         pjoin(params_dir, 'params_rate_2.json'), 
         change_dict={'exp_name': '2', 'header_file': '/home/ec2-user/SageMaker/headers/exp_2_rate_header.csv'}, 
         del_keys=['term_col_postfix'])
chg_json(pjoin(params_dir, 'params_term_1.json'), 
         pjoin(params_dir, 'params_term_2.json'),  
         change_dict={'exp_name': '2', 'header_file': '/home/ec2-user/SageMaker/headers/exp_2_term_header.csv'}, 
         del_keys=['term_col_postfix'])

for exp in [3, 4]:
    for model in ['rate', 'term']:
        chg_json(pjoin(params_dir, f'params_{model}_2.json'), pjoin(params_dir, f'params_{model}_{exp}.json'), 
                 change_dict={'exp_name': f'{exp}', 'header_file': f'/home/ec2-user/SageMaker/headers/exp_{exp}_{model}_header.csv'})
        
chg_json(pjoin(params_dir, 'params_rate_2.json'), 
         pjoin(params_dir, 'params_rate_5.json'), 
         change_dict={'exp_name': '5', 'header_file': '/home/ec2-user/SageMaker/headers/exp_5_rate_header.csv'})

chg_json(pjoin(params_dir, 'params_term_2.json'), 
         pjoin(params_dir, 'params_term_5.json'),  
         change_dict={'exp_name': '5', 'header_file': '/home/ec2-user/SageMaker/headers/exp_4_term_header.csv'})

    