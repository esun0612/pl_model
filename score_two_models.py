#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 25 11:30:22 2019

@author: esun

Score final financial metrics from two experiments folder of rate and term models
"""
import os
from os.path import join as pjoin
import glob
from shutil import copy
import sys
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/erika_utils')
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/pl_model')
from util_model import load_json
from header_to_model import merge_all_pred, report_performance_metrics, collect_result_model


def score_two_model(rate_model_dir, term_model_dir, output_dir):
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    rate_perf_file = glob.glob(pjoin(rate_model_dir, 'metrics_rate_*'))[0]
    term_perf_file = glob.glob(pjoin(term_model_dir, 'metrics_term_*'))[0]
    copy(rate_perf_file, output_dir)
    copy(term_perf_file, output_dir)
    params_rate_json_file = glob.glob(pjoin(rate_model_dir, 'params_rate_*.json'))[0]
    params_rate = load_json(params_rate_json_file) 
    terms = params_rate['terms']
    if 'term_col_postfix' in params_rate:
        rate_cols = ['{0}_{1}_{2}'.format(params_rate['term_col_prefix'], term, params_rate['term_col_postfix']) for term in terms]
    else:
        rate_cols = ['{0}_{1}'.format(params_rate['term_col_prefix'], term) for term in terms]
    act_term_prob_cols = ['signed_ind_{}'.format(term) for term in terms]  
    rate_map_cols = [f'rate_map_{term}' for term in terms]
    pred_term_prob_cols = [col + '_prob_adj' for col in rate_cols]
    rate_cols = ['{0}_{1}'.format(params_rate['term_col_prefix'], term) for term in terms]
    df_final_train = merge_all_pred(pjoin(rate_model_dir, 'pred_train_rate.feather'), pjoin(term_model_dir, 'pred_train_term_final.feather'), 
                                    params_rate['dev_dat'], act_term_prob_cols, rate_map_cols)
    df_final_oos = merge_all_pred(pjoin(rate_model_dir, 'pred_oos_rate.feather'), pjoin(term_model_dir, 'pred_oos_term_final.feather'), 
                                  params_rate['oos_dat'], act_term_prob_cols, rate_map_cols)
    df_final_oot = merge_all_pred(pjoin(rate_model_dir, 'pred_oot_rate.feather'), pjoin(term_model_dir, 'pred_oot_term_final.feather'), 
                                      params_rate['oot_dat'], act_term_prob_cols, rate_map_cols)
    final_result = report_performance_metrics([df_final_train, df_final_oos, df_final_oot], ['train', 'oos', 'oot'], terms, act_term_prob_cols,
                                              pred_term_prob_cols, rate_map_cols, rate_cols, 'tier', 'tier_pre')
    final_result.to_csv(pjoin(output_dir, f"final_metrics_{params_rate['exp_name']}.csv"))

score_two_model('/home/ec2-user/SageMaker/round_2/experiments/13_2020-01-14_15-32-03', 
                '/home/ec2-user/SageMaker/round_2/experiments/7g_2019-11-26_16-29-29',
                '/home/ec2-user/SageMaker/round_2/experiments/13rate_7g_term')

score_two_model('/home/ec2-user/SageMaker/round_2/experiments/12_2020-01-14_14-29-17', 
                '/home/ec2-user/SageMaker/round_2/experiments/7g_2019-11-26_16-29-29',
                '/home/ec2-user/SageMaker/round_2/experiments/12rate_7g_term')

score_two_model('/home/ec2-user/SageMaker/round_2/experiments/11_2019-12-30_17-14-32', 
                '/home/ec2-user/SageMaker/round_2/experiments/7g_2019-11-26_16-29-29',
                '/home/ec2-user/SageMaker/round_2/experiments/11rate_7g_term')
  
score_two_model('/home/ec2-user/SageMaker/round_2/experiments/7c_2019-11-22_19-48-31', 
                '/home/ec2-user/SageMaker/round_2/experiments/10_2019-12-04_19-34-26',
                '/home/ec2-user/SageMaker/round_2/experiments/7c_rate_10_term')
  
score_two_model('/home/ec2-user/SageMaker/round_2/experiments/7e_2019-11-22_19-51-30', 
                '/home/ec2-user/SageMaker/round_2/experiments/7c_2019-11-22_19-48-31',
                '/home/ec2-user/SageMaker/round_2/experiments/7e_rate_7c_term')

score_two_model('/home/ec2-user/SageMaker/round_2/experiments/7c_2019-11-22_19-48-31',
                '/home/ec2-user/SageMaker/round_2/experiments/7f_2019-11-25_12-53-50', 
                '/home/ec2-user/SageMaker/round_2/experiments/7c_rate_7f_term')

score_two_model('/home/ec2-user/SageMaker/round_2/experiments/7c_2019-11-22_19-48-31',
                '/home/ec2-user/SageMaker/round_2/experiments/14_2020-02-12_13-37-05', 
                '/home/ec2-user/SageMaker/round_2/experiments/7c_rate_14_term')

score_two_model('/home/ec2-user/SageMaker/round_2/experiments/7c_2019-11-22_19-48-31',
                '/home/ec2-user/SageMaker/round_2/experiments/15_2020-02-12_19-35-18', 
                '/home/ec2-user/SageMaker/round_2/experiments/7c_rate_15_term')