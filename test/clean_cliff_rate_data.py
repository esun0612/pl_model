#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 18 12:44:28 2019

@author: esun
"""
import pandas as pd
df = pd.read_csv('/Users/esun/Desktop/cliff_rate.csv')
df['Tier'] = df['Tier'].str[-1]
df = pd.pivot(df, index='Tier', columns='Term', values='Rate')

df = pd.read_csv('/Users/esun/Desktop/cliff_overlay.csv')
df = df[df['llpa_type'].str.contains('PL Term By Fico')]
df['term'] = df['term'].str[0]
map_dict = {'FICO_780_799': '[780.0, 800.0)', 'FICO_OVER_OR_EQUAL_800': '[800.0, inf)', 
            'FICO_760_779': '[760.0, 780.0)', 'FICO_740_759': '[740.0, 760.0)', 
            'FICO_720_739': '[720.0, 740.0)', 'FICO_680_699': '[680.0, 700.0)', 
            'FICO_700_719': '[700.0, 720.0)'}
df['fico bucket'] = df['fico bucket'].map(map_dict)
map_dict_loan = {'PL Term By Fico 5-20k': '[5000, 20000]', 'PL Term By Fico 20-50k': '(20000, 50000]',
                   'PL Term By Fico 50-100k': '(50000, 100000]'}
df['llpa_type'] = df['llpa_type'].map(map_dict_loan)
df = pd.pivot_table(df, index=['llpa_type', 'fico bucket'], columns='term', values='amount')
df.sort_values(by=['llpa_type', 'fico bucket'], ascending=[True, False])