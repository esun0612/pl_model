#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 30 09:47:43 2019

@author: esun
"""
import pandas as pd
import time
import sys
sys.path.insert(0, '/Users/esun/Documents/erika_git/erika_utils')
import util_data
from util_data import query_snowflake, query_sofidw

start_date = '2018-01-01'
end_date = '2019-09-30'
product = f"'PL'" #'REFI' for SLR, 'PL' for personal loan
date_range = f"af.date_start>='{start_date}' and af.date_start<='{end_date}'"

def query_snowflake(qry):
    """Return the pandas DataFrame from input SQL qry with connection to snowflake
    """
    start = time.time()
    sc = snowflake.connector.connect(user='sdm_ro', password='none', port=1444, host='localhost', account='sofi', protocol='http')
    df = pd.read_sql(qry, sc)
    sc.close()
    df.columns = df.columns.str.lower()
    end = time.time()
    print(f'snowflake in {end - start}[s]')
    return df

def query_sofidw(qry):
    """Return the pandas DataFrame from input SQL qry with connection to sofidw
    """
    start = time.time()
    my_db = 'postgresql://localhost:15501/dw-prod-sofidw-ro'
    engine = create_engine(my_db, echo=True)
    df = pd.read_sql_query(qry, con=engine)
    end = time.time()
    print(f'sofidw in {end - start}[s]')
    return df
  
#Pull data for stu attributes
PL_qry_stu_4 =  f"""
SELECT
af.id,
af.date_start,
acaf.stu0416, 
acaf.stu2550, 
acaf.stu6200, 
acaf.stu8151
FROM dwmart.applications_file af
JOIN dwmart.application_credit_attributes_file acaf ON af.dw_application_id = acaf.dw_application_id
WHERE af.application_type = {product} AND {date_range}
AND (af.interest_rate_type = 'FIXED' OR af.interest_rate_type IS NULL) \
AND af.current_decision = 'ACCEPT'
"""
df_query_sf = query_snowflake(PL_qry_stu_4)
df_query_dw = query_sofidw(PL_qry_stu_4)

snowflake in 6.178697109222412[s]
sofidw in 22.039873123168945[s]

df_query_sf['ind'] = 'sf'
df_query_dw['ind'] = 'dw'
df = pd.merge(df_query_sf, df_query_dw, how='outer', on='id')
print(df_query_sf.shape[0], df_query_dw.shape[0], df.shape[0])
636151 714759 714759

df['date_start_y'] = pd.to_datetime(df['date_start_y'])
min_start_date = df['date_start_y'].min()
df['month'] = df['date_start_y'].dt.year * 12 + df['date_start_y'].dt.month - (min_start_date.year * 12 + min_start_date.month)

df[df['ind_x'].isnull()]['month'].value_counts()
df2 = pd.merge(df_query_sf, df_query_dw, on='id')
for col in ['stu0416', 'stu2550', 'stu6200', 'stu8151']:
    print(df[(df[col + '_x'] != df[col + '_y']) & (df[])].shape[0])



