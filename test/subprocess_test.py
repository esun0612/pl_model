#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 26 18:50:32 2019

@author: esun
"""
import subprocess
from subprocess import Popen, PIPE

cmd_1 = ['/home/ec2-user/anaconda3/envs/python3/bin/python',
        '/home/ec2-user/SageMaker/erika_git/pl_model/header_to_model.py',
        '/home/ec2-user/SageMaker/round_2/params/params_rate_8.json',
        '/home/ec2-user/SageMaker/round_2/params/params_term_8.json',
        '--shut_train_term', '--shut_finantial_metrics'] 
cmd_2 = ['/home/ec2-user/anaconda3/envs/python3/bin/python',
        '/home/ec2-user/SageMaker/erika_git/pl_model/header_to_model.py',
        '/home/ec2-user/SageMaker/round_2/params/params_rate_7h.json',
        '/home/ec2-user/SageMaker/round_2/params/params_term_7h.json',
        '--shut_train_term', '--shut_finantial_metrics']
cmds_list = [cmd_1, cmd_2]
log_1 = open('/home/ec2-user/SageMaker/round_2/experiments/test/exp_8_stdout.txt', 'w')
log_2 = open('/home/ec2-user/SageMaker/round_2/experiments/test/exp_7h_stdout.txt', 'w')
log_list = [log_1, log_2]
procs_list = [Popen(cmd, stdout=log) for cmd, log in zip(cmds_list, log_list)]

for proc in procs_list:
	proc.wait()

log_1.close()
log_2.close()
    
subprocess.run(['/home/ec2-user/anaconda3/envs/python3/bin/python',
                '/home/ec2-user/SageMaker/erika_git/pl_model/header_to_model.py',
                '/home/ec2-user/SageMaker/round_2/params/params_rate_8.json',
                '/home/ec2-user/SageMaker/round_2/params/params_term_8.json',
                '--shut_train_term', '--shut_finantial_metrics'])