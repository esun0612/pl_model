#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  2 10:23:32 2019

@author: esun
"""

import argparse
import os
from os.path import join as pjoin, splitext
import glob
import time
import pandas as pd
from shutil import copy
import sys
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/erika_utils')
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/pl_model')
from util_model import train_lightgbm, load_json, read_prepare_data, make_output_dir, \
score_rate_model, read_params, score_model, score_term_model
from binary_term_model import train_term_model_seperate, merge_score_multilabel
from util_analysis import cust_format_time
from metrics import gini, accuracy_binary, accuracy_multiclass, performance_metrics, accuracy_index
from util_data import eread, esave, add_postfix_colname

train_term_model_seperate(header_file, params_rate, params_term, model_out_dir, target_col_root_nm='signed_ind', use_monotone=True)

model_out_dir = '/home/ec2-user/SageMaker/round_2/experiments/9_2019-12-02_18-16-58'
params_rate = load_json('/home/ec2-user/SageMaker/round_2/params/params_rate_9.json')
terms = params_rate['terms']
rate_cols = ['{0}_{1}'.format(params_rate['term_col_prefix'], term) for term in terms]
rate_orig_cols = [col + '_orig' for col in rate_cols]
target_term_col = 'initial_term'
target_term_prob_cols = [f'{target_term_col}_{term}' for term in terms]

for data_nm, score_nm in zip(['dev', 'oos', 'oot'], ['train', 'oos', 'oot']):
    print(pjoin(model_out_dir, f'pred_{score_nm}_term_final.feather'))
    merge_score_multilabel(model_out_dir, pjoin(model_out_dir, f'pred_{score_nm}_term_final.feather'), 
                           params_rate[f'{data_nm}_dat'], data_nm, terms,
                           impute_rate_cols=rate_cols, orig_rate_cols=rate_orig_cols, target_term_prob_cols=target_term_prob_cols)
    
term_result_list = [eval_model_perform_term(df, pred_term_prob_cols, target_term_prob_cols) 
                            for df in [y_pred_train_all, y_pred_oos_all, y_pred_oot_all]]
df_result_term = pd.DataFrame(term_result_list)   
df_result_term.columns = ['avg_gini', 'acc', 'auc', 'acc_rate', 'acc_rate_weighted']
df_result_term.index = ['train', 'oos', 'oot']
df_result_term.to_csv(pjoin(model_out_dir, f"metrics_term_{params_rate['exp_name']}.csv"))  

y_pred_alls = []
for data_nm, score_nm in zip(['dev', 'oos', 'oot'], ['train', 'oos', 'oot']):
    y_pred_alls.append(merge_score_multilabel(model_out_dir, pjoin(model_out_dir, f'pred_{score_nm}_term_final.feather'), 
                       params_rate[f'{data_nm}_dat'], data_nm, terms,
                       impute_rate_cols=rate_cols, orig_rate_cols=rate_orig_cols, target_term_prob_cols=target_term_prob_cols))

#Generate metrics for rate model
print("Term model start evaluation")
term_result_list = [eval_model_perform_term(df, pred_term_prob_cols, target_term_prob_cols) 
                    for df in y_pred_alls]
df_result_term = pd.DataFrame(term_result_list)   
df_result_term.columns = ['avg_gini', 'acc', 'auc', 'acc_rate', 'acc_rate_weighted']
df_result_term.index = ['train', 'oos', 'oot']
df_result_term.to_csv(pjoin(model_out_dir, f"metrics_term_{params_rate['exp_name']}.csv"))  

model_out_dir = '/home/ec2-user/SageMaker/round_2/experiments/9_2019-12-04_17-51-08'
df = eread(pjoin(model_out_dir, 'pred_train_term_final.feather'))
df = eread('/home/ec2-user/SageMaker/round_2/experiments/7g_2019-11-26_16-29-29/pred_oot_term_final.feather')
for term in terms:
    print(gini(df[f'initial_term_{term}'], df[f'min_rate_{term}_prob']))
  
0.8819981735821303
0.6589659022566132
0.4980829075237292
0.5756824735696902
0.7980976390758738
0.8312821236836541

#Check why the score is random, check term 2 
#?Score and merging is wrong -> guess

#Correct
0.6273258870150511
0.42537512356250784
0.3778044072574227
0.4944734298052871
0.8087553288848524
0.8411669848498915

df_orig = eread(params_term['dev_dat'])
df = pd.merge(df_orig, df, on='id')
for term in terms:
    print(gini(df[f'signed_ind_{term}'], df[f'min_rate_{term}_prob']))
    
gini_all = []
for pred_term_prob_col, target_term_prob_col in zip(pred_term_prob_cols, target_term_prob_cols):
    gini_all.append(gini(df[target_term_prob_col], df[pred_term_prob_col]))
    

#Find error on 12/04
params_term = load_json('/home/ec2-user/SageMaker/round_2/params/params_term_9.json') 
header_file = params_term['header_file']
header = Header(header_file)
free_cash_flow = False
for col in header.num_cols:
    if 'free_cash_flow_post' in col:
        free_cash_flow = True
        break
terms = params_term['terms']
fit_params_term, classifier_params_term = read_params(params_term)
X_train, ys_train, cat_cols = read_data_from_header_multilabel(header_file, params_term['dev_dat'], terms, return_cat_cols=True)
X_oos, ys_oos = read_data_from_header_multilabel(header_file, params_term['oos_dat'], terms)

idx = 0
y_train = ys_train[0]
y_oos = ys_oos[0]
term = terms[0]

rate_col_nm = '{0}_{1}'.format(params_term['term_col_prefix'], term)
rate_cols = ['{0}_{1}'.format(params_term['term_col_prefix'], term) for term in terms]
if free_cash_flow:
    fcfp_col_nm = f'free_cash_flow_post_{term}'
    fcfp_cols = [f'free_cash_flow_post_{term}' for term in terms]
    fcfp_cols.remove(fcfp_col_nm)
rate_diff_cols = [f'min_rate_diff_{term_plus}_{term}' for term, term_plus in zip(terms[:-1], terms[1:])]
X_train_iter = X_train.copy()
X_oos_iter = X_oos.copy()
rate_cols.remove(rate_col_nm)

if idx == 0:
    monotone_decreasing_cols = [rate_col_nm]
    monotone_increasing_cols = [rate_diff_cols[idx]]
    del(rate_diff_cols[idx])
elif idx == len(terms)-1:
    monotone_decreasing_cols = [rate_col_nm, rate_diff_cols[idx-1]]
    monotone_increasing_cols = []
    del(rate_diff_cols[idx-1])
else:
    monotone_decreasing_cols = [rate_col_nm, rate_diff_cols[idx-1]]
    monotone_increasing_cols = [rate_diff_cols[idx]]
    del(rate_diff_cols[idx])
    del(rate_diff_cols[idx-1])

drop_cols = rate_cols + rate_diff_cols
if free_cash_flow:
    monotone_increasing_cols.append(fcfp_col_nm)
    drop_cols.extend(fcfp_cols)

X_train_iter.drop(drop_cols, axis=1, inplace=True)
X_oos_iter.drop(drop_cols, axis=1, inplace=True)
model_out_dir = '/home/ec2-user/SageMaker/round_2/experiments/test'
write_header(X_train_iter, y_train, cat_cols, pjoin(model_out_dir, f'header_term_{term}.csv'))
use_monotone=cast_bool_from_json(params_term, 'use_monotone', True)
if not use_monotone:
    monotone_decreasing_cols = []
    monotone_increasing_cols = []
model = train_lightgbm(fit_params_term, classifier_params_term, X_train_iter, y_train, X_oos_iter, y_oos, 
                       model_out_dir, 
                       model_name=f'term_{term}', 
                       monotone_decreasing_cols=monotone_decreasing_cols,
                       monotone_increasing_cols=monotone_increasing_cols)

id_col = 'id'
target_col = 'signed_ind_2'
y_pred = model.predict_proba(X_train_iter)[:,1]
df = pd.read_csv(params_term['dev_dat'], usecols=[id_col, target_col])
df['rate_pred'] = y_pred
esave(df, 'rate_pred_train_2.csv')    

term = 2
y_1 = score_model(model_file=pjoin('/home/ec2-user/SageMaker/round_2/experiments/9_2019-12-02_18-16-58', f'model_term_{term}.txt'), 
            output_file=pjoin(model_out_dir, f'pred_dev_2.csv'), 
            data_file=params_term[f'dev_dat'], 
            header_file=pjoin(model_out_dir, f'header_term_{term}.csv'))
df['rate_pred_1'] = y_1
from util_analysis import compare_dat_3, compare_dat
stat, fig = compare_dat_3(df, 'rate_pred', 'rate_pred_1')
print(gini(df[target_col], df['rate_pred']), gini(df[target_col], df['rate_pred_1']))

X, y = read_prepare_data(pjoin(model_out_dir, f'header_term_{term}.csv'), params_term[f'dev_dat'])
X.equals(X_train_iter)
c = compare_dat(X, X_train_iter, by_index=True)
mdl = lgb.Booster(model_file=pjoin('/home/ec2-user/SageMaker/round_2/experiments/9_2019-12-02_18-16-58', f'model_term_{term}.txt'))
y_pred_2 = pd.DataFrame(mdl.predict(X_train_iter))
y_pred_3 = pd.DataFrame(mdl.predict(X))