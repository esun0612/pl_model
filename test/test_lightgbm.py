#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 10 11:41:37 2019

@author: esun
"""
from util_model import read_prepare_data
#Test basic features
params_dir = '/home/ec2-user/SageMaker/params'
params_rate = load_json(pjoin(params_dir, 'params_rate_6.json')) 
header_file_rate = params_rate['header_file']
fit_params, classifier_params = read_params(params_rate)
X_train, y_train = read_prepare_data(header_file_rate, params_rate['dev_dat'], fillna_value=-99999)
X_oos, y_oos = read_prepare_data(header_file_rate, params_rate['oos_dat'], fillna_value=-99999)

fit_params['eval_set'] = [(X_oos, y_oos), (X_train, y_train)]
model_name = '_test'
output_folder = '/home/ec2-user/SageMaker/experiment/test'
classifier_params['output_model'] = pjoin(output_folder, f'model_file{model_name}.txt')
rate_cols = ['{0}_{1}'.format(params_rate['term_col_prefix'], term) for term in terms]
monotone_decreasing_cols=rate_cols

if monotone_decreasing_cols is not None:
    x_cols = list(X_train.columns)
    constrs = [0] * len(x_cols)
    for rate_col in monotone_decreasing_cols:
        rate_col_ind = x_cols.index(rate_col)
        constrs[rate_col_ind] = -1
    classifier_params['monotone_constraints'] = constrs 

clf = lgb.LGBMClassifier(**classifier_params)
clf.fit(X_train, y_train, **fit_params)
lgb.plot_metric(clf, metric=fit_params['eval_metric'])
clf.booster_.feature_importance()
fi_df = pd.DataFrame({'Variable': clf.booster_.feature_name(),
                      'Absolute_Importance': clf.booster_.feature_importance()})
fi_df['Relative_Importance'] = fi_df['Absolute_Importance'] / fi_df['Absolute_Importance'].sum()
fi_df.sort_values('Absolute_Importance', ascending=False, inplace=True)
fi_df['Relative_Importance'] = fi_df['Relative_Importance'].apply(lambda x: '{:.1%}'.format(x))

def get_feature_importance(booster):
    """Read feature importance to pandas DataFrame based on booster object (LightGBM Python API)
    """
    fi_df = pd.DataFrame({'Variable': booster.feature_name(),
                          'Absolute_Importance': booster.feature_importance()})
    fi_df['Relative_Importance'] = fi_df['Absolute_Importance'] / fi_df['Absolute_Importance'].sum()
    fi_df['Relative_Importance'] = fi_df['Relative_Importance'].apply(lambda x: '{:.1%}'.format(x))
    fi_df.sort_values('Absolute_Importance', ascending=False, inplace=True)
    return fi_df

clf.predict(X_oos[:5])
clf.predict_proba(X_oos[:5])

#Try term model
params_term = params_rate = load_json(pjoin(params_dir, 'params_term_6.json')) 
header_file_term = params_term['header_file']
fit_params_term, classifier_params_term = read_params(params_term)
copy(header_file_term, model_out_dir)
copy(args.params_term, model_out_dir)

#Read data
print("Reading data", params_term['dev_dat'],  params_term['oos_dat'], params_term['oot_dat'])
X_train_term, y_train_term = read_prepare_data(header_file_term, params_term['dev_dat'])
X_oos_term, y_oos_term = read_prepare_data(header_file_term, params_term['oos_dat'])
print("Finishing reading and preparing: ", X_train_term.shape, y_train_term.shape, X_oos_term.shape, y_oos_term.shape)

#Train term model
print("Term model start training")
time_3 = time.time()
model_out_dir = '/home/ec2-user/SageMaker/experiment/test'
from util_model import train_lightgbm
model_term = train_lightgbm(fit_params_term, classifier_params_term, X_train_term, y_train_term.values, 
                            X_oos_term, y_oos_term.values, 
                            model_out_dir, model_name='term')
[1068]  dev's multi_logloss: 0.982747   oos's multi_logloss: 1.06065
{'boosting_type': 'gbdt',
 'class_weight': None,
 'colsample_bytree': 1.0,
 'importance_type': 'split',
 'learning_rate': 0.05,
 'max_depth': 5,
 'min_child_samples': 300,
 'min_child_weight': 0.001,
 'min_split_gain': 0.0,
 'n_estimators': 3000,
 'n_jobs': 4,
 'num_leaves': 32,
 'objective': None,
 'random_state': None,
 'reg_alpha': 0.0,
 'reg_lambda': 0.0,
 'silent': True,
 'subsample': 1.0,
 'subsample_for_bin': 200000,
 'subsample_freq': 0}

#fillna, no missing value here, test later
X_train_term, y_train_term = read_prepare_data(header_file_term, params_term['dev_dat'])
X_oos_term, y_oos_term = read_prepare_data(header_file_term, params_term['oos_dat'])

#Categorical variable encoding
map_dict = {index: i for i, index in enumerate(X_train_term['pl_funds_use'].value_counts().index)}
X_train_term['pl_funds_use'] = X_train_term['pl_funds_use'].map(map_dict)
X_oos_term['pl_funds_use'] = X_oos_term['pl_funds_use'].map(map_dict)
fit_params_term['eval_set'] = [(X_oos_term, y_oos_term), 
                               (X_train_term, y_train_term)]
fit_params_term['categorical_feature'] = ['pl_funds_use', 'attr_affiliate_referrer_new', 'consolidated_channel']
clf = lgb.LGBMClassifier(**classifier_params_term)
clf.fit(X_train_term, y_train_term, **fit_params_term)
