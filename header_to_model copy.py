#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  7 10:47:33 2019

@author: esun
"""
import argparse
import os
from os.path import join as pjoin, splitext
import numpy as np
import glob
import time
import pandas as pd
from shutil import copy
import sys
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/erika_utils')
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/pl_model')
from util_model import train_lightgbm, load_json, read_prepare_data, make_output_dir, \
score_rate_model, read_params, score_model, score_term_model, cast_bool_from_json, merge_rate_model
from binary_term_model import train_term_model_seperate, merge_score_multilabel
from util_analysis import cust_format_time
from metrics import gini, accuracy_binary, accuracy_multiclass, performance_metrics, accuracy_index
from util_data import eread, esave, add_postfix_colname

def eval_model_perform_rate(df, pred_col='rate_pred', target_col='signed_ind'):
    gini_result = gini(df[target_col], df[pred_col])
    accuracy_result = accuracy_binary(df[target_col], df[pred_col])
    auc = (gini_result + 1) / 2
    return [gini_result, accuracy_result, auc]

def eval_model_perform_term(df, pred_term_prob_cols, target_term_prob_cols, target_term_col='initial_term', 
                            pred_rate_col='avg_rate', act_rate_col='interest_rate', loan_amount_col='requested_amount'):
    df = df.loc[df[target_term_col].notnull()]
    gini_all = []
    for pred_term_prob_col, target_term_prob_col in zip(pred_term_prob_cols, target_term_prob_cols):
        gini_all.append(gini(df[target_term_prob_col], df[pred_term_prob_col]))
    gini_result = sum(gini_all) / len(gini_all)
    auc = (gini_result + 1) / 2
    accuracy_result = accuracy_multiclass(df, target_term_prob_cols, pred_term_prob_cols)
    #accuracy on predicted rate
    acc_rate = accuracy_index(df[act_rate_col], df[pred_rate_col])
    acc_rate_weighted = accuracy_index(df[act_rate_col]*df[loan_amount_col], df[pred_rate_col]*df[loan_amount_col])
    return [gini_result, accuracy_result, auc, acc_rate, acc_rate_weighted]

def merge_all_pred(rate_pred_file, term_pred_file, data_file, act_term_prob_cols, rate_map_cols, output_file=None, 
                   id_col='id', data_cols=['requested_amount', 'gross_income', 'credit_score', 'tier', 'interest_rate', 'tier_pre'],
                   pred_term_prob_cols=[f'min_rate_{term}_prob_adj' for term in range(2,8)], pred_sign_col='rate_pred'):
    """Merge rate, term predictions together and output to output_file
    """
    df_rate_pred = eread(rate_pred_file)
    df_term_pred = eread(term_pred_file)
    read_data_cols = []
    for col in data_cols:
        if col not in df_term_pred.columns:
            read_data_cols.append(col)
    data_file_ext = splitext(data_file)[-1]
    data_cols = [id_col] + read_data_cols + act_term_prob_cols + rate_map_cols
    if data_file_ext == ".csv":
        df_data = pd.read_csv(data_file, usecols=data_cols)
    elif data_file_ext == ".feather":
        df_data = pd.read_feather(data_file)
        df_data = df_data[data_cols]
    df_final = pd.merge(df_rate_pred, df_term_pred, on=id_col)
    df_final = pd.merge(df_final, df_data, on=id_col)
    df_final['term_score_sum'] = df_final[pred_term_prob_cols].sum(axis=1)
    df_final[pred_sign_col] = np.where(df_final['term_score_sum'] != 0, df_final[pred_sign_col], 0)
    df_final.drop('term_score_sum', axis=1, inplace=True)
    if output_file is not None:
        esave(df_final, output_file)
    return df_final

def cal_profit(act_wac, sign_population, total_population):
    """
    Erika:
        afs_wac = .051, available for sale wac,
        afs_execution = .0207  execution is the revenue we make per $100 sale to the secondary market, function of afc_wac
        marketing_cost = 166
        duration = 2.6 in years
    """
    afs_wac = 0.051
    afs_execution = 0.0207  
    marketing_cost = 166
    duration = 2.6 
    execution_act = (act_wac-afs_wac) * duration + afs_execution
    total_profit_act = execution_act*sign_population - marketing_cost*total_population
    return total_profit_act
    
def report_performance_metrics(dfs, df_names, terms, act_term_prob_cols, pred_term_prob_cols, act_rate_cols, pred_rate_cols, 
                               act_tier_col, pred_tier_col,
                               pred_sign_col='rate_pred', pred_rate_col='avg_rate', pred_term_col='term_wgt_prob',
                               act_sign_col='signed_ind', act_rate_col='interest_rate', act_term_col='initial_term'):
    """Report the performance metrics for given dfs (list of dataframes) and corresponding data frame names in list (df_names) for prediction and actual
    """
    final_result = []
    for df in dfs:
        final_result.append(performance_metrics(df, act_sign_col, act_rate_col, act_term_col, act_term_prob_cols, act_rate_cols, tier_col=act_tier_col, loan_amt_col='requested_amount'))
        final_result.append(performance_metrics(df, pred_sign_col, pred_rate_col, pred_term_col, pred_term_prob_cols, pred_rate_cols, tier_col=pred_tier_col, loan_amt_col='requested_amount'))
    df_result = pd.DataFrame(final_result)   
    cols = ['population', 'total_signed', 'pct_signed', 'conv_dollar_basis', 'wac', 'wam', 'avg_loan_amt', 'w_avg_fico', 'w_avg_income'] \
            + ['term_dist_{}'.format(term) for term in terms] + ['wac_term_{}'.format(term) for term in terms] \
            + ['wac_tier_{}'.format(tier) for tier in range(1,8)] + ['tier_dist_{}'.format(tier) for tier in range(1,8)]
    df_result.columns = cols
    df_result.index = [df_name + '_' + seg for df_name in df_names for seg in ['act', 'pred']] 
    df_result['profit'] = cal_profit(df_result['wac'], df_result['total_signed'], df_result['population'])
    cols.insert(9, 'profit')
    df_result = df_result[cols]
    df_result = df_result.transpose()    
    for name in df_names:
        df_result[f'{name}_diff'] = df_result[f'{name}_pred'] - df_result[f'{name}_act']
        df_result[f'{name}_diff_ratio'] =  df_result[f'{name}_diff'] / df_result[f'{name}_act']
    return df_result.transpose()

def collect_result_model(model_dir, terms):
    """Collect OOS and OOT ratio and absolute difference and model performance in one place
    """
    rate_perf_file = glob.glob(pjoin(model_dir, 'metrics_rate_*'))[0]
    term_perf_file = glob.glob(pjoin(model_dir, 'metrics_term_*'))[0]
    final_perf_file = glob.glob(pjoin(model_dir, 'final_metrics_*'))[0]
    rate_perf = pd.read_csv(pjoin(model_dir, rate_perf_file), index_col=0)
    rate_perf = add_postfix_colname(rate_perf, 'rate')
    term_perf = pd.read_csv(pjoin(model_dir, term_perf_file), index_col=0)
    term_perf = add_postfix_colname(term_perf, 'term')
    final_perf = pd.read_csv(pjoin(model_dir, final_perf_file), index_col=0)
    final_perf = final_perf.iloc[6:, 2:]
    final_perf.reset_index(inplace=True)
    final_perf['index_1'] = final_perf['index'].str.split('_').apply(lambda x: x[0])
    final_perf = pd.merge(term_perf, final_perf, left_index=True, right_on='index_1') 
    final_perf = pd.merge(rate_perf, final_perf, left_index=True, right_on='index_1')
    final_perf['term_dist_diff_sum'] = final_perf[[f'term_dist_{term}' for term in terms]].abs().sum(axis=1)
    final_perf['wac_term_diff_sum'] = final_perf[[f'wac_term_{term}' for term in terms]].abs().sum(axis=1)
    final_perf['wac_tier_diff_sum'] = final_perf[[f'wac_tier_{tier}' for tier in range(1,8)]].abs().sum(axis=1)
    final_perf['tier_dist_diff_sum'] = final_perf[[f'tier_dist_{tier}' for tier in range(1,8)]].abs().sum(axis=1)
    selected_cols = ['index'] + rate_perf.columns.tolist() + term_perf.columns.tolist() \
                    + ['pct_signed', 'conv_dollar_basis', 'wac', 'wam', 'avg_loan_amt', 'w_avg_fico', 'w_avg_income', 'profit'] \
                    + [f'term_dist_{term}' for term in terms] + ['term_dist_diff_sum'] \
                    + [f'wac_term_{term}' for term in terms] + ['wac_term_diff_sum'] \
                    + [f'wac_tier_{tier}' for tier in range(1,8)] + ['wac_tier_diff_sum'] \
                    + [f'tier_dist_{tier}' for tier in range(1,8)] + ['tier_dist_diff_sum'] 
    final_perf = final_perf[selected_cols]
    return final_perf
    
if __name__ == '__main__':
    start = time.time()
    parser = argparse.ArgumentParser()  
    #Difference per experment are header_file and exp_name in the current phase
    parser.add_argument("params_rate", type=str, help="Name of the parameters json file for rate model.")  
    parser.add_argument("params_term", type=str, help="Name of the parameters json file for term model.")  
    parser.add_argument("--shut_train_rate", action='store_true', help="Turn off rate training")
    parser.add_argument("--shut_term", action='store_true', help="Turn off term model")
    parser.add_argument("--shut_train_term", action='store_true', help="Turn off term training")
    parser.add_argument("--shut_finantial_metrics", action='store_true', help="Turn off financial metrics evaluation")
    parser.add_argument("--model_output_dir", type=str, default=None, help="Model output directory")
    args = parser.parse_args()
    
    params_rate = load_json(args.params_rate) 
    exp_name = params_rate['exp_name']
    terms = params_rate['terms']
    if args.model_output_dir is None:
        model_out_dir = make_output_dir(params_rate['out_dir'], params_rate['exp_name'])
    else:
        model_out_dir = args.model_output_dir
    if 'term_col_postfix' in params_rate:
        rate_cols = ['{0}_{1}_{2}'.format(params_rate['term_col_prefix'], term, params_rate['term_col_postfix']) for term in terms]
    else:
        rate_cols = ['{0}_{1}'.format(params_rate['term_col_prefix'], term) for term in terms]
    pred_term_col = 'term_wgt_prob'
    pred_term_prob_cols = [col + '_prob_adj' for col in rate_cols]
        
    #1. Train rate model 
    if not args.shut_train_rate:
        #Read rate paramsters
        print("Training rate model.")
        header_file_rate = params_rate['header_file']
        fit_params_rate, classifier_params_rate = read_params(params_rate)
        copy(header_file_rate, model_out_dir)
        copy(args.params_rate, model_out_dir)
                
        #Read data
        print("Reading data", params_rate['dev_dat'],  params_rate['oos_dat'], params_rate['oot_dat'])
        X_train_rate, y_train_rate = read_prepare_data(header_file_rate, params_rate['dev_dat'], fillna_value=None)
        X_oos_rate, y_oos_rate = read_prepare_data(header_file_rate, params_rate['oos_dat'], fillna_value=None)
        X_oot_rate, y_oot_rate = read_prepare_data(header_file_rate, params_rate['oot_dat'], fillna_value=None)
        print("Finishing reading and preparing: ", X_train_rate.shape, y_train_rate.shape, X_oos_rate.shape, y_oos_rate.shape, X_oot_rate.shape, y_oot_rate.shape)
        
        #Train rate model
        print("Rate model start training")
        time_0 = time.time()
        model_rate = train_lightgbm(fit_params_rate, classifier_params_rate, X_train_rate, y_train_rate, X_oos_rate, y_oos_rate, 
                                    model_out_dir, model_name=f'rate_{exp_name}', monotone_decreasing_cols=rate_cols)
        time_1 = time.time()
        print("Rate model finish training in {}".format(cust_format_time(time_1 - time_0)))
         
        #Score rate model
        print("Rate model start scoring")
        df_train_rate = score_rate_model(params_rate['dev_dat'], X_train_rate, model_rate, pjoin(model_out_dir, 'pred_train_rate.feather'))
        df_oos_rate = score_rate_model(params_rate['oos_dat'], X_oos_rate, model_rate, pjoin(model_out_dir, 'pred_oos_rate.feather'))
        df_oot_rate = score_rate_model(params_rate['oot_dat'], X_oot_rate, model_rate, pjoin(model_out_dir, 'pred_oot_rate.feather'))
        time_2 = time.time()
        print("Rate model finish scoring in {}".format(cust_format_time(time_2 - time_1)))
        
        #Generate metrics for rate model
        print("Rate model start evaluation")
        rate_result_list = [eval_model_perform_rate(df) for df in [df_train_rate, df_oos_rate, df_oot_rate]]
        df_result_rate = pd.DataFrame(rate_result_list)   
        df_result_rate.columns = ['gini', 'acc', 'auc']
        df_result_rate.index = ['train', 'oos', 'oot']
        df_result_rate.to_csv(pjoin(model_out_dir, f"metrics_rate_{params_rate['exp_name']}.csv"))
        print("Rate model finish evaluation")
        
    #2. Train term model
    #Read paramsters
    if not args.shut_term:
        print("Training term model.")
        params_term = load_json(args.params_term)  
        rate_orig_cols = [col + '_orig' for col in rate_cols]
        target_term_col = 'initial_term'
        target_term_prob_cols = [f'{target_term_col}_{term}' for term in terms]
        header_file_term = params_term['header_file']
        copy(header_file_term, model_out_dir)
        copy(args.params_term, model_out_dir)
        if params_term.get('seperate_model', 'False') == 'False':
            fit_params_term, classifier_params_term = read_params(params_term) 
            #Read data
            print("Reading data", params_term['dev_dat'],  params_term['oos_dat'], params_term['oot_dat'])
            X_train_term, y_train_term = read_prepare_data(header_file_term, params_term['dev_dat'])
            X_oos_term, y_oos_term = read_prepare_data(header_file_term, params_term['oos_dat'])
            print("Finishing reading and preparing: ", X_train_term.shape, y_train_term.shape, X_oos_term.shape, y_oos_term.shape)
            
            #Train term model
            print("Term model start training")
            time_3 = time.time()
            model_term = train_lightgbm(fit_params_term, classifier_params_term, X_train_term, y_train_term, X_oos_term, y_oos_term, 
                                 model_out_dir, model_name=f'term_{exp_name}')
            time_4 = time.time()
            print("Term model finish training in {}".format(cust_format_time(time_4 - time_3)))
            
            #Score model
            print("Term model start scoring")
            model_file_term = pjoin(model_out_dir, f'model_term_{exp_name}.txt')
            #Raw prediction
            y_pred_train = score_model(model_file=model_file_term, output_file=pjoin(model_out_dir, 'pred_train_term.csv'), 
                                       data_file=params_rate['dev_dat'], header_file=header_file_term)
            y_pred_oos = score_model(model_file=model_file_term, output_file=pjoin(model_out_dir, 'pred_oos_term.csv'), 
                                     data_file=params_rate['oos_dat'], header_file=header_file_term)
            y_pred_oot = score_model(model_file=model_file_term, output_file=pjoin(model_out_dir, 'pred_oot_term.csv'), 
                                     data_file=params_rate['oot_dat'], header_file=header_file_term)
            
            #From raw prediction, calculate 3 columns from cal_term_rate function
            try:
                y_pred_train_all = score_term_model(data_file=params_rate['dev_dat'], raw_model_output_file=pjoin(model_out_dir, 'pred_train_term.csv'), 
                                                    output_file=pjoin(model_out_dir, 'pred_train_term_final.feather'), 
                                                    impute_rate_cols=rate_cols, orig_rate_cols=rate_orig_cols, terms=terms, target_term_prob_cols=target_term_prob_cols)
            except ValueError:
                rate_cols = [f'min_rate_{term}' for term in terms]
                rate_orig_cols = [f'min_rate_{term}_orig' for term in terms]
                pred_term_prob_cols = [col + '_prob_adj' for col in rate_cols]
                y_pred_train_all = score_term_model(data_file=params_rate['dev_dat'], raw_model_output_file=pjoin(model_out_dir, 'pred_train_term.csv'), 
                                                    output_file=pjoin(model_out_dir, 'pred_train_term_final.feather'), 
                                                    impute_rate_cols=rate_cols, orig_rate_cols=rate_orig_cols, terms=terms, target_term_prob_cols=target_term_prob_cols)
            y_pred_oos_all = score_term_model(data_file=params_rate['oos_dat'], raw_model_output_file=pjoin(model_out_dir, 'pred_oos_term.csv'), 
                                                output_file=pjoin(model_out_dir, 'pred_oos_term_final.feather'), 
                                                impute_rate_cols=rate_cols, orig_rate_cols=rate_orig_cols, terms=terms, target_term_prob_cols=target_term_prob_cols)
            y_pred_oot_all = score_term_model(data_file=params_rate['oot_dat'], raw_model_output_file=pjoin(model_out_dir, 'pred_oot_term.csv'), 
                                                output_file=pjoin(model_out_dir, 'pred_oot_term_final.feather'), 
                                                impute_rate_cols=rate_cols, orig_rate_cols=rate_orig_cols, terms=terms, target_term_prob_cols=target_term_prob_cols)
            y_pred_alls = [y_pred_train_all, y_pred_oos_all, y_pred_oot_all]
            time_5 = time.time()
            print("Term model finish scoring in {}".format(cust_format_time(time_5 - time_4)))
        else:
            if not args.shut_train_term:
                print("Term model start training")
                time_3 = time.time()
                train_term_model_seperate(params_rate, params_term, model_out_dir, use_monotone=cast_bool_from_json(params_term, 'use_monotone', True))
                time_4 = time.time()  
                print("Term model finish training in {}".format(cust_format_time(time_4 - time_3)))
                
            print("Term model start scoring")
            for term in terms:
                for dat_time in ['dev', 'oos', 'oot']:
                    print(f"Term model for {dat_time} start scoring")
                    time_start = time.time()
                    score_model(model_file=pjoin(model_out_dir, f'model_term_{term}.txt'), output_file=pjoin(model_out_dir, f'pred_{dat_time}_{term}.csv'), 
                                data_file=params_rate[f'{dat_time}_dat'], header_file=pjoin(model_out_dir, f'header_term_{term}.csv'))
                    time_end = time.time()
                    print("Term model for {} finish scoring in {}".format(term, cust_format_time(time_end - time_start)))
            y_pred_alls = []
            for data_nm, score_nm in zip(['dev', 'oos', 'oot'], ['train', 'oos', 'oot']):
                y_pred_alls.append(merge_score_multilabel(model_out_dir, pjoin(model_out_dir, f'pred_{score_nm}_term_final.feather'), 
                                   params_rate[f'{data_nm}_dat'], data_nm, terms,
                                   impute_rate_cols=rate_cols, orig_rate_cols=rate_orig_cols, target_term_prob_cols=target_term_prob_cols))
            time_5 = time.time()
            print("Term model finish scoring in {}".format(cust_format_time(time_5 - time_4)))
            
        #Generate metrics for rate model
        print("Term model start evaluation")
        term_result_list = [eval_model_perform_term(df, pred_term_prob_cols, target_term_prob_cols) 
                            for df in y_pred_alls]
        df_result_term = pd.DataFrame(term_result_list)   
        df_result_term.columns = ['avg_gini', 'acc', 'auc', 'acc_rate', 'acc_rate_weighted']
        df_result_term.index = ['train', 'oos', 'oot']
        df_result_term.to_csv(pjoin(model_out_dir, f"metrics_term_{params_rate['exp_name']}.csv"))  
        print("Term model finish evaluation")
        
    
    #3. Generate overall financial metrics
    #Merge all data together 
    if not args.shut_finantial_metrics:
        print("Generating finicial metrics")
        act_term_prob_cols = ['signed_ind_{}'.format(term) for term in terms]  
        rate_map_cols = [f'rate_map_{term}' for term in terms]
        df_final_train = merge_all_pred(pjoin(model_out_dir, 'pred_train_rate.feather'), pjoin(model_out_dir, 'pred_train_term_final.feather'), 
                                        params_rate['dev_dat'], act_term_prob_cols, rate_map_cols, output_file=pjoin(model_out_dir, 'pred_train_final.feather'))
        
        df_final_oos = merge_all_pred(pjoin(model_out_dir, 'pred_oos_rate.feather'), pjoin(model_out_dir, 'pred_oos_term_final.feather'), 
                                      params_rate['oos_dat'], act_term_prob_cols, rate_map_cols, output_file=pjoin(model_out_dir, 'pred_oos_final.feather'))
        
        df_final_oot = merge_all_pred(pjoin(model_out_dir, 'pred_oot_rate.feather'), pjoin(model_out_dir, 'pred_oot_term_final.feather'), 
                                      params_rate['oot_dat'], act_term_prob_cols, rate_map_cols, output_file=pjoin(model_out_dir, 'pred_oot_final.feather'))
        print(df_final_train.shape, df_final_oos.shape, df_final_oot.shape) 
        
        try:
            final_result = report_performance_metrics([df_final_train, df_final_oos, df_final_oot], ['train', 'oos', 'oot'], terms, act_term_prob_cols,
                                                  pred_term_prob_cols, rate_map_cols, rate_cols, 'tier', 'tier_pre')
        except KeyError:
            rate_cols = [f'min_rate_{term}' for term in terms]
            rate_orig_cols = [f'min_rate_{term}_orig' for term in terms]
            pred_term_prob_cols = [col + '_prob_adj' for col in rate_cols]
            final_result = report_performance_metrics([df_final_train, df_final_oos, df_final_oot], ['train', 'oos', 'oot'], terms, act_term_prob_cols,
                                                  pred_term_prob_cols, rate_map_cols, rate_cols, 'tier', 'tier_pre')
        final_result.to_csv(pjoin(model_out_dir, f"final_metrics_{params_rate['exp_name']}.csv"))
        final_perf = collect_result_model(model_out_dir, terms)
        final_perf.to_csv(pjoin(model_out_dir, f"collect_diff_metrics_{params_rate['exp_name']}.csv"), index=None)
        print("Finished finicial metrics")
        
    end = time.time()
    print("Total training and scoring time is {}".format(cust_format_time(end - start)))