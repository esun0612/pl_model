-- After 2020-01-17
with core as (
SELECT
       poa.id AS partner_offer_attempt_id,
       poa.product_tracking_id,
       --poa.product_tracking_id as product_tracking_id
       poa.response_code,
       poa.credit_pull_code,
       poa.offer_package_type,
       poa.created_dt,
       poa.request,
       json_extract(poa.request, '$.leadInformation.loanInformation.purpose') AS purpose,
       te.tracking_id,
       case when te.event is not null then te.event else 'No_Followthrough' end as reaction,
       te3.target_id as attempt_target_id,
       poa2.uuid,
       poa2.name,
       pop.annual_income,
       pop.loan_amount,
       pop.email,
       pop.apr
FROM
(
  SELECT * FROM datalake_production_core.tracking_event_value
  WHERE attribute = 'partnerAttemptId' AND value != 'null') AS tev
  --WHERE attribute = 'productTrackingId' AND value != 'null') AS tev --09/20 - 01/16
RIGHT OUTER JOIN
(SELECT * FROM datalake_production_core.partner_offer_attempt where created_dt>= date '2020-01-17'
 and response_code in ('OFFERS','COUNTER') and credit_pull_code='SUCCESS') poa
 ON cast(tev.value as integer)  = cast(poa.id as integer)
 --ON cast(tev.value as integer)  = cast(poa.product_tracking_id as integer)
LEFT JOIN datalake_production_core.tracking_event te ON tev.tracking_event_id = te.id
left join
(
select
distinct te.tracking_id, te.target_id
from datalake_production_core.tracking_event te
join
 (select
  te.tracking_id
  from (select *
        from datalake_production_core.tracking_event_value
        where attribute='partnerAttemptId' AND value != 'null' ) tev
  left join datalake_production_core.tracking_event te
  on tev.tracking_event_id=te.id) te2
on te.tracking_id=te2.tracking_id
where target_type='APP') te3
on te.tracking_id=te3.tracking_id
left join datalake_production_core.partner_offer_account poa2 on poa.partner_offer_account_uuid = poa2.uuid
LEFT JOIN datalake_production_core.partner_offer_prepop pop ON pop.partner_offer_attempt_id = poa.id
where poa2.product='PL'
),
product as (
select
       ofr.*,
       coalesce(ofr.offer_creditScore1, ofr.offer_creditScore2, ofr.offer_creditScore3) as credit_score,
       op.product_name,
       op.asof_dt,
       up.app_id app_id,
       up.decision,
       up.requested_amt,
       cc.ch_ch_name,
       ud.sofi_score,
       ud.gross_income,
       ud.years_of_experience,
       ud.monthly_free_cash_flow,
       ud.monthly_real_cash_flow,
       ud.tax_burden_amt,
       ud.revolving_credit_amt,
       offer.tier,
       offer.rate_2 min_rate_2,
       offer.rate_3 min_rate_3,
       offer.rate_4 min_rate_4,
       offer.rate_5 min_rate_5,
       offer.rate_6 min_rate_6,
       offer.rate_7 min_rate_7
from (
  SELECT
       id as offer_requests_id,
       offer_package_id,
       target_id as ofr_target_id,
       target_type,
       party_id,
       TRY(CAST(json_extract(offer_request_payload, '$.creditPullInformations.0.creditPullData.ILN5020') AS INTEGER)) AS iln5020,
       TRY(CAST(json_extract(offer_request_payload, '$.creditPullInformations.0.creditPullData.ALX8220') AS INTEGER)) AS alx8220,
       TRY(CAST(json_extract(offer_request_payload, '$.creditPullInformations.0.creditPullData.ALL8020') AS INTEGER)) AS all8020,
       TRY(CAST(json_extract(offer_request_payload, '$.creditPullInformations.0.creditPullData.ILN5820') AS INTEGER)) AS iln5820,
       TRY(CAST(json_extract(offer_request_payload, '$.creditPullInformations.0.creditPullData.ALL0416') AS INTEGER)) AS all0416,
       TRY(CAST(json_extract(offer_request_payload, '$.creditPullInformations.0.creditPullData.REV0416') AS INTEGER)) AS rev0416,
       TRY(CAST(json_extract(offer_request_payload, '$.creditPullInformations.0.creditPullData.MTF5020') AS INTEGER)) AS mtf5020,
       TRY(CAST(json_extract(offer_request_payload, '$.creditPullInformations.0.creditPullData.MTA5020') AS INTEGER)) AS mta5020,
       TRY(CAST(json_extract(offer_request_payload, '$.creditPullInformations.0.creditPullData.MTA5830') AS INTEGER)) AS mta5830,
       TRY(CAST(json_extract(offer_request_payload, '$.creditPullInformations.0.creditPullData.MTJ5030') AS INTEGER)) AS mtj5030,
       TRY(CAST(json_extract(offer_request_payload, '$.creditPullInformations.0.creditPullData.MTJ5820') AS INTEGER)) AS mtj5820,
       TRY(CAST(json_extract(offer_request_payload, '$.creditPullInformations.0.creditPullData.ALL5820') AS INTEGER)) AS all5820,
       TRY(CAST(json_extract(offer_request_payload, '$.creditPullInformations.0.creditPullData.ALL8220') AS INTEGER)) AS all8220,
       TRY(CAST(json_extract(offer_request_payload, '$.creditPullInformations.0.creditPullData.ALL5020') AS INTEGER)) AS all5020,
       TRY(CAST(json_extract(offer_request_payload, '$.creditPullInformations.0.creditPullData.BCC7110') AS INTEGER)) AS bcc7110,
       TRY(CAST(json_extract(offer_request_payload, '$.creditPullInformations.0.creditPullData.BCC8322') AS INTEGER)) AS bcc8322,
       TRY(CAST(json_extract(offer_request_payload, '$.creditPullInformations.0.creditPullData.BCX3421') AS INTEGER)) AS bcx3421,
       TRY(CAST(json_extract(offer_request_payload, '$.creditPullInformations.0.creditPullData.BCX3422') AS INTEGER)) AS bcx3422,
       TRY(CAST(json_extract(offer_request_payload, '$.creditPullInformations.0.creditPullData.BCX5020') AS INTEGER)) AS bcx5020,
       TRY(CAST(json_extract(offer_request_payload, '$.creditPullInformations.0.creditPullData.BCX5320') AS INTEGER)) AS bcx5320,
       TRY(CAST(json_extract(offer_request_payload, '$.creditPullInformations.0.creditPullData.BCX7110') AS INTEGER)) AS bcx7110,
       TRY(CAST(json_extract(offer_request_payload, '$.creditPullInformations.0.creditPullData.REV5020') AS INTEGER)) AS rev5020,
       TRY(CAST(json_extract(offer_request_payload, '$.creditPullInformations.0.creditPullData.REV8320') AS INTEGER)) AS rev8320,
       TRY(CAST(json_extract(offer_request_payload, '$.creditPullInformations.0.creditPullData.ALL7517') AS INTEGER)) AS all7517,
       TRY(CAST(json_extract(offer_request_payload, '$.creditPullInformations.0.creditPullData.ILN0416') AS INTEGER)) AS iln0416,
       TRY(CAST(json_extract(offer_request_payload, '$.creditPullInformations.0.creditPullData.MTA0416') AS INTEGER)) AS mta0416,
       TRY(CAST(json_extract(offer_request_payload, '$.creditPullInformations.0.creditPullData.MTA1370') AS INTEGER)) AS mta1370,
       TRY(CAST(json_extract(offer_request_payload, '$.creditPullInformations.0.creditPullData.PIL0438') AS INTEGER)) AS pil0438,
       TRY(CAST(json_extract(offer_request_payload, '$.creditPullInformations.0.creditScores.EXPERIAN.score') AS INTEGER)) AS offer_creditScore1,
       TRY(CAST(json_extract(offer_request_payload, '$.creditPullInformations.1.creditScores.EXPERIAN.score') AS INTEGER)) AS offer_creditScore2,
       TRY(CAST(json_extract(offer_request_payload, '$.creditScores.EXPERIAN.score') AS INTEGER)) AS offer_creditScore3,
       created_dt as offer_request_created_dt
FROM datalake_production_products.offer_requests
where created_dt>=date '2020-01-17') ofr
join (select * from datalake_production_products.offer_package where product_name='PL') op on ofr.offer_package_id=op.id
join (select * from datalake_production_products.underwriting_packet where decision='ACCEPT') up ON op.uw_packet_id = up.id
join datalake_production_products.underwriting_data ud ON up.id = ud.uw_packet_id
left join datalake_production_products.champion_challenger cc on op.champion_challenger_id = cc.id
left join (
  select
       op.id as offer_package_id,
       up.requested_amt,
       min(regexp_replace(min_tier, '[^0-9]')) as tier,
       min(case when offer.term=2 then offer.min_rate end) as rate_2,
       min(case when offer.term=3 then offer.min_rate end) as rate_3,
       min(case when offer.term=4 then offer.min_rate end) as rate_4,
       min(case when offer.term=5 then offer.min_rate end) as rate_5,
       min(case when offer.term=6 then offer.min_rate end) as rate_6,
       min(case when offer.term=7 then offer.min_rate end) as rate_7
from datalake_production_products.offer_package op
join datalake_production_products.underwriting_packet up on op.uw_packet_id = up.id
left join datalake_production_products.offer on op.id=offer.offer_package_id and up.requested_amt<=offer.max_amount and up.requested_amt>=offer.min_amount
where datalake_production_products.offer.rate_type='FIXED' and offer.eligible='true' and offer.decision='ACCEPT'
and op.product_name='PL' and up.decision='ACCEPT'
group by 1,2
    ) offer on op.id = offer.offer_package_id
)
select
core.*,
product.*
from core
join product
on cast(core.partner_offer_attempt_id as integer)=cast(product.ofr_target_id as integer)
--on cast(core.product_tracking_id as integer)=cast(product.ofr_target_id as integer)

