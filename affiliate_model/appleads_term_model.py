#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 23 11:42:55 2020

@author: esun
"""

df = pd.read_csv(pjoin(dat_dir, 'pl_201901_2020_0310_with_rates.csv'))
df = df[df['initial_term'] != 20]
df['date_start'] = pd.to_datetime(df['date_start'])
df['min_all_rates'] = df[[f'min_rate_{term}' for term in range(2, 8)]].min(axis=1, skipna=True)
#registration_date: date_start - var
df['registration_date'] = pd.to_datetime(df['registration_date'])
df['registration_date'] = (df['date_start'] - df['registration_date']).dt.days
df['registration_date'] = df['registration_date'].fillna(0)       
df['registration_date'] = np.clip(df['registration_date'], 0, 1717) 
channel_list = ['www.sofi.com', 'Affiliate', 'SEM', 'Organic Search', 'MKT-DM', 'P2P']
df = cut_cat_var(df, 'consolidated_channel', channel_list) 
df['consolidated_channel_model'] = df['consolidated_channel']
df['consolidated_channel_model'] = np.where(df['consolidated_channel'].isin(['www.sofi.com', 'Organic Search']), 'combined_organic', df['consolidated_channel_model'])
df['consolidated_channel_model'] = np.where(df['consolidated_channel'].isin(['P2P', 'Rest', 'SEM']), 'Rest', df['consolidated_channel_model'])
df.to_csv(pjoin(dat_dir, 'data_regular.csv'), index=None)

df = df[df['initial_term'].notnull()]
train, oos = divide_train_oos(df)
train.to_csv(pjoin(dat_dir, 'train_regular.csv'), index=None)
oos.to_csv(pjoin(dat_dir, 'oos_regular.csv'), index=None)

def gen_term_data(in_data_file, out_data_file):
    df = pd.read_csv(in_data_file)
    for term in range(2, 8):
        df[f'initial_term_{term}'] = np.where((df['initial_term'] == term), 1, 0)
    df.to_csv(out_data_file, index=None)
gen_term_data(pjoin(dat_dir, 'train_regular.csv'), pjoin(dat_dir, 'train_regular.csv'))
gen_term_data(pjoin(dat_dir, 'oos_regular.csv'), pjoin(dat_dir, 'oos_regular.csv'))
    
#4. Term selection model
#Read term paramsters
params_term = load_json('../round_2/experiments/7c_2019-11-22_19-48-31/params_term_7c.json') 
header_file_term = '../appleads_model/model/header_appleads_term.csv'
fit_params_term, classifier_params_term = read_params(params_term)

#Read data
train_data_file = '../appleads_model/data/train_regular.csv'
oos_data_file = '../appleads_model/data/oos_regular.csv'
X_train_term, y_train_term = read_prepare_data(header_file_term, train_data_file)
X_oos_term, y_oos_term = read_prepare_data(header_file_term, oos_data_file)

#Train term model
model_output_dir = '../appleads_model/model/model_option_3_term_regular'
rate_cols = [f'min_rate_{term}' for term in range(2, 8)]
fit_params_term['early_stopping_rounds'] = 500

model_term = train_lightgbm(fit_params_term, classifier_params_term, X_train_term, y_train_term, X_oos_term, y_oos_term, 
                            model_output_dir, model_name=f'appleads_option3_term_regular')
model_file_term = '../appleads_model/model/model_option_3_term_regular/model_appleads_option3_term_regular.txt'

#Score term model
y_pred_oos = score_model(model_file=model_file_term, output_file=pjoin(model_output_dir, 'pred_oos_term.csv'), 
                         data_file=oos_data_file, header_file=header_file_term)
rate_orig_cols = [f'min_rate_{term}_orig' for term in range(2, 8)]
terms = range(2, 8)
target_term_col = 'initial_term'
target_term_prob_cols = [f'{target_term_col}_{term}' for term in terms]
pred_term_prob_cols = [col + '_prob_adj' for col in rate_cols]

y_pred_oos_all = score_term_model(data_file=oos_data_file, raw_model_output_file=pjoin(model_output_dir, 'pred_oos_term.csv'), 
                                output_file=pjoin(model_output_dir, 'pred_oos_term_final.csv'), 
                                impute_rate_cols=rate_cols, orig_rate_cols=rate_cols, terms=terms, 
                                target_term_prob_cols=target_term_prob_cols)

eval_model_perform_term(y_pred_oos_all, pred_term_prob_cols, target_term_prob_cols)
[0.6529670359407782,
 0.425238772155241,
 0.8264835179703891,
 0.9759204472983826,
 0.9737659936657799]