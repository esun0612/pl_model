#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 10 09:32:41 2020

@author: esun
"""
from os.path import join as pjoin
import numpy as np
import pandas as pd
import mdsutils
import sys
sys.path.append('../erika_git/erika_utils')
from util_data import add_signed_ind, clean_str

drop_cols = ['product_tracking_id',
            'response_code',
            'credit_pull_code',
            'offer_package_type',
            'request',
            'tracking_id',
            'reaction',
            'uuid',
            'email',
            'apr',
            'offer_requests_id',
            'offer_package_id',
            'ofr_target_id',
            'target_type',
            'party_id',
            'product_name',
            'decision',
            'sofi_score',
            'monthly_real_cash_flow']

dat_dir = '../appleads_model/data'
df = pd.read_csv(pjoin(dat_dir, 'appleads_after_0117.csv'))
df.drop_duplicates(subset=['partner_offer_attempt_id'], inplace=True)
df.drop(drop_cols, inplace=True, axis=1)
df.to_csv(pjoin(dat_dir, 'appleads_after_0117_drop.csv'), index=None)
df['created_dt'] = pd.to_datetime(df['created_dt'])
df['asof_dt'] = pd.to_datetime(df['asof_dt'])
df['date_diff'] = df['asof_dt'] - df['created_dt']
df['date_diff'].describe()
#Two dates are the same

#applications_file
df = pd.read_csv(pjoin(dat_dir, 'pl_201901_2020_0310.csv'))
df = df[df['date_start'] >= '2020-01-17']

df_al = pd.read_csv(pjoin(dat_dir, 'appleads_after_0117_drop.csv'))
df_al['created_dt'] = pd.to_datetime(df_al['created_dt'])
df_al = df_al[df_al['created_dt'] <= '2020-02-08']
df_al = df_al[df_al['attempt_target_id'].notnull()]

t = pd.merge(df, df_al, left_on='id', right_on='attempt_target_id')
t = add_signed_ind(t)

#Other channel conversion
#Query campaign_id
ath = mdsutils.AthenaClient(database='datalake_production_core')
df = ath.query_to_df("""select distinct te.target_id, t.campaign_id
                        from tracking_event as te
                        join tracking as t on te.tracking_id = t.id
                        where te.target_type = 'APP' and
                        te.created_dt >= date '2020-01-01'
                        """)                    
df.to_csv(pjoin(dat_dir, 'compaign_id.csv'), index=None)

#Other channel conversion
df = pd.read_csv(pjoin(dat_dir, 'pl_201901_2020_0310.csv'))
df = df[(df['date_start'] >= '2020-01-17') & (df['date_start'] <= '2020-02-08')]
df = add_signed_ind(df)

df_campaign = pd.read_csv(pjoin(dat_dir, 'compaign_id.csv'))
df_campaign_dedup = df_campaign.drop_duplicates('target_id')
df = pd.merge(df, df_campaign_dedup, how='left', left_on='id', right_on='target_id')
#Create API indicator by campaign_id
api_campaign_list = [1420, 1418, 3156,1420,1420,1420,2862,1420,2140,2293,1420,1420,1420,1420,1420,1420,1420,1414,1420,2260,1420,2210,1420,597]
df['api_ind'] = np.where(df['campaign_id'].isin(api_campaign_list), 1, 0)
df['attr_affiliate_referrer'] = clean_str(df['attr_affiliate_referrer'])
df['k_channel'] = np.where(df['consolidated_channel'] == 'MKT-DM', 'DM',
                  np.where((df['attr_affiliate_referrer'] == 'lending_tree') & (df['api_ind'] == 1), 'LT_API',
                  np.where((df['attr_affiliate_referrer'] == 'credit_karma') & (df['api_ind'] == 1), 'CK_API',
                  np.where((df['consolidated_channel'] == 'Affiliate') & (df['api_ind'] == 1), 'other_API',
                  np.where(df['consolidated_channel'] == 'Affiliate', 'other_non_API',
                  np.where(df['consolidated_channel'].isin(['www.sofi.com', 'Organic Search']), 'combined_organic', 'other')
                  )))))
df.groupby('k_channel').agg({'id': 'count', 'signed_ind': 'sum'}).sort_values('id', ascending=False)
