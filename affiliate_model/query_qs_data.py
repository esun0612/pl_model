#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 10 09:55:59 2020

@author: esun
"""

from os.path import join as pjoin
import numpy as np
import pandas as pd
import sys
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/erika_utils')
from util_data import query_snowflake
from util_data import flat_term_rate_dat

#1. Query raw data 
start_date = '2019-01-01'
product = f"'PL'"
date_range = f"af.date_start>='{start_date}'"

df = query_snowflake(PL_qry)
df.to_csv('/Users/esun/Documents/Experiments/PL_model/data/appleads/pl_201901_2020_0310.csv', index=None)

PL_qry = f"""
SELECT
af.id,
af.applicant_id, 
af.date_signed,
af.date_doc_upload,
af.date_start,
af.initial_term,
af.interest_rate,
af.requested_amount,
af.gross_income,
af.app_created_via_mobile,
af.g_program,
af.g_grad_year,
af.credit_score,
af.employer_name,
af.years_of_experience,
af.pl_funds_use,
af.tax_burden_amount,
af.consolidated_channel,
af.attr_affiliate_referrer,
af.campaign_welcome_bonus,
af.housing_status,
acaf.pil0438,
af.registration_date,
af.revolving_credit_amount,
af.tier,
af.ug_ctgry,
af.ug_grad_year,
af.ug_program,
af.member_indicator,
acaf.iln5020,
acaf.alx8220,
acaf.all8020,
acaf.iln5820,
acaf.all0416,
acaf.REV0416,
acaf.mtf5020,
acaf.mta5020,
acaf.mta5830,
acaf.mtj5030,
acaf.mtj5820,
acaf.all5820,
acaf.all7516,
acaf.all8220,
acaf.all5020,
acaf.bcc7110,
acaf.bcc8322,
acaf.bcx3421,
acaf.bcx3422,
acaf.bcx5020,
acaf.bcx5320,
acaf.bcx7110,
acaf.rev5020,
acaf.rev8320,
acaf.rta7300,
acaf.rtr5030,
acaf.all7517,
acaf.iln0416,
acaf.mta0416,
acaf.mta1370,
acaf.mta2800,
acaf.mta8150,
acaf.mta8153,
acaf.mta8157,
acaf.mta8160,
acaf.mtf0416,
acaf.mtf4260,
acaf.mtf8166,
acaf.mts5020,
af.free_cash_flow_pre,
CASE WHEN af.date_doc_upload IS NOT NULL THEN 1 ELSE 0 END AS docs_ind,
CASE WHEN af.date_signed IS NOT NULL THEN 1 ELSE 0 END AS signed_ind
FROM dwmart.applications_file af
JOIN dwmart.application_credit_attributes_file acaf ON af.dw_application_id = acaf.dw_application_id
WHERE af.application_type = {product} AND {date_range}
AND (af.interest_rate_type = 'FIXED' OR af.interest_rate_type IS NULL)
AND af.current_decision = 'ACCEPT'
"""
"""
LEFT JOIN                   
(
                    SELECT
                    paf.application_id,
                    min(o.champion_challenger_name) as champion_challenger_name,
                    min(CASE WHEN p.product_term = 2
                    THEN ofr.min_rate END) AS rate_2,
                    min(CASE WHEN p.product_term = 3
                    THEN ofr.min_rate END) AS rate_3,
                    min(CASE WHEN p.product_term = 4
                    THEN ofr.min_rate END) AS rate_4,
                    min(CASE WHEN p.product_term = 5
                    THEN ofr.min_rate END) AS rate_5,
                    min(CASE WHEN p.product_term = 6
                    THEN ofr.min_rate END) AS rate_6,
                    min(CASE WHEN p.product_term = 7
                    THEN ofr.min_rate END) AS rate_7,
                    min(CASE WHEN p.product_term = 10
                    THEN ofr.min_rate END) AS rate_10,
                    min(CASE WHEN p.product_term = 15
                    THEN ofr.min_rate END) AS rate_15,
                    min(CASE WHEN p.product_term = 20
                    THEN ofr.min_rate END) AS rate_20,
                    min(REGEXP_SUBSTR(min_tier_code,'[1-9]')) AS tier,
                    min(d.CALENDAR_DATE) AS offer_date
                    FROM sofidw.product_application_facts paf
                    JOIN sofidw.underwriting_info ui ON ui.underwriting_info_id =
                    coalesce(NULLIF(paf.final_uw_id, 0), NULLIF(paf.selected_uw_id, 0),
                    NULLIF(paf.initial_uw_id, 0))
                    JOIN sofidw.offer_facts ofr ON ofr.underwriting_info_id = ui.underwriting_info_id
                    JOIN sofidw.offer_details o on ofr.offer_details_id = o.offer_details_id
                    JOIN sofidw.products p ON p.product_id = ofr.product_id
                    JOIN sofidw.dates d ON d.DATE_ID = ofr.ASOF_DATE_ID
                    JOIN dwmart.applications_file af2 on af2.dw_application_id = paf.application_id AND af2.requested_amount > ofr.min_amount AND af2.requested_amount <= ofr.max_amount
                    WHERE o.rate_type_code = 'FIXED' AND o.eligible = TRUE
                    GROUP BY paf.APPLICATION_ID
                    ) oo ON oo.application_id = af.dw_application_id
"""

#2. Query rate data
rate_qry = f"""SELECT
af.id,
af.requested_amount,
p.product_term,
of.min_rate,
SUBSTRING(o.min_tier_code,'[1-9]')::INT AS tier,
of.max_amount
FROM dwmart.applications_file af
left JOIN product_application_facts paf on af.dw_application_id = paf.application_id
JOIN underwriting_info ui ON ui.underwriting_info_id = coalesce(NULLIF(paf.final_uw_id, 0), NULLIF(paf.selected_uw_id, 0), NULLIF(paf. initial_uw_id, 0))
JOIN offer_facts of ON of.underwriting_info_id = ui.underwriting_info_id
JOIN offer_details o on of.offer_details_id = o.offer_details_id
JOIN products p ON p.product_id = of.product_id
WHERE o.rate_type_code = 'FIXED' AND o.eligible = TRUE
and af.application_type = {product} AND {date_range}
AND af.current_decision = 'ACCEPT'
"""
df_rate = query_data(rate_qry, 'sofidw')
df_rate.sort_values(by=['id', 'product_term', 'max_amount'], inplace=True)
df2 = df_rate.drop_duplicates(subset=['id', 'product_term', 'min_rate'], keep='last') #For each unique id, product_term and min_rate, keep the largest max_amount
df4 = df2[df2['max_amount'] >= df2['requested_amount']]
df4 = df4.sort_values(by=['id', 'product_term', 'max_amount'])
df4 = df4.drop_duplicates(subset=['id', 'product_term'], keep='first')
df4_new = flat_term_rate_dat(df4, flat_cols=['min_rate', 'tier'], consolidate_cols=[])
df4_new.shape
#(437672, 12)
df5_new = df4_new.reset_index()
df5_new.to_csv('../appleads_model/data/rates_201901_2020_0310.csv', index=None)
