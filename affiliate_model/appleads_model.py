#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 10 13:02:30 2020

@author: esun

tier_pre missing, assign tier according to rate
"""
from os.path import join as pjoin
import numpy as np
import pandas as pd
import sys
sys.path.append('../erika_git/erika_utils')
from util_data import add_signed_ind, divide_train_oos
from util_model import train_lightgbm, load_json, read_prepare_data, make_output_dir, \
score_rate_model, read_params, score_model, score_term_model, cast_bool_from_json, merge_rate_model
from metrics import gini, accuracy_binary

#Build elasticity model for appleads data
#1. 01/17 - 02/08 API data
dat_dir = '../appleads_model/data'
df = pd.read_csv(pjoin(dat_dir, 'appleads_after_0117_drop.csv'))
df['created_dt'] = pd.to_datetime(df['created_dt'])
df = df[df['created_dt'] <= '2020-02-08']
df['api_ind'] = 1
df['consolidated_channel_model'] = np.where(df['name'] == 'Credit Karma', 'CK_API',
                                   np.where(df['name'] == 'Lending Tree', 'LT_API', 'Other_API'))
df['pl_funds_use'] = np.where(df['purpose'] == 'DEBTCONSOLIDATION', 'loan_consolidate',
                     np.where(df['purpose'] == 'CCREFI', 'credit_card_refi',
                     np.where(df['purpose'] == 'HOMEIMP', 'home_imp',
                     np.where(df['purpose'] == 'MAJORPURCHASE', 'major_purchase', 'other'))))
df.rename(columns={'attempt_target_id': 'id', 'created_dt': 'date_start', 'tier': 'tier_pre', 
                   'monthly_free_cash_flow': 'free_cash_flow_pre', 'requested_amt': 'requested_amount',
                   'revolving_credit_amt': 'revolving_credit_amount', 'tax_burden_amt': 'tax_burden_amount'}, inplace=True)
select_cols = ['partner_offer_attempt_id', 'id', 'date_start', 'api_ind', 'all0416', 'all5020', 'all5820', 'all7517',
                'all8020',
                'all8220',
                'alx8220',
                'bcc7110',
                'bcc8322',
                'bcx3421',
                'bcx3422',
                'bcx5020',
                'bcx5320',
                'bcx7110',
                'consolidated_channel_model',
                'credit_score',
                'free_cash_flow_pre',
                'gross_income',
                'iln0416',
                'iln5020',
                'iln5820',
                'min_rate_2',
                'min_rate_3',
                'min_rate_4',
                'min_rate_5',
                'min_rate_6',
                'min_rate_7',
                'mta0416',
                'mta1370',
                'mta5020',
                'mta5830',
                'mtf5020',
                'mtj5030',
                'mtj5820',
                'pil0438',
                'pl_funds_use',
                'requested_amount',
                'rev0416',
                'rev5020',
                'rev8320',
                'revolving_credit_amount',
                'tax_burden_amount']
df = df[select_cols]

#Merge to get label variable
df_af = pd.read_csv(pjoin(dat_dir, 'pl_201901_2020_0310_with_rates.csv'))
df_af = df_af[(df_af['date_start'] >= '2020-01-17')]
df = pd.merge(df, df_af[['id', 'signed_ind', 'registration_date', 'initial_term'] + [f'min_rate_{term}' for term in range(2, 8)] \
                        + ['tier_pre'] + [f'signed_ind_{term}' for term in range(2, 8)]], 
              how='left', on='id')
df[['signed_ind'] + [f'signed_ind_{term}' for term in range(2, 8)]] = df[['signed_ind'] + [f'signed_ind_{term}' for term in range(2, 8)]].fillna(0)
for term in range(2, 8):
    df[f'min_rate_{term}'] = np.where(df[f'min_rate_{term}_y'].notnull(), 
                                      df[f'min_rate_{term}_y'], df[f'min_rate_{term}_x'])
df = df[~np.all(df[[f'min_rate_{term}' for term in range(2, 8)]].isnull(), axis=1)]
df.drop([f'min_rate_{term}_x' for term in range(2, 8)] + [f'min_rate_{term}_y' for term in range(2, 8)] , axis=1, inplace=True)
df.to_csv(pjoin(dat_dir, 'appleads_0117_0208_clean.csv'), index=False)

#2. 2019/01 - 2020/02/08 Non API data
df = pd.read_csv(pjoin(dat_dir, 'pl_201901_2020_0310.csv'))
df = df[df['date_start'] <= '2020-02-08']
df = df.loc[df['requested_amount'] != 0]
df = add_signed_ind(df)
df_rates = pd.read_csv('../appleads_model/data/rates_201901_2020_0310.csv')
df_rates['tier_pre'] = df_rates[[f'tier_{term}' for term in range(2, 8)]].min(axis=1, skipna=True)
df = pd.merge(df, df_rates[[f'min_rate_{term}' for term in range(2, 8)] + ['id', 'tier_pre']], on='id')
df.to_csv(pjoin(dat_dir, 'pl_201901_2020_0310_with_rates.csv'), index=None)

#3. Concat
df = pd.read_csv(pjoin(dat_dir, 'pl_201901_2020_0310_with_rates.csv'))
df_campaign = pd.read_csv(pjoin(dat_dir, 'compaign_id.csv'))
df_campaign_dedup = df_campaign.drop_duplicates('target_id')
df = pd.merge(df, df_campaign_dedup, how='left', left_on='id', right_on='target_id')
#Create API indicator by campaign_id
api_campaign_list = [1420, 1418, 3156,1420,1420,1420,2862,1420,2140,2293,1420,1420,1420,1420,1420,1420,1420,1414,1420,2260,1420,2210,1420,597]
df['api_ind'] = np.where(df['campaign_id'].isin(api_campaign_list), 1, 0)
df = df[df['api_ind'] == 0]

df['pl_funds_use'] = np.where(df['pl_funds_use'] == 'Loan consolidation', 'loan_consolidate',
                     np.where(df['pl_funds_use'] == 'Credit card payoff', 'credit_card_refi',
                     np.where(df['pl_funds_use'] == 'Home improvement', 'home_imp',
                     np.where(df['pl_funds_use'] == 'Major purchase', 'major_purchase', 'other'))))
df['consolidated_channel_model'] = np.where(df['consolidated_channel'] == 'MKT-DM', 'DM',
                                  np.where((df['attr_affiliate_referrer'] == 'lending_tree') & (df['api_ind'] == 1), 'LT_API',
                                  np.where((df['attr_affiliate_referrer'] == 'credit_karma') & (df['api_ind'] == 1), 'CK_API',
                                  np.where((df['consolidated_channel'] == 'Affiliate') & (df['api_ind'] == 1), 'other_API',
                                  np.where(df['consolidated_channel'] == 'Affiliate', 'other_non_API',
                                  np.where(df['consolidated_channel'].isin(['www.sofi.com', 'Organic Search']), 'combined_organic', 'other')
                                  )))))

df_api = pd.read_csv(pjoin(dat_dir, 'appleads_0117_0208_clean.csv'))
df2 = df[df_api.columns[1:]]
df_all = pd.concat([df2, df_api], axis=0)
df_all = df_all[df_api.columns]
df_all = df_all[df_all['initial_term'] != 20]
df_all.to_csv(pjoin(dat_dir, 'data_all_1.csv'), index=None)

#process data
df = pd.read_csv(pjoin(dat_dir, 'data_all_1.csv'))
df['date_start'] = pd.to_datetime(df['date_start'])
df['date_start_year'] =  df['date_start'].dt.year
df['date_start_month'] = df['date_start'].dt.month
df['date_start_day'] = df['date_start'].dt.day
df['min_all_rates'] = df[[f'min_rate_{term}' for term in range(2, 8)]].min(axis=1, skipna=True)
#registration_date: date_start - var
df['registration_date'] = pd.to_datetime(df['registration_date'])
df['registration_date'] = (df['date_start'] - df['registration_date']).dt.days
df['registration_date'] = df['registration_date'].fillna(0)       
df['registration_date'] = np.clip(df['registration_date'], 0, 1717)            
df.reset_index(inplace=True)
df.rename(columns={'index': 'eid'}, inplace=True)
df.to_csv(pjoin(dat_dir, 'data_all_clean.csv'), index=None)

#Divide 70/30
train, oos = divide_train_oos(df)
train.to_csv(pjoin(dat_dir, 'train.csv'), index=None)
oos.to_csv(pjoin(dat_dir, 'oos.csv'), index=None)
train.iloc[:1].to_csv(pjoin(dat_dir, 'header_appleads.csv'), index=None)

#04/23: Merge initial_term variable to train and oos dataset
def merge_col(orig_dat_file, merge_dat_file, output_file, id_col='eid', merge_col='initial_term'):
    df = pd.read_csv(orig_dat_file)
    df_merge = pd.read_csv(merge_dat_file, usecols=[id_col, merge_col])
    df = pd.merge(df, df_merge, on=id_col)
    df.to_csv(output_file, index=None)
    return df

train = merge_col(orig_dat_file=pjoin(dat_dir, 'train.csv'), 
                  merge_dat_file=pjoin(dat_dir, 'data_all_clean.csv'), 
                  output_file=pjoin(dat_dir, 'train_0423.csv'))
oos = merge_col(orig_dat_file=pjoin(dat_dir, 'oos.csv'), 
                  merge_dat_file=pjoin(dat_dir, 'data_all_clean.csv'), 
                  output_file=pjoin(dat_dir, 'oos_0423.csv'))

#term selection data
def gen_term_data(in_data_file, out_data_file):
    df = pd.read_csv(in_data_file)
    orig_shape = df.shape[0]
    df = df[df['initial_term'].notnull()]
    for term in range(2, 8):
        df[f'initial_term_{term}'] = np.where((df['initial_term'] == term), 1, 0)
    df.to_csv(out_data_file, index=None)
    print(df.shape[0] / orig_shape)
    
gen_term_data(pjoin(dat_dir, 'train_0423.csv'), pjoin(dat_dir, 'train_term_0423.csv'))
gen_term_data(pjoin(dat_dir, 'oos_0423.csv'), pjoin(dat_dir, 'oos_term_0423.csv'))


#3. Elasticity model
#Read rate paramsters
params_rate = load_json('../round_2/experiments/7c_2019-11-22_19-48-31/params_rate_7c.json') 
header_file_rate = '../appleads_model/model/header_appleads.csv'
fit_params_rate, classifier_params_rate = read_params(params_rate)

#Read data
train_data_file = '../appleads_model/data/train.csv'
oos_data_file = '../appleads_model/data/oos.csv'
X_train_rate, y_train_rate = read_prepare_data(header_file_rate, train_data_file)
X_oos_rate, y_oos_rate = read_prepare_data(header_file_rate, oos_data_file)

#Train rate model
model_output_dir = '../appleads_model/model/model_option_3'
rate_cols = [f'min_rate_{term}' for term in range(2, 8)]
model_rate = train_lightgbm(fit_params_rate, classifier_params_rate, X_train_rate, y_train_rate, X_oos_rate, y_oos_rate, 
                            model_output_dir, model_name=f'appleads_option3', monotone_decreasing_cols=rate_cols)

#Score rate model
df_train_rate = score_rate_model(train_data_file, X_train_rate, model_rate, pjoin(model_output_dir, 'pred_train_rate.csv'),
                                 id_col='eid', other_cols=['api_ind'])
df_oos_rate = score_rate_model(oos_data_file, X_oos_rate, model_rate, pjoin(model_output_dir, 'pred_oos_rate.csv'),
                               id_col='eid', other_cols=['api_ind'])

#Model evaluation
print(df_oos_rate['signed_ind'].mean())
print(df_oos_rate.groupby('api_ind')['signed_ind'].mean())

def cal_gini_api(df):
    df_api = df[df['api_ind'] == 1]
    df_other = df[df['api_ind'] == 0]
    result = [gini(df['signed_ind'], df['rate_pred']), 
                  gini(df_api['signed_ind'], df_api['rate_pred']), 
                  gini(df_other['signed_ind'], df_other['rate_pred'])]
    return pd.DataFrame(result).transpose()

def cal_accuracy_api(df):
    df_api = df[df['api_ind'] == 1]
    df_other = df[df['api_ind'] == 0]
    result = [accuracy_binary(df['signed_ind'], df['rate_pred']), 
              accuracy_binary(df_api['signed_ind'], df_api['rate_pred']), 
              accuracy_binary(df_other['signed_ind'], df_other['rate_pred'])]
    return pd.DataFrame(result).transpose()

#4. Term selection model
#Read term paramsters
params_term = load_json('../round_2/experiments/7c_2019-11-22_19-48-31/params_term_7c.json') 
header_file_term = '../appleads_model/model/header_appleads_term.csv'
fit_params_term, classifier_params_term = read_params(params_term)

#Read data
train_data_file = '../appleads_model/data/train_term_0423.csv'
oos_data_file = '../appleads_model/data/oos_term_0423.csv'
X_train_term, y_train_term = read_prepare_data(header_file_term, train_data_file)
X_oos_term, y_oos_term = read_prepare_data(header_file_term, oos_data_file)

#Train term model
model_output_dir = '../appleads_model/model/model_option_3_term'
rate_cols = [f'min_rate_{term}' for term in range(2, 8)]
fit_params_term['early_stopping_rounds'] = 500
classifier_params_term['learning_rate'] = 0.01
classifier_params_term['max_depth'] = 4
classifier_params_term['num_leaves'] = 16
classifier_params_term['min_child_samples'] = 100

model_term = train_lightgbm(fit_params_term, classifier_params_term, X_train_term, y_train_term, X_oos_term, y_oos_term, 
                            model_output_dir, model_name=f'appleads_option3_term')
model_file_term = '../appleads_model/model/model_option_3_term/model_appleads_option3_term.txt'

#Score term model
y_pred_oos = score_model(model_file=model_file_term, output_file=pjoin(model_output_dir, 'pred_oos_term.csv'), 
                         data_file=oos_data_file, header_file=header_file_term)
rate_orig_cols = [f'min_rate_{term}_orig' for term in range(2, 8)]
terms = range(2, 8)
target_term_col = 'initial_term'
target_term_prob_cols = [f'{target_term_col}_{term}' for term in terms]
pred_term_prob_cols = [col + '_prob_adj' for col in rate_cols]

def score_term_model(data_file, raw_model_output_file, output_file, 
                     impute_rate_cols, orig_rate_cols, terms, target_term_prob_cols,
                     id_col='eid', target_col='initial_term', loan_amount_col='requested_amount'):
    """From raw prediction populate the final columns
    
    Input:
        data_file: original data file for prediction
        raw_model_output_file: raw prediction from lightgbm
        output_file: location to save output data
        impute_rate_cols: list for column names of rates with missing imputations
        orig_rate_cols: list for column names of rates without missing imputations
        terms: list of integer terms
    """
    p = pd.read_csv(raw_model_output_file)
    prob_cols = [rate_col + '_prob' for rate_col in impute_rate_cols]
    p.columns = prob_cols
    df = pd.read_csv(data_file, usecols=[id_col, target_col, loan_amount_col] + impute_rate_cols + orig_rate_cols + target_term_prob_cols)
    df = pd.merge(df, p, left_index=True, right_index=True)
    df = cal_term_rate(df, impute_rate_cols, orig_rate_cols, terms, prob_cols)
    esave(df, output_file)
    return df

def cal_term_rate(df, impute_rate_cols, orig_rate_cols, terms, prob_cols):
    """Generate the following:
        1. term with highest score
        2. score weighted average term and round to the nearest term in terms
        3. score weighted average rate
        
    Input: 
        df: original data frame and raw prediction
        impute_rate_cols: list for column names of rates with missing imputations
        orig_rate_cols: list for column names of rates without missing imputations
        terms: list of integer terms
    """
    rates = df[orig_rate_cols]
    #mask matrix: 0 for missing rates, 1 non-missing
    mask = ~rates.isna() + 0
    adjusted_p = df[prob_cols].values * mask.values
    adjusted_p = pd.DataFrame(adjusted_p)
    
    #1. term with highest score
    adjusted_p_max = adjusted_p.idxmax(axis=1) #index for the maximum probability
    term_map = {idx: term for idx, term in enumerate(terms)}
    df['term_max_prob'] = adjusted_p_max.map(term_map) #map the index to rate
        
    #2. score weighted average term and round to the nearest term in terms
    adjusted_p['sum'] = adjusted_p.sum(axis=1)
    for col in adjusted_p.columns[: -1]:
        adjusted_p[col] = np.where(adjusted_p['sum'] != 0, adjusted_p[col] / adjusted_p['sum'], 0)
    term_p_cols = []
    for idx, term in enumerate(terms):
        term_p_col_nm = '{}_p'.format(term)
        adjusted_p[term_p_col_nm] = adjusted_p[idx] * term
        term_p_cols.append(term_p_col_nm)
    df['term_wgt_prob'] = adjusted_p[term_p_cols].sum(axis=1)
    
    def find_closest_value(val):
        distance = 999999
        answer = -1
        for term in terms:
            if np.abs(val-term) < distance:
                distance = np.abs(val-term)
                answer = term
            else:
                continue
        return answer 
    
    df['term_wgt_prob'] = df['term_wgt_prob'].map(find_closest_value)
    
    #Merge adjusted probability with df
    df = pd.merge(df, adjusted_p.iloc[:, :len(terms)], left_index=True, right_index=True)
    prob_cols_adj = [prob_col + '_adj' for prob_col in prob_cols]
    df.rename(columns={idx: prob_col_adj for idx, prob_col_adj in enumerate(prob_cols_adj)}, inplace=True)
    
    #3. score weighted average rate  
    df['avg_rate'] = np.sum(df[impute_rate_cols].fillna(0).values * df[prob_cols_adj].values, axis=1)
    return df

y_pred_oos_all = score_term_model(data_file=oos_data_file, raw_model_output_file=pjoin(model_output_dir, 'pred_oos_term.csv'), 
                                output_file=pjoin(model_output_dir, 'pred_oos_term_final.csv'), 
                                impute_rate_cols=rate_cols, orig_rate_cols=rate_cols, terms=terms, 
                                target_term_prob_cols=target_term_prob_cols)

#Model evaluation
def eval_model_perform_term(df, pred_term_prob_cols, target_term_prob_cols, target_term_col='initial_term', 
                            pred_rate_col='avg_rate', act_rate_col='interest_rate', loan_amount_col='requested_amount'):
    df = df.loc[df[target_term_col].notnull()]
    gini_all = []
    for pred_term_prob_col, target_term_prob_col in zip(pred_term_prob_cols, target_term_prob_cols):
        gini_all.append(gini(df[target_term_prob_col], df[pred_term_prob_col]))
    gini_result = sum(gini_all) / len(gini_all)
    auc = (gini_result + 1) / 2
    accuracy_result = accuracy_multiclass(df, target_term_prob_cols, pred_term_prob_cols)
    #accuracy on predicted rate
    return [gini_result, accuracy_result] + gini_all

eval_model_perform_term(y_pred_oos_all, pred_term_prob_cols, target_term_prob_cols)
[0.0010402270202613158, 0.2904346844171112]